@isTest
public class Test_LawItemTriggerHandler {

	public class Mock implements HttpCalloutMock {
		public HTTPResponse respond(HTTPRequest req) {
			HTTPResponse res = new HTTPResponse();
			res.setStatusCode(200);
			if(req.getEndpoint().containsIgnoreCase('https://www.hebcal.com/converter')){
				res.setBody('{"{"gy":2019,"gm":3,"gd":11,"hy":5779,"hm":"Adar II","hd":4,"hebrew":"ד׳ בַּאֲדָר ב׳ תשע״ט","events":["Parashat Vayikra"]}');
			}else if (req.getEndpoint().containsIgnoreCase('api/login/version/4') || req.getEndpoint().containsIgnoreCase('https://pi.pardot.com/api/login/version/4')){
				system.debug('get autjhentication from pardot ');
				res.setBody('<api_key>asdd1651315</api_key>');
			}else if (req.getEndpoint().containsIgnoreCase('api/list/version/4/do/create')){
				system.debug('create a list at pardot ');
				res.setBody('<list><id>15151</id></list>');
			}else if (req.getEndpoint().containsIgnoreCase('api/email/version/4/do/send/')){
				system.debug('sent email to list members ');
				res.setBody('');
			}else if (req.getEndpoint().containsIgnoreCase('api/listMembership/version/4/do/create')){
				system.debug('create a list at pardot ');
				res.setBody('<list><id>15151</id></list>');
			}			
			return res;
        }
	}

	static void setup() {
        ClsObjectCreator cls = new ClsObjectCreator();

		// List<emailTemplatesTextValues__c> emailTemplatesTextValuesCS = Test.loadData(emailTemplatesTextValues__c.sObjectType, 'emailTemplatesTextValues');
		// List<LawItemRecordTypeList__c> lawItemRecordTypeListCS = Test.loadData(LawItemRecordTypeList__c.sObjectType, 'LawItemRecordTypeList');

		// List<emailTemplatesTextValues__c> emailTemplatesTextValuesCS = new List<emailTemplatesTextValues__c>();
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'And', HebrewValue__c = 'ו', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Brief', HebrewValue__c = 'תקציר:', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Change_Mailing_Preferences', HebrewValue__c = 'לשינוי העדפות הדיוור', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Closed_for_public_comments', HebrewValue__c = 'סגור להערות ציבור מ:', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Combo_Wording', HebrewValue__c = 'נוסח משולב', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Comments', HebrewValue__c = 'הערות', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'daily_or_weekly_summary', HebrewValue__c = 'סיכום %s', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Daily_subject', HebrewValue__c = 'יומי', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Delete_From_Mailing_list', HebrewValue__c = 'להסרה מרשימת הדיוור', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Download', HebrewValue__c = 'להורדת נוסח התזכיר', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Files', HebrewValue__c = 'קבצים:', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'For_Inspecting_And_Commenting', HebrewValue__c = 'לעיון בפרסום והעברת הערות', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'governmental_publications', HebrewValue__c = 'פרסומים ממשלתיים להערות הציבור', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Hello', HebrewValue__c = 'שלום רב,', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Inline_Inspecting_Link', HebrewValue__c = 'לעיון בפרסום והעברת הערות באתר קשרי ממשל.', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'In_the_past_day_in_date', HebrewValue__c = 'ביום האחרון, בתאריך %d.', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'In_the_past_week_in_dates', HebrewValue__c = 'בשבוע האחרון, בין התאריכים %d1 - %d2.', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Legal_advice', HebrewValue__c = 'ייעוץ משפטי,', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Legal_Advice_Department_of', HebrewValue__c = 'מחלקת הייעוץ המשפטי של %acc.', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Ministry_Publishes_For_Comments', HebrewValue__c = '%m מפרסם את %li להערות הציבור.', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Open_to_public_comments_until', HebrewValue__c = 'פתוח להערות ציבור עד:', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'RIA_Report', HebrewValue__c = 'דו"ח RIA', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Sincerely', HebrewValue__c = 'בכבוד רב,', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Summary_email_subject', HebrewValue__c = 'פרסומים ממשלתיים להערות הציבור - סיכום %s', ArabicValue__c = ''));
		// emailTemplatesTextValuesCS.add(new emailTemplatesTextValues__c(Name = 'Summary_Of_Distributed', HebrewValue__c = 'מצורף סיכום הפרסומים שהופצו להערות הציבור באתר החקיקה הממשלתי', ArabicValue__c = ''));
		// insert emailTemplatesTextValuesCS;

		// List<LawItemRecordTypeList__c> lawItemRecordTypeListCS = new List<LawItemRecordTypeList__c>();
		// lawItemRecordTypeListCS.add(new LawItemRecordTypeList__c(Name = 'Memorandum_of_Law', Icon_Width__c = 35, Icon_Url__c = '/Icon-tzkir.png',
		// 																Icon_Height__c = 17, Icon_Base64__c = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAABUCAYAAAAbBQyRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAD5tJREFUeNrsXWlsVNcVvsZmM2vMYjYT2xizB2wwBhsKadI2JDQNKaRK1Kqt1KpVoqaJKvqjVE3aRmp/VG1+ILX5F7WqGrVS1e1HpagEFYzBDTvYYIzZ98VsxmC23qP5rubM5S33j',
		// 																HebrewHeaderImmediate__c = 'פרסום תזכיר חוק להערות הציבור', ArabicHeaderImmediate__c = ''));
		// lawItemRecordTypeListCS.add(new LawItemRecordTypeList__c(Name = 'Ministry_Directive_or_Procedure_Draft', Icon_Width__c = 25, Icon_Url__c = '/Icon-nhlm.png',
		// 																Icon_Height__c = 29, Icon_Base64__c = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAAB7CAYAAACRtWXuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACTxJREFUeNrsXWtsFFUUPkB5YwuoCFGBCioKVETFRyCKP7RR0BoTgmAEDWpQEiBCBDVaEQM/fABGRVEECaARpShGBIRiMBYUeQYEQQoBQQQstUAVCp6TOZM9vZ3Zzu7Ozs7uni/5I',
		// 																HebrewHeaderImmediate__c = 'פרסום טיוטת הנחיה או נוהל להערות הציבור', ArabicHeaderImmediate__c = ''));
		// lawItemRecordTypeListCS.add(new LawItemRecordTypeList__c(Name = 'Secondary_Legislation_Draft', Icon_Width__c = 25, Icon_Url__c = '/Icon-mshn.png',
		// 																Icon_Height__c = 29, Icon_Base64__c = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF0AAABjCAYAAAD5AQweAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACIRJREFUeNrsXQuwVWMUXrlJhUgv9MYoFJmQV2hQYVKRiQbD5NVDefTwKL0n0ShSKTWTIjMJmSJvN0NFGRWV0kjpIT1GFG5177W+2WvPXXe39z5n7/PaZ9//m/lmzr/PPufs+5111',
		// 																HebrewHeaderImmediate__c = 'פרסום טיוטת חקיקת משנה להערות הציבור', ArabicHeaderImmediate__c = ''));
		// lawItemRecordTypeListCS.add(new LawItemRecordTypeList__c(Name = 'Support_Test_Draft', Icon_Width__c = 21, Icon_Url__c = '/Icon-mvhn.png',
		// 																Icon_Height__c = 29, Icon_Base64__c = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF0AAACECAYAAADyQCckAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACPJJREFUeNrsnQvQVVMUx9en5OuBSmGkqFRKUTEVSWpihGoUhiJmMiaRx5gKGZNHHjEVQlJeKfJWaCKvJMKUyKOmh6KHTCoKpXzW3153Ou1773df+5xz7znrP7Pm6+x7O/fc3913n',
		// 																HebrewHeaderImmediate__c = 'פרסום טיוטת מבחן תמיכה להערות הציבור', ArabicHeaderImmediate__c = ''));
		// insert lawItemRecordTypeListCS;

		List<RecordType> accRecTypes = [select Id,Name from RecordType where IsPersonType = false and  SobjectType='Account' And RecordType.Name = 'MOJ Contact'];
		List<Account> accounts = new List<Account>();
		List<Law__c> laws = new List<Law__c>();
		Id lawRecordType = Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Law')!=null ? Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Law').getRecordTypeId() : null;
		Id MemorandumofLaw = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law')!=null ? Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId() : null;
		Id MinistryDirectiveorProcedureDraft = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Ministry_Directive_or_Procedure_Draft')!=null? Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Ministry_Directive_or_Procedure_Draft').getRecordTypeId(): null;
		Id SecondaryLegislationDraft = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Secondary_Legislation_Draft')!=null ? Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Secondary_Legislation_Draft').getRecordTypeId() : null;

		List<Law_Item__c> lawItems = new List<Law_Item__c>();
		List<Contact> contacts = new List<Contact>();
		List<Classification__c> classifications = new List<Classification__c>();
		List<Law_Item_Classification__c> LIclassifications = new List<Law_Item_Classification__c>();
		List<Mailing_Subscription__c> mailinigs = new List<Mailing_Subscription__c>();
		List<ContentVersion> docs = new List<ContentVersion>();
		Map<Id, List<ContentVersion>> docsMap = new Map<Id, List<ContentVersion>>();

		laws.add(cls.returnLaw('0101','1 כאב ראש 1'));
		laws.add(cls.returnLaw('0210','2 כאב ראש 2'));
		laws.add(cls.returnLaw('6660','3 כאב ראש 3'));
		laws.add(cls.returnLaw('5151','4 כאב ראש 4'));
		insert laws;

		accounts.add(cls.returnAccount('ToInsert',accRecTypes[0].Id));
		accounts.add(cls.returnAccount('משרד המשפטים',accRecTypes[0].Id));
		accounts.add(cls.returnAccount('משרד כאב הראש',accRecTypes[0].Id));
		accounts.add(cls.returnAccount('משרד המסחר',accRecTypes[0].Id));
		accounts.add(cls.returnAccount('משרד הופלות',accRecTypes[0].Id));
		insert accounts;

		contacts.add(cls.ReturnContact(accounts[0].Id, 'חיים', 'משה', '5520', 'a@P.com','999985336'));
		contacts.add(cls.ReturnContact(accounts[1].Id, 'Al', 'Chuti', '5523', 'ab@P.com','999584188'));
		contacts.add(cls.ReturnContact(accounts[2].Id, 'Boba', 'Banai', '5524', 'abc@P.com','999884646'));
		contacts.add(cls.ReturnContact(accounts[3].Id, 'שם', 'משפחה', '5526', 'abcd@P.com','999966377'));
		insert contacts;

		List<Contact> contactList = new List<Contact>();
		contactList.add(new Contact(LastName = 'LASTNAME', pi__url__c = '11111111'));
		insert contactList;

		classifications.add(cls.returnClassification('כאב ראש', '0101'));
		classifications.add(cls.returnClassification('בטוחן עצמי', '0202'));
		classifications.add(cls.returnClassification('סתם אכילת ראש', '0303'));
		classifications.add(cls.returnClassification('שעמום כללי', '0404'));
		insert classifications;

		List<Mailing_Subscription__c> msList = new List<Mailing_Subscription__c>();
		msList.add(new Mailing_Subscription__c(Classification__c = null, Law__c = null, Ministry__c = null, Registration_Type__c = 'Personal Preferences',Item_Type__c = 'Memorandum of Law', Mailing_Language__c = 'Hebrew', Contact__c = contactList[0].Id, Items_With_RIA__c = false));
		msList.add(new Mailing_Subscription__c(Classification__c = null, Law__c = null, Ministry__c = null, Registration_Type__c = 'Personal Preferences',Item_Type__c = 'Memorandum of Law', Mailing_Language__c = 'Hebrew', Contact__c = contactList[0].Id, Items_With_RIA__c = true));
		insert msList;

		lawItems.add(cls.returnLawItem(MemorandumofLaw, 'שעמום כללי 1511', 'العَرَبِيَّة‎',accounts[0].Id , 'Legislation Amendment', '', '', accounts[0].Id, true, 'Distributed'));

		// List<ContentVersion> cvs = new List<ContentVersion>();
		// ContentVersion cv1 = cls.returnContentVersion('name1', 'name1.pdf', 'vd');
		// cv1.File_Type__c = 'Main File';
		// cvs.add(cv1);
		// cv1 = cls.returnContentVersion('name2', 'name2.word', 'vd');
		// cv1.File_Type__c = 'RIA File';
		// cvs.add(cv1);
		// cv1 = cls.returnContentVersion('name3', 'name3.txt', 'vd');
		// cv1.File_Type__c = 'Combined Version File';
		// cvs.add(cv1);
		// insert cvs;

		// List<ContentDocument> cds = new List<ContentDocument>();
		// //main
		// cds.add( [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cvs[0].Id LIMIT 1] );
		// //ria
		// cds.add( [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cvs[1].Id LIMIT 1] );
		// //comb
		// cds.add( [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cvs[2].Id LIMIT 1] );

		String myStr = '';
		for(Integer i = 0; i<520; i++){
			myStr+='s';
			if(i == 200 || i == 400) {
				myStr+=' ';
			}
		}
		lawItems[0].Document_Brief__c = myStr;
		lawItems[0].Notify_Users__c = true;
		lawItems[0].Status__c='Distributed';
		lawItems[0].Legal_Counsel_Name__c = contacts[0].Id;
		lawItems[0].Legal_Counsel_Name_Additional__c = null;
		lawItems[0].Updates_Recipients__c = 'ttt@gmail.com';
		lawItems[0].Extra_Updates_Recipients__c = 'yyy@gmail.com';

		insert lawItems;

		// List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
		// //main
		// cdlList.add(cls.returnCDL(cds[0].Id, lawItems[0].Id));
		// //ria
		// cdlList.add(cls.returnCDL(cds[1].Id, lawItems[0].Id));
		// //comb
		// cdlList.add(cls.returnCDL(cds[2].Id, lawItems[0].Id));
		// insert cdlList;

		// lawItems[0].Legal_Counsel_Name_Additional__c = null;

		// lawItems[0].Legal_Counsel_Name__c = contacts[0].Id;
		// lawItems[0].Legal_Counsel_Name_Additional__c = null;
		// update lawItems[0];

		// lawItems[0].Legal_Counsel_Name_Additional__c = contacts[1].Id;
		// update lawItems[0];
		
		List<Related_to_Law__c> r2l = new List<Related_to_Law__c>();

		lawItems.add(cls.returnLawItem(MemorandumofLaw, 'שעמום כללי 1611', 'العَرَبِيَّة',accounts[1].Id , 'New Legislation', lawItems[0].Id, laws[0].Id, accounts[2].Id, false,  'Distributed'));
		//lawItems.add(cls.returnLawItem(MinistryDirectiveorProcedureDraft, 'שעמום כללי 3013', 'العَرَبِيَّة',accounts[4].Id , 'Legislation Amendment', lawItems[1].Id, laws[0].Id, accounts[2].Id, false,  'Distributed'));
		//lawItems.add(cls.returnLawItem(SecondaryLegislationDraft, 'שעמום כללי 5991', 'العَرَبِيَّة',accounts[3].Id , 'Secondary Legislation from Proposed Law', lawItems[2].Id, laws[0].Id, accounts[2].Id, true, 'Distributed'));
		upsert lawItems;

		r2l.add(new Related_to_Law__c(Law_Item__c = lawItems[0].Id, Law__c = laws[0].Id) );
		//r2l.add(new Related_to_Law__c(Law_Item__c = lawItems[1].Id, Law__c = laws[0].Id) );
		//r2l.add(new Related_to_Law__c(Law_Item__c = lawItems[2].Id, Law__c = laws[0].Id) );
		//r2l.add(new Related_to_Law__c(Law_Item__c = lawItems[3].Id, Law__c = laws[0].Id) );
		insert r2l;

		Integer classIndex = 0;
		for(Law_Item__c li : lawItems){
			LIclassifications.add(new Law_Item_Classification__c(Law_Item__c = li.Id, Classification__c = classifications[0].Id));
			LIclassifications.add(new Law_Item_Classification__c(Law_Item__c = li.Id, Classification__c = classifications[1].Id));
			LIclassifications.add(new Law_Item_Classification__c(Law_Item__c = li.Id, Classification__c = classifications[2].Id));
			Boolean isRia = math.mod(classIndex,2)==0? true: false;
			if(!docsMap.containsKey(li.Id)){
				docsMap.put(li.Id, new List<ContentVersion>());
			}
			ContentVersion c1 = cls.returnContentVersion('myTitle '+ String.valueOf(classIndex), false, true ) ;
			docs.add(c1); // one is main must
			docsMap.get(li.Id).add(c1);

			ContentVersion c2 = cls.returnContentVersion('myTitle '+ String.valueOf(classIndex+' i'), isRia, true ) ;
			docs.add(c2); // one is main must
			docsMap.get(li.Id).add(c2);

			if(math.mod(classIndex,3)==0){
				ContentVersion c3 = cls.returnContentVersion('myTitle '+ String.valueOf(classIndex+' j'), false, false );
				docs.add(c3); // one is main must
				docsMap.get(li.Id).add(c3);
			}

			classIndex++;
			classIndex = classIndex>3? 0 : classIndex; 
		}

		upsert LIclassifications;
		insert docs;

		Map<Id,Id> cv2cdMap = new Map<Id,Id>();
		for(ContentVersion c : [SELECT Id, ContentDocumentId from ContentVersion WHERE ID IN : docs]){
			cv2cdMap.put(c.Id, c.ContentDocumentId);
		}

		List<ContentDocumentLink> cdls = new List<ContentDocumentLink>();
		for(Id li : docsMap.KeySet()){
			for(ContentVersion cv : docsMap.get(li)){
				cdls.add(new ContentDocumentLink(   ShareType= 'I',
					        						LinkedEntityId = li, 
					        						ContentDocumentId=cv2cdMap.get(cv.Id),
					        						Visibility = 'AllUsers'));
			}
		}

		insert cdls;

		for(Contact c : contacts){
			mailinigs.add(cls.returnMailingSubsciptionsList(c.Id, 'Personal Preferences', '', accounts[0].Id, 'Arabic', '',	'', true, '', '', '', false));
			mailinigs.add(cls.returnMailingSubsciptionsList(c.Id, 'Personal Preferences', '', '', 'Hebrew', '', laws[0].id, true, '', '', '', true));
			mailinigs.add(cls.returnMailingSubsciptionsList(c.Id, 'Personal Preferences', '', '', 'Hebrew', '', '', false, '','Secondary Legislation Draft Based on Sec Legislation', '', false));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'Personal Preferences', '', '', 'Hebrew', '', '', true, '', '', 'Memorandum of Law', false));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'Personal Preferences', '', '', 'Hebrew', '', '', true, classifications[0].Id, '', '', false));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'Personal Preferences', '', '', 'Hebrew', lawItems[0].id, '', true, '', '', '', false));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'All Updates', 'Weekly', '', 'Hebrew', '', laws[0].id, true, '', '', '', true));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id,'All Updates', 'Daily', '', 'Hebrew', '', '', true, '', '', 'Memorandum of Law', false));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'All Updates', 'Daily', '', 'Arabic', '', '', true, classifications[0].Id, '', '', false));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'All Updates', 'Weekly', '', 'Arabic', '', '', true, classifications[0].Id, '', '', false));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'Personal Preferences', '', accounts[1].Id, 'Hebrew', '', '', true, '', '', '', true));
			mailinigs.add(cls.returnMailingSubsciptionsList( c.Id, 'Personal Preferences', '', accounts[1].Id, 'Hebrew', '', '', true, '', 'Secondary Legislation Draft Based on Authorization Legislation', '', true));
		}
		insert mailinigs;
    }

	@isTest(seeAllData=true)
	static void runningImmendiateMailingBatch() {
		Test.setMock(HttpCalloutMock.class, new Mock());

		Test.StartTest();
		setup();
		Test.stopTest();
		
		Law_Item__c li = [SELECT Id,Notify_Users__c FROM Law_Item__c WHERE Law_Item_Name__c = 'שעמום כללי 1511' LIMIT 1];
		System.assertEquals(false, li.Notify_Users__c, 'ERROR: Notify_Users__c field of Law Item is true after trigger execution');
	}
}