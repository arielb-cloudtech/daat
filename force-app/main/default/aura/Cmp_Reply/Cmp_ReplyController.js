({
    showContent: function(component, event, helper) {

        console.log('showContent: ...');
        var reply = JSON.parse(JSON.stringify(component.get("v.reply")));
        var element = document.querySelector('#' + reply.mId + '.txt');
        if (element) {
            if (element.classList.contains('slds-truncate'))
                element.classList.remove('slds-truncate');
            else
                element.classList.add('slds-truncate');
        }


        var element = document.querySelector('#' + reply.mId + '.files');
        if (element) {
            if (element.classList.contains('groupHidden')) {
                element.classList.remove('groupHidden');
                element.classList.add('groupVisible');
            } else {
                element.classList.remove('groupVisible');
                element.classList.add('groupHidden');
            }
        }

        var element = document.querySelector('#' + reply.mId + '.slds-button');
        if (element) {
            if (element.classList.contains('open')) {
                element.classList.remove('open');
                element.classList.add('close');
                element.innerHTML = component.get("v.btnClose");
            } else {
                element.classList.remove('close');
                element.classList.add('open');
                element.innerHTML = component.get("v.btnOpen");
            }
        }


    },
    showText: function(component, event, helper) {
        console.log('showText');
        if (event.target.classList.contains('slds-truncate')) {
            event.target.classList.remove('slds-truncate');
            event.target.classList.add('preStyle');
            event.getSource().set("v.label", $A.get("$Label.c.Hide"));

        } else {
            event.target.classList.add('slds-truncate');
            event.target.classList.remove('preStyle');
            event.getSource().set("v.label", $A.get("$Label.c.More"));

        }

        var rep = component.get("v.reply");
        rep.bNew = false;
        console.log("rep:");
        console.log(rep);
        component.set("v.reply", rep);
        var eve = $A.get("e.c:updateNotReadCounter");
        eve.fire();
    }
})