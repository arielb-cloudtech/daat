public with sharing class Util_ProfanityCheck {
	public Util_ProfanityCheck() {
		
	}

    public static Case ProfanityCheck(String content, Id comment, Boolean IsCommentRelated, Id reply){
    	Case ProfanityCase = new Case(OriginalText__c=content, ReplacementText__c=content, IsCommentRelated__c = IsCommentRelated);
    	if(IsCommentRelated){
    		ProfanityCase.Comment__c = comment;
            ProfanityCase.ReplacementText__c = comment;
		}else{
    		ProfanityCase.Comment_Reply__c = comment;
    		ProfanityCase.ReplacementText__c = comment;
		}
    	Set<String> commentWords = content!= null && String.isNotBlank(content)? new Set<String>(content.toLowerCase().split(' ')) : new Set<String>();
    	Set<String> customSet = Profanity__c.getAll().KeySet();
    	//Set<String> copy = commentWords.Retains(customSet);

    	//if(commentWords.removeAll(customSet)){ // if any are conatined returns true else returns true
			// = !ProfanityCase.Profanity_Reason__c.containsIgnoreCase('נמצאו מילים פוגעניות בגוף ההערה: ')? 'נמצאו מילים פוגעניות בגוף ההערה: ': ProfanityCase.Profanity_Reason__c;
    	ProfanityCase.Profanity_Reason__c ='';
        if(!commentWords.isEmpty()){
	    	for(String p: customSet){
	    		if(String.isNotBlank(p) && (commentWords.contains(p) || content.containsIgnoreCAse(p)) ){
	    			ProfanityCase.Profanity_Reason__c += '/the word '+p+' : found at '+findAllSubString(p,content.toLowerCase())+'/\r\n';
	    			String replacement ='';
	    			for(Integer i = 0; i < p.Length(); i++){
	    				replacement+='*';
	    			}
	    			content=content.toLowerCase().replaceAll(p,replacement);
	    		}
	    	}
	    	if(!String.isBlank(ProfanityCase.Profanity_Reason__c)){
	    		ProfanityCase.Profanity_Reason__c = 'נמצאו מילים פוגעניות בגוף ההערה: \r\n'+ProfanityCase.Profanity_Reason__c;

	    	}
        }
    	ProfanityCase.ReplacementText__c = content;
    	system.debug('ProfanityCase '+ ProfanityCase);
        return  ProfanityCase;
    	//}else{

    	//}

    }

    public static String findAllSubString(String subString, String content){
    	String res='';
    	Integer Index = 0;
    	while( Index != -1 ){
    		Index = content.indexOf(subString, Index);
    		if(Index!= -1){
    			res += String.valueOf(index)+',';
    			index=index+subString.length();
    		} 
    	}
    	res.removeEnd(',');
    	return res;
    }
}