({
	doInit : function(cmp, evt, helper){
		
		switch ( cmp.get("v.currentRecordType") ) {
			case "Secondary Legislation Draft":
			case "Ministry Directive or Procedure Draft":
				cmp.find("shortTimeApproved").set("v.label","התקבל אישור היועץ המשפטי של הגורם המפיץ לקיצור מספר הימים");
			break;
			case "Support Test Draft":
				cmp.find("shortTimeApproved").set("v.label","התקבל אישור ייעוץ וחקיקה לקיצור מספר הימים");
			break;
		}

		var lawItem = cmp.get("v.newLawItem");

		if(! lawItem.Number_of_Days_to_Respond__c ){
			helper.setDatesByNumber( cmp,  21 );
		}
		else {
			helper.setDatesByNumber( cmp,  lawItem.Number_of_Days_to_Respond__c );
		}

		cmp.set("v.commentsAlerts", lawItem.Updates_Recipients__c);
		helper.showDeductionReason(cmp, lawItem);
	},

	reasonChange : function(cmp, evt, helper) {
		var reason = cmp.get("v.reasonChoice");
		if(reason == 'אחר'){
			cmp.set("v.showExtraOther", "true");
		}else{
			cmp.set("v.showExtraOther", "false");
			var p3Data = cmp.get("v.p3Data");
			p3Data.exp = '';
			cmp.set("v.p3Data", p3Data);
		}
		var lawItem = cmp.get("v.newLawItem");
		switch(reason){
			case 'צורך חיוני או דחיפות בפרסום':
				lawItem.Time_Deduction_Reason__c = 'Urgent Requirement';
				break;
			case 'השלכות מינימליות על הציבור':
				lawItem.Time_Deduction_Reason__c = 'Minimal Public Impact';
				break;
			case 'אחר':
				lawItem.Time_Deduction_Reason__c = 'Other';
				break;
			default:
				radioValue = '';
		}
		lawItem.Time_Deduction_Approval__c = 'Legal Counsel Approved';
		cmp.set("v.newLawItem", lawItem);
	},

	numberOfDaysChanged : function(cmp, evt, helper){
		var lawItem = cmp.get("v.newLawItem");
		helper.setDatesByNumber(cmp, parseInt(lawItem.Number_of_Days_to_Respond__c));
	},
	
	dateChanged : function(cmp, evt, helper){
		var p3Date = cmp.get("v.p3Data");
		var selectedDate  = new Date(p3Date.lastDate); 
		var publicationDate = new Date(p3Date.pubDate);
		var daysBetween = Math.ceil((selectedDate-publicationDate)/1000/60/60/24);
		
		helper.setDatesByNumber(cmp, daysBetween);
	},

	doValidation : function(cmp, event, helper) {
		var lawItem = cmp.get("v.newLawItem");
		
		var p3Data = cmp.get("v.p3Data");
		if((p3Data.pubDate != "" && p3Data.pubDate != undefined) && (p3Data.pubHour != "" && p3Data.pubHour != undefined) && (p3Data.lastDate != "" && p3Data.lastDate != undefined) && (p3Data.lastHour != "" && p3Data.lastHour != undefined)){
			lawItem.Publish_Date__c = p3Data.pubDate + "T" + p3Data.pubHour + "Z";
			lawItem.Last_Date_for_Comments__c = p3Data.lastDate + "T" + p3Data.lastHour + "Z";
			cmp.set("v.newLawItem", lawItem);
		}
		
		var errorCounter = 0;

		if(p3Data.pubDate == "" || p3Data.pubDate == null || p3Data.pubDate == undefined){
			errorCounter++;
			cmp.set("v.showPubError", "true");
		}else{
			cmp.set("v.showPubError", "false");
		}
		if(p3Data.lastDate == "" || p3Data.lastDate == null || p3Data.lastDate == undefined){
			errorCounter++;
			cmp.set("v.showLastDateError", "true");
		}else{
			cmp.set("v.showLastDateError", "false");
		}
		
		var showExtra = cmp.get("v.showExtra");
		var reasonChoice = cmp.get("v.reasonChoice");
		if(showExtra == "true" && (reasonChoice == "" || reasonChoice == "נא לבחור" || reasonChoice == null || reasonChoice == undefined)){
			errorCounter++;
			cmp.set("v.showChoiceError", "true");
		}else{
			cmp.set("v.showChoiceError", "false");
		}
		var showExtraOther = cmp.get("v.showExtraOther");
		if(showExtra == "true" && showExtraOther == "true" && (lawItem.Other_Time_Deduction_Reason__c == "" || lawItem.Other_Time_Deduction_Reason__c == null || lawItem.Other_Time_Deduction_Reason__c == undefined)){
			errorCounter++;
			cmp.set("v.showExpError", "true");
		}else{
			cmp.set("v.showExpError", "false");
		}
		
		if ( lawItem.Number_of_Days_to_Respond__c > 999 || isNaN(lawItem.Number_of_Days_to_Respond__c) ) {
			errorCounter++;
			cmp.set("v.showDays3Error", "true");
		}else{
			cmp.set("v.showDays3Error", "false");
		}	
		
		if(showExtra == "true" && lawItem.Short_Time_Approval__c == false){
			errorCounter++;
			cmp.set("v.approvalReceivedError", "true");
		}else{
			cmp.set("v.approvalReceivedError", "false");
		}

		var distAlerts = cmp.get("v.distAlerts");
		var commentsAlerts = lawItem.Updates_Recipients__c;
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(lawItem.Extra_Updates_Recipients__c){
			distAlerts = lawItem.Extra_Updates_Recipients__c
			distAlerts = distAlerts.trim().split(";");
			for(const key in distAlerts){
				if(distAlerts[key].trim() && !re.test(distAlerts[key].trim())){
					errorCounter++;
					cmp.set("v.distAlertsError", "true");
				}else{
					cmp.set("v.distAlertsError", "false");
				}
			}
		}
		if(commentsAlerts){
			cmp.set("v.commentsAlertsError2", "false");
			commentsAlerts = commentsAlerts.trim().split(";");
			var isemp = false;
			for(const key in commentsAlerts){
				if(commentsAlerts[key].trim()){
					isemp = true;
				}
				if(commentsAlerts[key].trim() && !re.test(commentsAlerts[key].trim())){
					errorCounter++;
					cmp.set("v.commentsAlertsError", "true");
				}else{
					cmp.set("v.commentsAlertsError", "false");
				}
			}
			if (!isemp) {
				errorCounter++;
				cmp.set("v.commentsAlertsError2", "true");
			}
		} else {
			errorCounter++;
			cmp.set("v.commentsAlertsError2", "true");
		}

		return errorCounter == 0;
	}
})