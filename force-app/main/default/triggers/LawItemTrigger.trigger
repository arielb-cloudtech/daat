trigger LawItemTrigger on Law_Item__c ( before insert, before update, after insert, after update) {
	LawItemTriggerHandler Handler = new LawItemTriggerHandler();
    
    if ( Trigger.isInsert ) {
        if ( Trigger.isBefore ) {
            for ( Law_Item__c lawItem : Trigger.new ) {
                lawitem.Publisher_Account__c = lawitem.Account_Responsibility__c;
            }
            Handler.splitDocBriefInsert(Trigger.new);
        } 
        else if ( Trigger.isAfter ) {
            Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Hebrew');
            // Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Arabic');

            Handler.sendEmailToWizardList(Trigger.new, 'Hebrew');

            Handler.UnCheckNotify(Trigger.new);
        } 
    }
    else if ( Trigger.isUpdate) {

        if ( Trigger.isBefore ) {
            Handler.splitDocBriefUpdate(Trigger.newMap, Trigger.oldMap, Trigger.new);
        } 
        else if ( Trigger.isAfter ) {
            Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Hebrew');
            // Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Arabic');
            
            Handler.sendEmailToWizardList(Trigger.new, 'Hebrew');

            Handler.UnCheckNotify(Trigger.new);
        }
    }
}