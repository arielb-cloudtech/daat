({
	doInit: function (component) {		 
		console.log('section',  JSON.parse(JSON.stringify(component.get("v.comment.sections"))));
		if (JSON.parse(JSON.stringify(component.get("v.comment.bFiles")))) {			
			var f = JSON.parse(JSON.stringify(component.get("v.comment.mFiles")));
			if (f.length > 0) {
				console.log("doInit - edit");				
				for (var i = 0; i < f.length; i++) {
					console.log('file from ' + i + ' = ' + f[i] + ' ' + f[i].documentId);
					$A.createComponent(
						"c:LawItem_FileLoaded",
						{
							"aura:id": "section1",
							"file": f[i],
							"deleteFile": true,
							"fileId": f[i].documentId
						},
						function (newComponent, status, errorMessage) {
							if (status === "SUCCESS") {
								var commentBody = component.get("v.commentBody");
								commentBody.push(newComponent);
								component.set("v.commentBody", commentBody);
								console.log("Success: ");
							}
							else if (status === "INCOMPLETE") {
								console.log("No response from server or client is offline.")
							}
							else if (status === "ERROR") {
								console.log("Error: " + errorMessage);
							}
						}
					);
				}
			}
		}
		var s = JSON.parse(JSON.stringify(component.get("v.comment.sections")));
		if (s.length > 0) {
			for (var i = 0; i < s.length; i++) {
				$A.createComponent(
					"c:LawItem_CommentSection",
					{
						"aura:id": "section1",
						"sectionNum": s[i].bulletNumber,
						"sectionComment": s[i].body,
						"subCommentId":s[i].subCommentId,
						"DeleteComp": component.get("v.deleteComp")
					},
					function (newComponent, status, errorMessage) {
						if (status === "SUCCESS") {
							var body = component.get("v.body");
							body.push(newComponent);
							component.set("v.body", body);
						}
						else if (status === "INCOMPLETE") {
							console.log("No response from server or client is offline.")
						}
						else if (status === "ERROR") {
							console.log("Error: " + errorMessage);
						}
					}
				);
			}
		}
		if (component.get("v.comment.bFiles"))
			component.set('v.bFile', true);
		if (!component.get('v.comment.bPrivate'))
			component.set('v.isPrivate', true);

	},

	cancelUpdate: function (component, event) {
		console.log('cancelUpdate');
		var action = component.get("c.CancelCommentUpdate");
		action.setParams({ "filesList": JSON.parse(JSON.stringify(component.get("v.filesList"))) });
		$A.enqueueAction(action);
		action.setCallback(this, function (response) {
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {
				component.set("v.isOpen", false);
			}
			else {
				component.set("v.isSaving", false);
				alert("Custom Update ERROR!!!");
			}

		});
	},

	addComment: function (component, event, helper) {
		console.log('createComponent');
		component.set('v.ifError', false);
		component.set("v.errorText", '');
		var body = component.get("v.body");
		var i = body.length - 1;
		if (i > 0 && body[i].get('v.sectionNum') == null && body[i].get('v.sectionComment') == null && body[i].get('v.deleteComp')) {
			component.set('v.ifError', true);
			component.set("v.errorText", 'חובה למלא הערה קודמת');
			return;
		}

		$A.createComponent(
			"c:LawItem_CommentSection",
			{
				"aura:id": "section1",
				"SectionNum": component.get("v.sectionNum"),
				"SectionComment": component.get("v.sectionComment"),
				"DeleteComp": component.get("v.deleteComp")
			},
			function (newComponent, status, errorMessage) {
				if (status === "SUCCESS") {
					var body = component.get("v.body");
					body.push(newComponent);
					component.set("v.body", body);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
				}
			}
		);
	},

	onSaveData: function (component, event) {
		console.log('save and close = ' + component.get("v.deletedFiles"));
		component.set('v.ifError', false);
		component.set("v.errorText", '');
		console.log('save and close');
		var recordId = event.getParam("recordId");
		console.log('recordId = ' + recordId);
		var fullText = null;

		fullText = component.get("v.comment.mBody");
		console.log('fullText = ' + fullText);
		if (fullText == null) {
			component.set("v.isSaving", false);
			component.set('v.ifError', true);
			component.set("v.errorText", 'חובה למלא הערה');
			return;
		}

		var fullTextArray = [];		
		var deleteTextArray = [];
		
		var body = component.get("v.body");
		var sNum;
		var sBody;
		var sId;
		for (var i = 0; i < body.length; i++) {
			sNum = body[i].get('v.sectionNum');
			sBody = body[i].get('v.sectionComment');
			sId = body[i].get('v.subCommentId');
			if (body[i].get('v.deleteComp')) {	
				fullTextArray.push ({bulletNumber: sNum, body: sBody, subCommentId: sId});
			}
			else {
				deleteTextArray.push ({bulletNumber: sNum, body: sBody, subCommentId: sId});
			}
		}

		var status = component.find('status').get('v.statusValue');
		if (!status || status.length == 0) {
			component.set("v.isSaving", false);
			component.set('v.ifError', true);
			component.set("v.errorText", 'חובה למלא סטטוס');
			return;
		}
		
		var isPrivate = component.get('v.isPrivate');
		
		
		var commentId = component.get("v.comment.mId");
		console.log('commentId = ' + commentId);
		var action = component.get("c.SaveComment");
		var subComments = JSON.stringify(fullTextArray);
		console.log('subComments = ' + subComments);		
		action.setParams({
							"body": fullText,
							"commentId": commentId,
							"status": status,
							"filesToDelete": JSON.parse(JSON.stringify(component.get("v.deletedFiles"))),
							"dispCom" : isPrivate,
							"subComments": subComments,
							"deleteComments" : JSON.stringify(deleteTextArray)
						});
		action.setCallback(this, function (response) {
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {

				var returnValue = response.getReturnValue();
				console.log('returnValue', returnValue);
				var comments = component.get('v.comments');
				for (var i = 0; i < comments["mComments"].length; i++) {
					if (comments["mComments"][i].mId == commentId) {
						comments["mComments"][i] = returnValue;
					}
				}
				component.set("v.comments", comments);
				component.set("v.isOpen", false);
				var fireEvent = component.getEvent("newCommentAdded");
				fireEvent.setParams({ "comments": comments });
				fireEvent.fire();
				console.log("DONE inner");
			}
			else {
				component.set("v.isSaving", false);
				alert("Custom Update ERROR!!!");
			}

		});
		$A.enqueueAction(action);
	}
})