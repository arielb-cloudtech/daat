public without sharing class ContactTriggerHandler {
	public Map<String, Id> profileIdByName;
	public Map<String, Id> userRoleByName;
	public Map<id,User> contactUserMap;

	private User setUser(Id userId, Contact con, Id profileId, Id userRoleId, String accountName){
		User newUser = new User();
		newUser.Id = userId;
		newUser.FirstName = con.FirstName;
		newUser.LastName = con.LastName;
		newUser.Email = con.Email;
		newUser.Username = con.Email;
		newUser.Alias =  con.LastName.left(8);
		newUser.ProfileId = profileId;
		if (! Test.isRunningTest() ) {
			newUser.Unit__c = accountName;
		}
		newUser.IsActive = true;
		newUser.LocaleSidKey = 'iw_IL' ;
		newUser.TimeZoneSidKey = 'Asia/Jerusalem';
		newUser.LanguageLocaleKey = 'iw';
		newUser.EmailEncodingKey  = 'UTF-8';
		return newUser;
	}

	private Map<Id, String> getAccountNames(List<Contact> contacts){
		Set<Id> accountIds = new Set<Id>();
		for(Contact con : contacts){
			accountIds.add(con.AccountId);
		}
		Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :accountIds]);
		Map<Id, String> accIdToName = new Map<Id, String>();
		for(Account acc : accountMap.values()){
			accIdToName.put(acc.Id, acc.Name);
		}
		return accIdToName;
	}

	private Map<String, Id> getProfileMap(Set<String> roleNames){
		Map<String, Id> toReturn = new Map<String, Id>();
		for (Profile prof : [SELECT Id, Name FROM Profile WHERE Name IN :roleNames]){
			toReturn.put(prof.Name, prof.Id);
		}
		return toReturn;
	}

	private Map<String, Id> getUserRoleMap(Set<String> roleNames){
		Map<String, Id> toReturn = new Map<String, Id>();
		for (UserRole usrRole : [SELECT Id, Name FROM UserRole WHERE Name IN :roleNames]){
			toReturn.put(usrRole.Name, usrRole.Id);
		}
		return toReturn;
	}

	private Set<String> getRoleNames(List<Contact> contacts){
		Set<String> roleNames = new Set<String>();
		for(Contact con : contacts){
			roleNames.add(con.role__c);
		}
		return roleNames;
	}
	private List<Contact> filterNewContacts(List<Contact> contacts){
		List<Contact> toReturn = new List<Contact>();
		for(Contact con : contacts){
			if(con.Active_User__c && con.Role__c != 'Customer Community MOJ'){
				toReturn.add(con);
			}
		}
		return toReturn;
	}

	private void createUserForContact(List<Contact> newList){
		List<Contact> filteredContacts = filterNewContacts(newList);
		Set<String> roleNames = getRoleNames(filteredContacts);
		Map<String, Id> profileMap = getProfileMap(roleNames);
		Map<String, Id> userRoleMap = getUserRoleMap(roleNames);
		Map<Id, String> accIdToName = getAccountNames(filteredContacts);
		
		
		Map<Integer, Contact> KeyToContact = new Map<Integer, Contact>();
		Map<Integer, User> KeyToUser = new Map<Integer, User>();
		Integer key = 0;
		for(Contact con : filteredContacts){
			KeyToContact.put(key, con);			
			KeyToUser.put(Key, setUser(null, con, profileMap.get(con.Role__c), userRoleMap.get(con.Role__c), accIdToName.get(con.AccountId)));		
			++key;
		}

		Database.DMLOptions dmlOpt = new Database.DMLOptions();
		dmlOpt.EmailHeader.triggerUserEmail = true;		
		Database.insert(KeyToUser.values(), dmlOpt);
		List<User> usersForUpdate = new List<User>();		

		for(Integer keyNumber : KeyToContact.keySet()){
			KeyToContact.get(keyNumber).User__c = KeyToUser.get(keyNumber).Id;
			KeyToUser.get(keyNumber).UserRoleId = userRoleMap.get(KeyToContact.get(keyNumber).Role__c);						
			usersForUpdate.add (KeyToUser.get(keyNumber));
		}		
		updateUserAsync(JSON.serialize(usersForUpdate));
	}

	private Map<Id, User> handleContactsToBeDisable(Map<Id, Contact> contacts){
		Map<Id, User> diabledUsers = new Map<Id, User>();		
		Set<Id> userIds = new Set<Id>();
		for(Contact con : contacts.values()){
			userIds.add(con.User__c);
		}
		Map<Id, User> users = new Map<Id, User>([SELECT ID, IsActive FROM User WHERE ID IN: userIds]);
		for(Id conId : contacts.keySet()){
			users.get(contacts.get(conId).User__c).IsActive = false;
			diabledUsers.put(conId, users.get(contacts.get(conId).User__c));
		}

		return diabledUsers;
	}

	private Map<Id, User> handleContactsWithUser(Map<Id, Contact> contacts, Map<String, Id> profileMap, Map<String, Id> userRoleMap, Map<Id, String> accIdToName){
		Map<Id, User> updatedUsers = new Map<Id, User>();
		for(Id conId : contacts.keySet()){
			updatedUsers.put(conId, setUser(contacts.get(conId).User__c, contacts.get(conId), profileMap.get(contacts.get(conId).Role__c), userRoleMap.get(contacts.get(conId).Role__c), accIdToName.get(contacts.get(conId).AccountId)));
		}
		return updatedUsers;
	}

	private Map<Id, User> handleContactsWithoutUser(Map<Id, Contact> contacts, Map<String, Id> profileMap, Map<String, Id> userRoleMap, Map<Id, String> accIdToName){
		Map<Id, User> newUsers = new Map<Id, User>();
		for(Id conId : contacts.keySet()){
			newUsers.put(conId, setUser(null, contacts.get(conId), profileMap.get(contacts.get(conId).Role__c), userRoleMap.get(contacts.get(conId).Role__c), accIdToName.get(contacts.get(conId).AccountId)));
		}
		return newUsers;
	}

	private Map<Id, Contact> getContactsToBeDisable(Map<Id, Contact> contacts){
		Map<Id, Contact> toReturn = new Map<Id, Contact>();
		for(Id conId : contacts.keySet()){
			if(!contacts.get(conId).Active_User__c){
				toReturn.put(conId, contacts.get(conId));
			}
		}
		return toReturn;
	}

	private Map<Id, Contact> getContactswithUser(Map<Id, Contact> contacts){
		Map<Id, Contact> toReturn = new Map<Id, Contact>();
		for(Id conId : contacts.keySet()){
			if(contacts.get(conId).Active_User__c && contacts.get(conId).User__c != null){
				toReturn.put(conId, contacts.get(conId));
			}
		}
		return toReturn;
	}

	private Map<Id, Contact> getContactsWithoutUser(Map<Id, Contact> contacts){
		Map<Id, Contact> toReturn = new Map<Id, Contact>();
		for(Id conId : contacts.keySet()){
			if(contacts.get(conId).Active_User__c && contacts.get(conId).User__c == null){
				toReturn.put(conId, contacts.get(conId));
			}
		}
		return toReturn;
	}

	private Map<Id, Contact> filterExistContacts(Map<Id, Contact> newMap, Map<Id, Contact> oldMap){
		Map<Id, Contact> toReturn = new Map<Id, Contact>();
		for(Id conId : newMap.keySet()){
			if(newMap.get(conId).Active_User__c && !oldMap.get(conId).Active_User__c){
				toReturn.put(conId, newMap.get(conId));
			}else if(!newMap.get(conId).Active_User__c && newMap.get(conId).User__c != null && oldMap.get(conId).Active_User__c){
				toReturn.put(conId, newMap.get(conId));
			}
			else if (newMap.get(conId).AccountId != oldMap.get(conId).AccountId && newMap.get(conId).User__c != null) {
				toReturn.put(conId, newMap.get(conId));
			}
		}
		return toReturn;
	}

	private void insertUpdateUserForContact(Map<Id, Contact> newMap, Map<Id, Contact> oldMap){
		Map<Id, Contact> filteredContacts = filterExistContacts(newMap, oldMap);
		Set<String> roleNames = getRoleNames(filteredContacts.values());
		Map<String, Id> profileMap = getProfileMap(roleNames);
		Map<String, Id> userRoleMap = getUserRoleMap(roleNames);
		Map<Id, String> accIdToName = getAccountNames(filteredContacts.values());

		Map<Id, Contact> contactsForNewUser = getContactsWithoutUser(filteredContacts);
		Map<Id, Contact> contactsForUpdateUser = getContactswithUser(filteredContacts);
		Map<Id, Contact> contactsForDisableUser = getContactsToBeDisable(filteredContacts);

		Map<Id, User> conIdToNewUser = handleContactsWithoutUser(contactsForNewUser, profileMap, userRoleMap, accIdToName);
		Map<Id, User> conIdToExistUser = handleContactsWithUser(contactsForUpdateUser, profileMap, userRoleMap, accIdToName);
		Map<Id, User> conIdToDisableUser = handleContactsToBeDisable(contactsForDisableUser);

		List<User> usersToUpsert = new List<User>();
		// usersToUpsert.addAll(conIdToNewUser.values());
		usersToUpsert.addAll(conIdToExistUser.values());
		usersToUpsert.addAll(conIdToDisableUser.values());

		Database.DMLOptions dmlOpt = new Database.DMLOptions();
		dmlOpt.EmailHeader.triggerUserEmail = true;
		Database.insert(conIdToNewUser.values(), dmlOpt);
		System.debug(usersToUpsert);
		if(!usersToUpsert.isEmpty()){
			// Database.update(usersToUpsert);
			updateUserAsync(JSON.serialize(usersToUpsert));
		}

		for(Id conId : filteredContacts.keySet()){
			if(conIdToNewUser.containsKey(conId)){
				filteredContacts.get(conId).User__c = conIdToNewUser.get(conId).Id;
			}
		}
	}

	public void insertUpdateUser(List<Contact> newList, Map<Id, Contact> newMap, Map<Id, Contact> oldMap){
		system.debug('~~~~~inserUpdateUser');
		if(Trigger.isInsert){
			createUserForContact(newList);
		}else if(Trigger.isUpdate){
			insertUpdateUserForContact(newMap, oldMap);
		}
	}

	@future
		private static void updateUserAsync(String usersListString){
			List<User> usersToUpsert = (List<User>) JSON.deserialize(usersListString, List<User>.class);
			Database.update(usersToUpsert);
		}
}