({
    init: function(cmp, event, helper) {
        var o = cmp.get("v.LawItem");
        var selectedType = cmp.get("v.LawItem.OwnerType");
        var iconType;
        var buttonText = $A.get("$Label.c.To_the_document_page_Button_Search_page");
        switch (selectedType) {
            case 'Memorandum_of_Law':
                iconType = 'Icon-tzkir';
                buttonText = $A.get("$Label.c.To_the_Memorandum_of_Law_Page");
                break;
            case 'Secondary_Legislation_Draft':
                iconType = 'Icon-mshn';
                buttonText = $A.get("$Label.c.To_the_Secondary_Legislation_Draft_Page");
                break;
            case 'Ministry_Directive_or_Procedure_Draft':
                iconType = 'Icon-nhlm';
                buttonText = $A.get("$Label.c.To_the_Ministry_Directive_or_Procedure_Draft_Page");
                break;
            case 'Support_Test_Draft':
                iconType = 'Icon-mvhn';
                buttonText = $A.get("$Label.c.To_the_Support_Test_Draft_Page");
                break;
        }
        
        cmp.set('v.lawItemImg', iconType);
        cmp.set('v.buttonText', buttonText);
    
        if ( !cmp.get("v.LawItem.IsOpenForComments")  ) {
            cmp.set("v.DateText", $A.get("$Label.c.Closed_for_public_comments_on_after_push"));
        }

        if (cmp.get("v.LawItem.HasFile") == true) {
            if (cmp.get("v.LawItem.FileType") != 'pdf') {
                cmp.set("v.LawItem.FileType", 'word');
            }
        }
    },
    Go2Lawitem: function(cmp, event, helper) {
        window.location.assign("/s/law-item/" +  cmp.get("v.LawItem.Id"));
    }
})