({ 
	doInit: function (component, event) {
		console.log ('doInit');
		var action = component.get("c.CreateComment");
		var recordId = component.get("v.recordId"); 
		console.log('recordId = ' + recordId);
		action.setParams({ "action": "comment", "lawItemId": recordId, "commentId" : "" });
		action.setCallback(this, function (response) {						
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {
				var returnValue = response.getReturnValue();								
				component.set("v.commentId", returnValue);
				console.log(component.get("v.commentId"));
			}
			else {				
				alert("Custom ERROR!!!");
			}

		});
		$A.enqueueAction(action);

		var action2 = component.get("c.getUserInfo");
		action2.setCallback(this, function (response) {						
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {
				try 
				{
					console.log('returned from get user');
					console.log('user:');
					console.log(response.getReturnValue());
					console.log(JSON.parse(response.getReturnValue()));
					var us = new Object();
					us = JSON.parse(response.getReturnValue())[0];
					component.set("v.user", us);
					console.log(component.get("v.user"));	
				} catch (err) 
				{
					console.log('err:');	
					console.log(err.message);	
				}
			}
			else {				
				alert("Custom ERROR!!!");
			}

		});
		$A.enqueueAction(action2);
	},	

	deleteCreatedComment: function (component) {
		
		var commentId = component.get("v.commentId");
		console.log(commentId);
		var action = component.get("c.DeleteComment");
		action.setParams({ "commentId": commentId });
		action.setCallback(this, function (response) {
			component.set("v.myComment", "");
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {				
				var comments = component.get('v.comments');
				for (var i = 0; i < comments["mComments"].length; i++) {
					if (comments["mComments"][i].mId == commentId) {						
						comments["mComments"].splice(i, 1);
					}
				}
				component.set("v.isDeleteOpen", false);
				component.set("v.comments", comments);
				component.set("v.isOpen", false);
				var fireEvent = component.getEvent("newCommentAdded");
				fireEvent.setParams({ "comments": comments });
				fireEvent.fire();
				console.log("DONE inner");
			}
			else {
				alert("Custom ERROR!!!");
			}
		});
		$A.enqueueAction(action);

	},

	addComment: function (component, event, helper) {
		console.log('createComponent');
		component.set('v.ifError', false);
		component.set("v.errorText", '');
		var body = component.get("v.body");
		var i = body.length - 1;
		if (i >= 0 && body[i].get('v.sectionNum') == null && body[i].get('v.sectionComment') == null && body[i].get('v.deleteComp')) {
			component.set('v.ifError', true);
			component.set("v.errorText", 'חובה למלא הערה קודמת');
			return;
		}

		$A.createComponent(
			"c:LawItem_CommentSection",
			{
				"aura:id": "section1",
				"SectionNum": component.get("v.sectionNum"),
				"SectionComment": component.get("v.sectionComment"),
				"subCommentId": component.get("v.subCommentId"),
				"DeleteComp": component.get("v.deleteComp")
			},
			function (newComponent, status, errorMessage) {
				if (status === "SUCCESS") {
					var body = component.get("v.body");
					body.push(newComponent);
					component.set("v.body", body);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
				}
			}
		);
	},


	onChildElementChange: function (component, event, helper) {		
		var body = component.get("v.body");		
		var cnt = 0;
		for (var i = 0; i < body.length; i++) {
			if (body[i].get('v.deleteComp')) {
				cnt++;
			}
		}
		if (cnt == 0) {
			component.set("v.btnText", "הוספה הערה לסעיפים");
		}
		else {
			component.set("v.btnText", "הערה לסעיף נוסף");
		}
	},	

	onSaveData: function (component, event) {
		console.log('onSaveData');
		component.set('v.ifError', false);
		component.set("v.errorText", '');
		console.log('save and close ');
		var recordId = event.getParam("recordId");
		console.log('recordId = ' + recordId);
		var fullText = '';
		var body = component.get("v.body");
		var main = component.get("v.mainText");
		if (main) {
			fullText = main;
		}
		
		var fullTextArray = [];
		var sNum;
		var sBody;
		var sId;
		for (var i = 0; i < body.length; i++) {
			if (body[i].get('v.deleteComp')) {	
				sNum = body[i].get('v.sectionNum');
				sBody = body[i].get('v.sectionComment');
				sId = body[i].get('v.subCommentId');

				fullTextArray.push ({bulletNumber: sNum, body: sBody, subCommentId: sId});

			}
		}


		console.log('onSaveData 2');
		var files = component.get("v.filesList");
		console.log("files");
		console.log(files.length);
		if (fullText == '' && files.length == 0) {
			component.set('v.ifError', true);
			component.set("v.isSaving", false);
			component.set("v.errorText", 'חובה למלא הערה');
			return;
		}

		var status = component.find('status').get('v.statusValue');
		console.log('status = ' + status);
		if (!status || status.length == 0) {
			console.log('return = ' + status);
			component.set('v.ifError', true);
			component.set("v.isSaving", false);
			component.set("v.errorText", 'חובה למלא סטטוס');
			return;
		}
		component.set("v.statusValue", status);	
		console.log('v.statusValue = ' + component.get('v.statusValue'));
		try
		{
			var action = component.get("c.SaveComment");		
			console.log('deletedFiles = ' + component.get("v.deletedFiles"));						
			var subComments = JSON.stringify(fullTextArray);
			console.log('subComments = ' + subComments);
				
			action.setParams({ "body": fullText, "commentId" : component.get("v.commentId"), "status": status, "filesToDelete" : JSON.parse(JSON.stringify(component.get("v.deletedFiles"))), "dispCom" : component.get("v.isDisp"), "subComments": subComments, "deleteComments" : "" });
			action.setCallback(this, function (response) {			
				//component.set("v.myComment", "");
				var state = response.getState();
				console.log("state = " + state);
				console.log("ani po");
				if (component.isValid() && state === "SUCCESS") {

					var returnValue = response.getReturnValue();								
					var	comments = component.get('v.comments');
					comments["mComments"].unshift(returnValue);				
					component.set("v.comments", comments);		
					component.set("v.isOpen", false);
					// var fireEvent = component.getEvent("newCommentAdded");
					// fireEvent.setParams({ "comments": comments });
					// fireEvent.fire();
					console.log("DONE inner");	
					location.reload();			
				}
				else {
					component.set("v.isSaving", false);
					// alert("תקלה לך לF12 מניוק");
					console.log(response.getError());
				}

			});
			$A.enqueueAction(action);
		}catch(err)
		{
			console.log("err:");
			console.log(err.message);
		}	
	}
})