({
    gotoSearch: function (cmp, event, helper) {
        var filter = cmp.get('v.filter');

        if(filter.soon) {
            event.preventDefault();
        }
    },
    Init: function (cmp, event, helper) {
        var filter = cmp.get('v.filter');
        switch (filter.label) {
            case $A.get("$Label.c.Memorandums_Of_Law"):
                cmp.set("v.toolTipTitle", $A.get("$Label.c.Memorandums_Of_Law"));
                cmp.set("v.toolTipBody", $A.get("$Label.c.Memorandums_Of_Law_Tooltip"));
                break;

            case $A.get("$Label.c.Drafts_Legislation"):
                cmp.set("v.toolTipTitle", $A.get("$Label.c.Drafts_Legislation"));
                cmp.set("v.toolTipBody", $A.get("$Label.c.Drafts_Legislation_Tooltip"));
                break;

            case $A.get("$Label.c.Draft_procedures_and_guidelines"):
                cmp.set("v.toolTipTitle", $A.get("$Label.c.Draft_procedures_and_guidelines"));
                cmp.set("v.toolTipBody", $A.get("$Label.c.Draft_procedures_and_guidelines_Tooltip"));  
                break;

            case $A.get("$Label.c.Draft_support_tests"):
                cmp.set("v.toolTipTitle", $A.get("$Label.c.Draft_support_tests"));
                cmp.set("v.toolTipBody", $A.get("$Label.c.Draft_support_tests_Tooltip"));
                break;

            case $A.get("$Label.c.All_Data"):
                cmp.set("v.toolTipTitle", $A.get("$Label.c.All_Data"));
                cmp.set("v.toolTipBody", "");
                break;
        }
    }
})