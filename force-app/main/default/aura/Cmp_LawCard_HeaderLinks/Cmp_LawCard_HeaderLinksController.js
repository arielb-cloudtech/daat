({
    
    Go2Lawitem: function(cmp, event, helper) {
        var item = cmp.get("v.LawItem");
        window.location.assign("/s/law-item/" + item.Id);
    },
    
    shareWithFacebook: function(component, event, helper) {
        try {
            var path = 'https://www.facebook.com/sharer/sharer.php?u=' + location.origin + '/s/law-item/' + component.get("v.LawItem").Id;
            window.open(path, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=700, height=500');
        } catch (error) {
            alert(error.message);
        }
    },

    shareWithWhatsapp: function(cmp, event, helper) {
        var item = cmp.get("v.LawItem");
        var text =  location.origin + '/s/law-item/' + item.Id;
        var path = 'https://api.whatsapp.com/send?text=' + text;                                                      
        window.open(path, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=700, height=500');
    },

    headerMouseOver: function(component, event, helper){
        console.log("ariel","openActions");
        var img = event.target;
        img.setAttribute('data-base-img',img.src)
        if(event.target.getAttribute('data-hover-img')) {
            img.src = event.target.getAttribute('data-hover-img');
        }
    },

    headerMouseOut: function(component, event, helper){
        var img = event.target;
        if(event.target.hasAttribute('data-base-img')) {
            img.src = event.target.getAttribute('data-base-img');
        }
    },

    shareWithGmail: function(component, event, helper) {
        try {
            var path = 'mailto:?body=' + location.origin + '/s/law-item/' + component.get("v.LawItem").Id;
            location.href = path;
        } catch (error) {
            alert(error.message);
        }
    },

    openActions:  function(cmp, event, helper) {
        if($A.util.hasClass(cmp.find("actions_tooltip"), "pop")) {
            $A.util.removeClass(cmp.find("actions_tooltip"), "pop");
        }
        else {
            $A.util.addClass(cmp.find("actions_tooltip"), "pop");
        }
    },

    showShare: function(cmp, event, helper) {
        cmp.set("v.showShare", !cmp.get("v.showShare"));
    }
})