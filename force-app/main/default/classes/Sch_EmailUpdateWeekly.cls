global class Sch_EmailUpdateWeekly implements Schedulable {
	global void execute(SchedulableContext sc) {
		Batch_SendPeriodicalyUpdateEmails b = new Batch_SendPeriodicalyUpdateEmails('7','Hebrew');
		database.executeBatch(b);
		// Batch_SendPeriodicalyUpdateEmails c = new Batch_SendPeriodicalyUpdateEmails('7','Arabic');
		// database.executeBatch(c);
	}
}