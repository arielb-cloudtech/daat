@isTest
private class Test_ContactTrigger extends accountMockUp  {

	@isTest
	static void Test_InsertUpdateContact() {
		registerMockUp();
		id accRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MOJ_Contact').getRecordTypeId();
		Account acc = new Account(RecordTypeId = accRtId, Name = 'sfdg', Sort_Name__c = 'sfdg', IsActive__c = true, IsUnit__c = true);
		insert acc;

		acc = [SELECT Id FROM Account LIMIT 1];
		
		Contact con = new Contact(AccountId = acc.Id, FirstName = 'con', LastName = 'con', Email = 'tocafik240@qmail2.net', Role__c = 'referant', Active_User__c = true);

		Test.startTest();
		insert con;
		con.Active_User__c = false;
		update con;
		Test.stopTest();

	}
}