public with sharing class contactToUpdateQueueable implements Queueable  {

	List <Contact> contactsList;
	public contactToUpdateQueueable(List <Contact> thisContacts) {
		contactsList= thisContacts;	
		system.debug('*** contactsList: ' + contactsList);	
	}

	public void execute(QueueableContext context) {
		if (contactsList != null || !contactsList.isEmpty()){
			try{
				update contactsList;
			}
			catch(exception ex){
				system.debug('*** Error in updating Contact-2: ' + ex);
			}				
		}
	}

}