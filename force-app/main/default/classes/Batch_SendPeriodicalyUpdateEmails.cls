global class Batch_SendPeriodicalyUpdateEmails implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts{
    
    String query;
    public String LastDays;
    public String Language;
    public Map<String,object> BodyParams = new Map<String,object>();
    public Set<Law_Item__c> LawItemSet = new Set<Law_Item__c>();
    public Map<Id,ContentDistribution> lawItem2MainConDis = new Map<Id,ContentDistribution>();
    public Map<String, Map<String,String>> DynamicListMapByLanguageAndFreq = new Map<String, Map<String,String>>();

    global Batch_SendPeriodicalyUpdateEmails(String ld, String lang) {
        LastDays = ld;
        Language = lang;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String s = startSetup(); 
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        System.debug('>>>>>Batch SCOPE: ' + scope);
        if(scope == null || scope.isEmpty()){
            System.debug('>>>>>0 Law Items are found for this summary email');
            return;
        }

        LawItemSet.addAll((List<Law_Item__c>) scope);
        addCurrentConDisMapping((List<Law_Item__c>) scope);
    }
    
    global void finish(Database.BatchableContext BC) {
        
        System.debug('>>>>>Batch LawItems: ' + LawItemSet);
        if(LawItemSet == null || LawItemSet.isEmpty()){
            System.debug('>>>>>0 Law Items are found for this summary email');
            return;
        }

        finishSetup();
        String freq = LastDays.equalsIgnoreCase('1') ? 'Daily' : 'Weekly';
        System.enqueueJob(new Queue_sendSummaryEmails(BodyParams,DynamicListMapByLanguageAndFreq.get(Language).get(freq)));
    }


    public void addCurrentConDisMapping(List<Law_Item__c> myList){
        List<ContentDistribution> cdlist = [Select Id, Name, DistributionPublicURL, ContentVersion.FileExtension, ContentDocumentId, ContentVersion.File_Type__c, RelatedRecordId, ContentVersionId, ContentVersion.ContentDocumentId, ContentVersion.ContentDocument.Title, ContentVersion.IsRia__c, ContentVersion.Main__c  From ContentDistribution WHERE ContentVersion.File_Type__c = 'Main File' AND ContentVersion.IsLatest = true AND RelatedRecordId  In : myList];

        Map<Id,ContentDistribution> MainFile = new Map<Id,ContentDistribution>();
        for(ContentDistribution cd : cdlist){
            if(cd.ContentVersion.File_Type__c != null && cd.ContentVersion.File_Type__c.equalsIgnoreCase('Main File')){
                if(cd.DistributionPublicURL != null && !lawItem2MainConDis.containsKey(cd.RelatedRecordId)){
                    MainFile.put(cd.RelatedRecordId, cd);
                }
            }
        }                                            
        lawItem2MainConDis.PutAll(MainFile);
    }

    public String startSetup(){
        for(DynamicList__c dl : DynamicList__c.getAll().Values()){
            if(!DynamicListMapByLanguageAndFreq.containsKey(dl.Language__c)){
                DynamicListMapByLanguageAndFreq.put(dl.Language__c, new Map<String,String>());
            }               
            DynamicListMapByLanguageAndFreq.get(dl.Language__c).put(dl.Frequency__c,dl.Pardot_List_Id__c);      
        }

        query = 'Select Id, Account_Responsibility__c, Publisher_Account__r.Name, Law_Item_Name__c, Account_Responsibility__r.Name, RecordTypeId, RecordType.Name, Publish_Date__c, Last_Date_for_Comments__c, Name, Document_Brief__c From Law_Item__c';
        //query+= ', (Select Id, ContentDocumentId, LinkedEntity.Name, ContentDocument.LatestPublishedVersion.Id, ContentDocument.LatestPublishedVersion.IsRia__c, ContentDocument.LatestPublishedVersion.Main__c ,ContentDocument.Title, LinkedEntityId FROM ContentDocumentLinks WHERE )';
        query += ' WHERE Status__c=\'Distributed\' AND LastModifiedDate =LAST_N_DAYS:'+LastDays+' ORDER BY Publish_Date__c DESC NULLS LAST';
       
        if(Test.isRunningTest()){
            query = 'Select Id, Account_Responsibility__c, Publisher_Account__r.Name, Law_Item_Name__c, Account_Responsibility__r.Name, RecordTypeId, RecordType.Name, Publish_Date__c, Last_Date_for_Comments__c, Name, Document_Brief__c From Law_Item__c';
            query += ' WHERE Status__c=\'Distributed\' AND LastModifiedDate =LAST_N_DAYS:'+LastDays+' ORDER BY Publish_Date__c DESC LIMIT 100';
        }
       
        return query;
    }

    public void finishSetup(){
        String freq = LastDays.equalsIgnoreCase('1') ? 'Daily' : 'Weekly';
        Boolean isDaily = LastDays.equalsIgnoreCase('1') ? true : false;

        if(Test.isRunningTest()){
            if( LawItemSet == null || LawItemSet.isEmpty() ){
                System.debug('>>>>>Test Adding law item to set');
                LawItemSet.add(new Law_Item__c(Law_Item_Name__c = 'TestLI', RecordTypeId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId(), Publish_Date__c = Datetime.now(), Last_Date_for_Comments__c = Datetime.now(), Name = 'IS RUNNING TEST LI', Document_Brief__c = 'TESTING'));
            }
        }

        System.debug('>>>>>LawItemSet: '+ LawItemSet);
        bodyParams = Util_Email_Templates.SendSummaryEmail(LawItemSet, isDaily, Language, lawItem2MainConDis);
    }
}