public with sharing class Util_ManageGroups {
    public Util_ManageGroups() {

    }

    public void createNewGroup(List<String> groupNames){
        List<Group> newGroups = new List<Group>();

        for(String groupName : groupNames){
            newGroups.add(new Group(Name = groupName));
        }
        insert newGroups;
    }

    public void deleteGroups(List<String> groupNames){
        List<Group> groupsToDelete = [SELECT Id FROM Group WHERE Name IN :groupNames];

        delete groupsToDelete;
    }

}
