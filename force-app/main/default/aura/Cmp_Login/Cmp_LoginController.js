({
    Loginy: function(cmp, evt, helper) {
        console.log("ariel","inn");
        try 
        {
            cmp.set('v.alEmail', "");
            cmp.set('v.alPass', "");
            if(evt.type == 'click' || evt.getParams().keyCode == 13)
            {
                var clientUN = cmp.get("v.username");
                var clientPASS = cmp.get("v.password");
                var action = cmp.get("c.LoginUser");
                var cont = true;
                var firstField = false;
    
                var emailField = cmp.find("emailCmp");
                if ($A.util.isEmpty(clientUN) || $A.util.isUndefined(clientUN)) {
                    emailField.set("v.errors", [{ message: $A.get("$Label.c.Mandatory_field") }]);
                    cont = false;
                    cmp.set('v.alEmail', $A.get("$Label.c.Mandatory_field"));
                    emailField.focus();
                    firstField = true;
                } else {
                    var e = /(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})/;
                    if (!e.test(clientUN)) {
                        emailField.set("v.errors", [{ message: $A.get("$Label.c.Invalid_email") }]);
                        cont = false;
                        cmp.set('v.alEmail', $A.get("$Label.c.Invalid_email"));
                        if(!firstField){
                            emailField.focus();
                        }
                    }
                }
    
                var passwordField = cmp.find("passCmp");

                if ($A.util.isEmpty(clientPASS) || $A.util.isUndefined(clientPASS)) {
                    passwordField.set("v.errors", [{ message: $A.get("$Label.c.Mandatory_field") }]);
                    cont = false;
                    cmp.set('v.alPass', $A.get("$Label.c.Mandatory_field"));
                    if(!firstField){
                         passwordField.focus();
                    }
                } else {
                    if (clientPASS == clientPASS.toUpperCase() || clientPASS.length < 8 || /[^a-zA-Z0-9]/.test(clientPASS)) {
                        passwordField.set("v.errors", [{ message: $A.get("$Label.c.Wrong_password") }]);
                        cont = false;
                        cmp.set('v.alPass', $A.get("$Label.c.Wrong_password"));
                        if(!firstField){
                             passwordField.focus();
                        }
                    }
                }
    
                console.log({
                    username: clientUN,
                    password: clientPASS,
                    Url: ""
                });
                action.setParams({
                    username: clientUN,
                    password: clientPASS,
                    Url: ""
                });
                // if(cmp.get('v.staySignIn')){
                //     var user_and_pass = cmp.get("v.username") + ',' + cmp.get('v.password');
                //     helper.setCookie('KeepMe', user_and_pass, 5);
                //  }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        cmp.set("v.Result", response.getReturnValue());
                        console.log('signIn: ' + mp.get('v.staySignIn'))
                         
    
                    }else if (state === "ERROR") {
                        var errors = response.getError();
                        console.log(errors);
                        passwordField.set("v.errors", [{ message: $A.get("$Label.c.Wrong_password") }]);
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                cmp.set("v.ErrorMsg", errors[0].message);
                                document.getElementById('errorgui').classList.remove("slds-hide");
                                console.log("error: " + errors[0].message);
                            }
                        } else {
                           
                        }
                    }
                });
                $A.enqueueAction(action);   
            }
        } catch (err) 
        {
            console.log("err:");    
            console.log(err.message);    
        }
    },

    Move2Register: function(cmp, event, helper) {
        window.parent.location = "/s/moj-register";
    },

    Move2Google: function(cmp, event, helper) {
        location.href = "/services/auth/sso/Google";
    },

    Move2Facebook: function(cmp, event, helper) {
        location.href = "/services/auth/sso/Facebook";
    },

    handleError: function(cmp, event, helper) {
        var comp = event.getSource();
        $A.util.addClass(comp, "error");
        $A.util.addClass(comp, "errorField");
    },

    handleClearError: function(cmp, event, helper) {
        var comp = event.getSource();
        $A.util.removeClass(comp, "error");
        $A.util.removeClass(comp, "errorField");
    },

    clearEmailError: function(cmp, event, helper) {
        var nameField = cmp.find("emailCmp");
        nameField.set("v.errors", null);
    },

    clearPassError: function(cmp, event, helper) {
        var nameField = cmp.find("passCmp");
        nameField.set("v.errors", null);
    },

    forgotPass: function(cmp, event, helper) {
        var action = cmp.get("c.LostPassword");
        action.setParams({ 'username': cmp.get("v.username") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var u = response.getReturnValue();
                if (u == true) {
                    cmp.set("v.PreRec", false);
                    cmp.set("v.Result", $A.get("$Label.c.You_may_have_accessed_your_spam_box_Subtitle"));
                } else {
                    cmp.set("v.Result", $A.get("$Label.c.Email_does_not_exist_on_the_system"));
                }

            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        cmp.set("v.ErrorMsg", errors[0].message);
                        document.getElementById('errorgui').classList.remove("slds-hide");
                    }
                } else {}
            }
        });
        $A.enqueueAction(action);
    },

    ForgotMode: function(cmp, event, helper) {

        var ForgtMode = cmp.get("v.forgotPassModal");
        cmp.set("v.PreRec", true);
        cmp.set("v.Result", "");

        if (ForgtMode) {
            cmp.set("v.username", "");
            cmp.set("v.password", "");
            cmp.set("v.forgotPassModal", false)

        } else {
            cmp.set("v.username", "");
            cmp.set("v.password", "");
            cmp.set("v.forgotPassModal", true)
        }
    },
    
    showSearchIcon: function(cmp, evt, helper) {
        var s = evt.getParam("showSearch");  
        cmp.set('v.showSearchEv', s);
	},
})