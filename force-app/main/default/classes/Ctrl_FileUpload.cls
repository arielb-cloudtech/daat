public with sharing class Ctrl_FileUpload {
    
    /**
     Delete attachment
    **/
    
    @AuraEnabled	
	public static ReturnObjects.AjaxMessage DeleteAttachment(  String deleteAttachment ) {
        
        ReturnObjects.AjaxMessage response = new ReturnObjects.AjaxMessage();
        Id currentUserId =  UserInfo.getUserId();
        
        Map<String, List<SObject>> deleteMap = new Map<String, List<SObject>> {
            'attachments' => [SELECT Id FROM Attachment WHERE CreatedById  = :currentUserId AND Id=:deleteAttachment ],
            'ContentVersion' => [SELECT Id FROM ContentVersion WHERE CreatedById = :currentUserId AND Id=:deleteAttachment ], 
            'ContentDocument' => [SELECT Id FROM ContentDocument WHERE CreatedById = :currentUserId AND Id=:deleteAttachment ]
        };
        response.message = deleteAttachment;
        
        for ( String deleteType : deleteMap.keySet() ) {
            if ( deleteMap.get(deleteType).size() > 0 ) {
                delete deleteMap.get(deleteType);
                response.setSuccessStatus();
            } 
        }
        
        return response;
    }
    
    /*** 
    Save attachment while supporting chunks.

    While uploading chunks data is saved in attachment. 
    after upload is completed data is save to contentVersion

    If Id is given content will be append to current attachment

    class using the  Cmp_fileUpload

    Params:
        AttachmentId - Id of attachment / ContentVersion
        ParentId - Id of SObject to append the document to
        fileName - name of file
        data - binary file data
        lastUpload - true if this is the last batch for current file
        SObjectType - type of sobject to create (needed to link the file)

    return attachment id
    ***/
    @AuraEnabled	
	public static fileUploadResponse SaveAttachment(  
                                        String AttachmentId, String ParentId, string fileName, 
                                        string data, Boolean IsLastBatch, String parentType ) {	
        
        fileUploadResponse response = new  fileUploadResponse();

        Id currentUserId =  UserInfo.getUserId();
        Blob newChunk = EncodingUtil.base64Decode(EncodingUtil.urlDecode(data, 'UTF-8'));

        // create new SObject to link the data to
        if ( ParentId == null || ParentId == '' )   {
            if ( parentType == 'Case') {
                Case feedBack = Ctrl_ContactUs.createFeedBackCase( null, 'anonymous', 'anonymous', 'no@mail.com', '' , '', '', 'Subject', 'Message' );
                ParentId = feedBack.Id;
            }
            if ( parentType == 'Comment') {
                ClsObjectCreator cls = new ClsObjectCreator(); 
                Comment__c comment  = new Comment__c(Comment_Status__c = 'Published', Display_Comment__c = true);
		        insert comment;
                ParentId = comment.Id;
            }
            if(ParentId  == null || ParentId == '' ) {
                throw new ReturnObjects.MojException('cannot append document without parent SObject information (Id/type)');
            }
        }

        Attachment tempAttachment;
        
        if ( AttachmentId != null && AttachmentId != '' ) {
            List<Attachment> attachments = [SELECT Id, Body, Name, parentId FROM Attachment WHERE Id = :AttachmentId  ];

            if( attachments.size() > 0 ) {
                tempAttachment = attachments[0];
            }
        }

        // Last batch - save data in ContentVersion
        if ( IsLastBatch ) {

            ContentVersion currentAttachment = new ContentVersion();
            currentAttachment.ContentLocation = 'S';
            currentAttachment.PathOnClient = fileName;
            currentAttachment.Title = fileName;
            if( tempAttachment!=null && tempAttachment.Body != null ) {
                newChunk = EncodingUtil.convertFromHex(EncodingUtil.convertToHex(tempAttachment.Body) + EncodingUtil.convertToHex(newChunk));
                delete tempAttachment;
            }
            currentAttachment.VersionData = newChunk;
            
            insert currentAttachment;

            currentAttachment = ([ SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :currentAttachment.Id ])[0];

            response.message = JSON.serialize(currentAttachment);
            
            ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
            contentDocumentLink.ContentDocumentId = currentAttachment.ContentDocumentId;
            contentDocumentLink.LinkedEntityId = ParentId;
            contentDocumentLink.ShareType = 'I'; // Inferred permission
            contentDocumentLink.Visibility = 'AllUsers';

            insert contentDocumentLink;

            AttachmentId = currentAttachment.Id;

        }
        // Batch - save data in attachment
        else {
            if( null == tempAttachment ) {
                tempAttachment = new Attachment();
                tempAttachment.ParentId = ParentId;
                tempAttachment.Name = fileName;
                tempAttachment.Body =  newChunk;
            }
            else {
                tempAttachment.Body = EncodingUtil.convertFromHex(EncodingUtil.convertToHex(tempAttachment.Body) + EncodingUtil.convertToHex(newChunk));
            }
            upsert tempAttachment;
            AttachmentId = tempAttachment.Id;
        }

        response.FileId = AttachmentId;
        response.ParentId = ParentId;
        response.setSuccessStatus();
    
        return response;
    }

    public class fileUploadResponse extends ReturnObjects.AjaxMessage {
		@AuraEnabled public String FileId;
		@AuraEnabled public String ParentId;
	
		public fileUploadResponse(){			
			super();
		}
	}	
}
