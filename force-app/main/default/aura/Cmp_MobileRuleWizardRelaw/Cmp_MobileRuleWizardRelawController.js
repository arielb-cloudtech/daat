({
	doInit : function(cmp, evt, helper) 
	{
		console.log('subssssss2: ');
		cmp.set("v.selectedSubName",  $A.get("$Label.c.Filter_by_subject_law_Search_page"))
		var action = cmp.get("c.getSubs");
			action.setParams({ subSearch : "" });
			action.setCallback(this, function(response) {
            var STATE = response.getState();
            
			if(STATE === "SUCCESS") {
				console.log('subssssss: ');
				console.log(response.getReturnValue());
                var returnValue = JSON.parse(response.getReturnValue()); 
				cmp.set("v.subLst", returnValue);
            }
			
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	},
	checkLaws : function(cmp, evt, helper) 
	{
		console.log("in checklaws");
		try
		{
			var laws = cmp.get("v.lawLst");
			var selLaws = cmp.get("v.selectedLaws");
			var selectedLawsIds = cmp.get("v.selectedLawsIds");
	
			for(const key in laws)
			{
				if(laws[key].isSelected && !selectedLawsIds.includes(laws[key].Id))
				{
					// console.log(laws[key]);
					selectedLawsIds.push(laws[key].Id);
					selLaws.push(laws[key]);
				}else if(!laws[key].isSelected && selectedLawsIds.includes(laws[key].Id))
				{
					for(var i = 0;i < selLaws.length;i++)
					{
						if(selLaws[i].Id == laws[key].Id)
						{
							selLaws.splice(i, 1);
						}
					}
					for(var i = 0;i < selectedLawsIds.length;i++)
					{
						if(selectedLawsIds[i] == laws[key].Id)
						{
							selectedLawsIds.splice(i, 1);
						}
					}
				}
			}
	
			cmp.set("v.selectedLaws", selLaws);
			cmp.set("v.selectedLawsIds", selectedLawsIds);
		}catch(err)
		{
			console.log(err);
		}
	},
	closeFiltersRelaw : function(cmp, evt, helper) 
	{
		setTimeout(() => {
			// cmp.set("v.isFiltersOpen", "true");
			// cmp.set("v.isFiltersOpenRelaw", "false");
			cmp.set("v.inMainWizard", "false");
			cmp.set("v.choice", "4");
			// var selLaws = [];
			// var selectedLawsIds = [];
			// cmp.set("v.selectedLaws", selLaws);
			// cmp.set("v.selectedLawsIds", selectedLawsIds);

			// var laws = [];

			// cmp.set("v.selectRecordName", "");
			// cmp.set("v.selectedSub", "");
			// cmp.set("v.selectedSubName", "");
			// cmp.set("v.lawLst", laws);
		}, 10);
		
		cmp.set("v.isSubFilter", "false");
	},
	back : function(cmp, evt, helper) 
	{
		cmp.set("v.inMain", "false");
		cmp.set("v.choice", "");
		var processRes = $A.get("e.c:showMobileMenuFromBack");
		console.log(processRes);
		processRes.fire();
	},
	clearFilters : function(cmp, evt, helper) 
	{
		cmp.set("v.selectedSubName",  $A.get("$Label.c.Filter_by_subject_law_Search_page"));
		var ddl = cmp.find('ddl');
		$A.util.removeClass(ddl, 'regText');
		$A.util.addClass(ddl, 'searchText');
		cmp.set("v.selectedSub", "");
		// cmp.set("v.selectedSubName", "");
		var selLaws = [];
		var selectedLawsIds = [];
		cmp.set("v.selectedLaws", selLaws);
		cmp.set("v.selectedLawsIds", selectedLawsIds);

		var laws = [];//cmp.get("v.lawLst");

		// for(const key in laws)
		// {
		// 	laws[key].isSelected = false;
		// }

		cmp.set("v.selectRecordName", "");
		cmp.set("v.lawLst", laws);
	},
	openSubFilter : function(cmp, evt, helper) 
	{
		try
		{
			setTimeout(() => {
				console.log(cmp.find("dropdownMenu").getElement());
				cmp.find("dropdownMenu").focus();
			}, 10);
			var isOp = cmp.get("v.isSubFilter");
			cmp.set("v.isSubFilter", !isOp);
		}catch(err)
		{
			console.log(err.message);
		}
	},
	closeSubFilter : function(cmp, evt, helper) 
	{
		// var isOp = cmp.get("v.isSubFilter");
		cmp.set("v.isSubFilter", false);
	},
	selectSub : function(cmp, evt, helper) 
	{
		// var isOp = cmp.get("v.isSubFilter");
		console.log(evt.target.nextSibling.textContent);
		cmp.set("v.selectedSubName", evt.target.textContent);
		var ddl = cmp.find('ddl');
		$A.util.removeClass(ddl, 'searchText');
		$A.util.addClass(ddl, 'regText');
		
		cmp.set("v.selectedSub", evt.target.nextSibling.textContent);
		cmp.set("v.isSubFilter", false);
		try
		{
			var currentText = cmp.get('v.selectRecordName');
			console.log("currentText", currentText);
			console.log("sadf", cmp.get('v.selectRecordId'));
			var resultBox = cmp.find('resultBox');
			if(currentText.length > 0) {
				$A.util.addClass(resultBox, 'slds-is-open');
			}else {
				$A.util.removeClass(resultBox, 'slds-is-open');
			}
			if(true)
			{
				var action = cmp.get("c.getLaws");
						action.setParams({ subSearch : cmp.get('v.selectRecordName'),
											subName : cmp.get("v.selectedSubName") == $A.get("$Label.c.Select_a_law_topic") ?  '' : cmp.get("v.selectedSubName")});
						action.setCallback(this, function(response) {
						var STATE = response.getState();
						if(STATE === "SUCCESS") {
							//console.log(response.getReturnValue());
							var returnValue = JSON.parse(response.getReturnValue()); 
							cmp.set("v.lawLst", returnValue);
						}
						
						else if (state === "ERROR") {
							var errors = response.getError();
							if (errors) {
								if (errors[0] && errors[0].message) {
									console.log("Error message: " + 
												errors[0].message);
								}
							} else {
								console.log("Unknown error");
							}
						}
					});
					
					$A.enqueueAction(action);
			}else
			{
				var lst = [];
				component.set("v.lawLst", lst);
			}
		}catch(err)
		{
			console.log(err.message);
			alert(err.message);
		}
	},

	getButtonComponent : function(component, event, helper) {
		console.log("~~~~~~getButtonComponent~~~~~~");
		var btnCmp = component.find("userinput");
		console.log("btnCmp: ", btnCmp);
		return btnCmp;
	},

	searchField : function(component, event, helper) {
		try
		{
			var currentText = event.getSource().get("v.value");
			console.log("currentText", currentText);
			console.log("sadf", component.get('v.selectRecordId'));
			var resultBox = component.find('resultBox');
			if(currentText.length > 0) {
				$A.util.addClass(resultBox, 'slds-is-open');
			}else {
				$A.util.removeClass(resultBox, 'slds-is-open');
			}
			if(currentText)
			{
				var action = component.get("c.getLaws");
						action.setParams({ subSearch : component.get('v.selectRecordName'),
											subName : component.get("v.selectedSubName") == $A.get("$Label.c.Select_a_law_topic") ?  '' : component.get("v.selectedSubName")});
						action.setCallback(this, function(response) {
						var STATE = response.getState();
						if(STATE === "SUCCESS") {
							//console.log(response.getReturnValue());
							var returnValue = JSON.parse(response.getReturnValue()); 
							component.set("v.lawLst", returnValue);
						}
						
						else if (state === "ERROR") {
							var errors = response.getError();
							if (errors) {
								if (errors[0] && errors[0].message) {
									console.log("Error message: " + 
												errors[0].message);
								}
							} else {
								console.log("Unknown error");
							}
						}
					});
					
					$A.enqueueAction(action);
			}else
			{
				var lst = [];
				component.set("v.lawLst", lst);
			}
		}catch(err)
		{
			console.log(err.message);
			alert(err.message);
		}
    },
    
    setSelectedRecord : function(component, event, helper) {
		console.log("in click");
        var currentText = event.currentTarget.id;
        var resultBox = component.find('resultBox');
		$A.util.removeClass(resultBox, 'slds-is-open');
		console.log(event.currentTarget.dataset.name);
		console.log(currentText);
		// var chosenSubs = component.get("v.chosenSubs");
		// var selectedSub = new Object();
		// selectedSub.Name = event.currentTarget.dataset.name;
		// selectedSub.Id = currentText;
		// selectedSub.label = event.currentTarget.dataset.name;
		// selectedSub.type = 'basic';
		// chosenSubs.push(selectedSub);
		// component.set("v.chosenSubs", chosenSubs);
		// component.set("v.selectRecordName", "");
    },
})