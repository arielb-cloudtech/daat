({
    doInit: function(cmp, evt, helper) {
        var op = [];
			
			var item = {
				label: $A.get("$Label.c.Search_the_Document_Name"),
				value: '1'
			};
			
            op.push(item);
            
            var item = {
				label: $A.get("$Label.c.Search_the_Name_of_the_document_and_the_summary_content"),
				value: '2'
			};
			
            op.push(item);
            
            cmp.set("v.options", op);
            window.setTimeout(
                $A.getCallback(function () {
                    cmp.find("searchField").focus();
                }), 1
            );

    },
    searchKeyUp: function(cmp, evt, helper) {
        if(evt.type == "click" || evt.keyCode == "13")
        {
            helper.submitForm(cmp.find("searchField") , cmp);
            cmp.find("searchField").focus();
        }
    },
    DataSearch: function(cmp, evt, helper) {
        helper.submitForm(cmp.find("searchField"), cmp);
    },
    
})