public with sharing class Utils_EmailDistributionListViaWizard {

    public static List<String> createListOfLawItemIdAndEmailKeys(List<Law_Item__c> lawItemsFromTrigger, String Language){
        Map<String, Law_Item__c> lawItemsMap = new Map<String, Law_Item__c>();
        List<String> lawItemIdAndEmailKeys = new List<String>();
        List<String> emailsList = new List<String>();
        Set<String> uniqueEmailsList;

        for(Law_Item__c li : lawItemsFromTrigger){
                lawItemsMap.put(li.Id, li);
        } 

        for(Law_Item__c li : [SELECT Id, Status__c, Law__c, Type__c, Publisher_Account__c, RIA_Attachment__c, Law_Item__c, Updates_Recipients__c, Extra_Updates_Recipients__c
                                FROM Law_Item__c 
                                WHERE Status__c != null AND Status__c ='Distributed' AND RecordTypeId != null AND Id In :lawItemsMap.keyset()]){
            
            uniqueEmailsList = new Set<String>();

            emailsList = parseEmailsString(li.Updates_Recipients__c, li.Extra_Updates_Recipients__c);

            if(emailsList != null && !emailsList.isEmpty()){
                for(String email : emailsList){
                    email.trim();
                    if(uniqueEmailsList != null && !uniqueEmailsList.contains(email) ){
                        uniqueEmailsList.add(email);
                        lawItemIdAndEmailKeys.add(li.id + '_' + email);
                    }
                }
            }
        }
        return lawItemIdAndEmailKeys; 
    }

    private static List<String> parseEmailsString(String recipients, String extraRecipients){
        List<String> emails = new List<String>();
        List<String> temp = new List<String>();

        if(recipients != null && !String.isEmpty(recipients)){
            temp = recipients.split(';');
            emails.addAll(temp);
        }
        if(extraRecipients != null && !String.isEmpty(extraRecipients)){
            temp = extraRecipients.split(';');
            emails.addAll(temp);
        }
        return emails;
    }

    public static Map<String,List<String>> convertKeysListToEmailsList(List<String> toConvert){
        Map<String,List<String>> convertedMap = new Map<String,List<String>>();
        String liId;
        String email;

        for(String key : toConvert){
            liId = key.substringBefore('_');
            email = key.substringAfter('_');

            if(!convertedMap.containsKey(liId)){
                convertedMap.put(liId, new List<String>());
            }
            convertedMap.get(liId).add(email);
        }
        return convertedMap;
    }
}
