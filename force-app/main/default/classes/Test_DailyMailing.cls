@isTest
public with sharing class Test_DailyMailing {

    @isTest(seeAllData=true)
    public static void testDailyScheduler(){
        Test.setMock(HttpCalloutMock.class, new Test_LawItemTriggerHandler.Mock());

		Test.StartTest();
		Id jobId = System.schedule('Daily Summary', '0 0 15 ? * SUN,MON,TUE,WED,THU,FRI *', new Sch_EmailUpdateDaily() );
		Test.stopTest();
    }
}
