@isTest
public with sharing class Test_Ctrl_SearchResults extends accountMockUp  {

	@TestSetup
	static void makeData(){
		registerMockUp();
		Test.startTest();
		Account acc = new Account(Name = 'sfgd', IsActive__c = true, IsUnit__c = true, Sort_Name__c = 'sfgd');
		insert acc;
		Test.stopTest();

		Id lawRtId = Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Parent_Law').getRecordTypeId();
		Id lawMotherRtId = Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Secondary_Law_Mother').getRecordTypeId();
		Law__c law = new Law__c(RecordTypeId = lawRtId, Law_Name__c = 'asdf', Name = 'asdfgdf',Classifications__c = 'class');
		insert law;
		Law__c lawMother = new Law__c(RecordTypeId = lawMotherRtId, Law_Name__c = 'lawMother', Name = 'lawMother',Classifications__c = 'class');
		insert lawMother;

		Id rtId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId();
		Law_Item__c li = new Law_Item__c(Document_Brief__c = 'asdfsd',
									Last_Date_for_Comments__c = System.now().addDays(35),
									Name = 'asdfsd',
									Law_Item_Name__c = 'asdfsd',
									Publish_Date__c = System.now(),
									Publisher_Account__c = acc.Id,
									Main_File_Name__c = 'MainFile',
									New_or_Exist_Item__c = false,
									No_RIA_Reason__c = 'Unregulated',
									Notify_Users__c = false,
									Number_of_Days_to_Respond__c = '35',
									Ready_for_Disribution__c = false,
									RecordTypeId = rtId,
									RIA_Attachment__c = 'RIA Report Not Required',
									Status__c = 'Distributed',
									Updates_Recipients__c = 'pikov94264@imail5.net',
									X2118_Regulations__c = false);
		insert li;

		Classification__c clsific = new Classification__c(Name = 'class', ClassificiationID__c = '1');
		insert clsific;

		Law_Item_Classification__c lic = new Law_Item_Classification__c(Classification__c = clsific.Id, Law_Item__c = li.Id);
		insert lic;

		LawClassification__c lawclass = new LawClassification__c(ClassificiationID__c = clsific.Id, LawClassificationID__c = '1', IsraelLawID__c = law.Id);
		insert lawclass;

		Related_to_Law__c rtli = new Related_to_Law__c(Law__c = law.Id, Law_Item__c = li.Id);
		insert rtli;

		Comment__c comment = new Comment__c(Is_Private_Comment__c = false, Display_Comment__c = true, Related_to_Law_Item__c = li.Id, Comment_Status__c = 'Published');
		insert comment;

		ContentVersion cv = new ContentVersion(File_Type__c = 'Main File', VersionData = Blob.valueOf('text'), Title = 'file', PathOnClient = 'file.docx');
		insert cv;

		cv = [SELECT ContentDocumentId  FROM ContentVersion WHERE id =: cv.Id LIMIT 1];

		ContentDocumentLink cdl = new ContentDocumentLink(ShareType = 'I', Visibility = 'AllUsers', ContentDocumentId = cv.ContentDocumentId, LinkedEntityId = li.Id);
		insert cdl;
	}



	@isTest
	static void testing(){
		Id rtId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId();
		List<String> lawTypes = new List<String>{rtId};
		List<String> commentsStatusOpen = new List<String>{'Open'};
		List<String> commentsStatusClose = new List<String>{'fghjk'};

		Classification__c clsific = [SELECT Id FROM Classification__c LIMIT 1];
		List<String> classifications = new List<String>{clsific.Id};

		Law__c law = [SELECT Id FROM Law__c LIMIT 1];
		List<String> relatedLaws = new List<String>{law.Id};


		Account acc = [SELECT Id FROM Account LIMIT 1];
		List<String> offices = new List<String>{acc.Id};

		Ctrl_SearchResults.getCSVSearchResults('asdfsd', null, String.valueOf(Date.today().addDays(-90)), String.valueOf(Date.today().addDays(90)), 'title', 'ASC', lawTypes, commentsStatusOpen, classifications, relatedLaws, offices);
		Ctrl_SearchResults.getCSVSearchResults(null, null, String.valueOf(Date.today().addDays(-90)), String.valueOf(Date.today().addDays(90)), 'name', 'DESC', lawTypes, commentsStatusClose, classifications, relatedLaws, offices);

		Ctrl_SearchResults.getLawItems('asdfsd', null, String.valueOf(Date.today().addDays(-90)), String.valueOf(Date.today().addDays(90)), 'title', 'ASC', null, lawTypes, commentsStatusOpen, classifications, relatedLaws, offices);
		Ctrl_SearchResults.getLawItems(null, null, String.valueOf(Date.today().addDays(-90)), String.valueOf(Date.today().addDays(90)), 'name', 'DESC', null, lawTypes, commentsStatusClose, classifications, relatedLaws, offices);

		List<Law_Item__c> liList = [SELECT Id FROM Law_Item__c];
		Ctrl_SearchResults.loadCommentsAndFiles(liList);

		Ctrl_SearchResults.getSubs('class');

		Ctrl_SearchResults.getLaws('asdf', 'class');
		Ctrl_SearchResults.getLaws(null, null);

		Ctrl_SearchResults.getSecLaws('lawMother');

		Ctrl_SearchResults.getData('asdfsd', '1');
		Ctrl_SearchResults.getData('asdfsd', '2');
		Ctrl_SearchResults.getData(null, '2');

		law = [SELECT ID FROM Law__c WHERE Name = 'lawMother' LIMIT 1];

		Ctrl_SearchResults.getRelatedLawsItems('asdfgdf', new List<String>{law.Id}, classifications);

		List<Law__c> laws = [SELECT Id FROM Law__c LIMIT 2];
		Ctrl_SearchResults.getFilters(new List<String>{laws[0].Id, laws[1].Id});

	}

}