trigger ContactTrigger on Contact (before insert, before update, after insert, after update){

     ContactTriggerHandler handler = new ContactTriggerHandler();

    if(Trigger.isBefore){
        if (Trigger.isInsert){            
            handler.insertUpdateUser(Trigger.new, null, null);
        }else if (Trigger.isUpdate){
            handler.insertUpdateUser(null, Trigger.newMap, Trigger.oldMap);
        }
    }
}