public without sharing class ContentDocumentLinkHandler {
	public ContentDocumentLinkHandler() {
		
	}
    public void CreateContentDistribution(List<ContentDocumentLink> newList){

    	Set<Id> linkedLawItems = new Set<Id>();
    	for(ContentDocumentLink cdl : newList){
	        String typeEntity = String.valueOf((cdl.LinkedEntityId).getsObjectType());
	        system.debug('typeEntity '+ typeEntity);
    		if( typeEntity.equalsIgnoreCase('Law_Item__c') ){
	            linkedLawItems.add(cdl.LinkedEntityId);
	        }
    	}
    	system.debug('linkedLawItems '+ linkedLawItems);

    	Set<Id> docs =  new Set<Id>();
    	Map<Id,ContentDocumentLink> ConV2Rel = new Map<Id,ContentDocumentLink>();
		for(Law_Item__c li : [SELECT Id, (SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId, LinkedEntityId FROM ContentDocumentLinks) FROM Law_Item__c WHERE Id IN: linkedLawItems]){     
	        for(ContentDocumentLink cdl : li.ContentDocumentLinks ){
	            ConV2Rel.put(cdl.ContentDocument.LatestPublishedVersionId, cdl);
	            docs.add(cdl.ContentDocumentId);
	        }
	    }
	    system.debug('^^^ ConV2Rel '+ ConV2Rel);


	    Map<Id,ContentDistribution> existing= new Map<Id,ContentDistribution>();
	    for(ContentDistribution cd : [SELECT Id, ContentDocumentId,Name,PreferencesExpires,PreferencesLinkLatestVersion,RelatedRecordId,PreferencesAllowOriginalDownload,PreferencesAllowPDFDownload,PreferencesAllowViewInBrowser,ContentVersionId, PreferencesNotifyOnVisit FROM ContentDistribution WHERE ContentVersionId IN:ConV2Rel.KeySet()]){
	    	existing.put(cd.ContentVersionId, cd);
	    }
	    system.debug('^^^ existing '+ existing);

	    Map<Id,ContentDistribution> conDis  = new Map<Id,ContentDistribution>();
        for(Id conv : ConV2Rel.KeySet()){
            System.debug('^^^ creating conD');
            ContentDistribution conD = new ContentDistribution();
            conD.Name = 'Download Link '+ConV2Rel.get(conv).ContentDocumentId;
            conD.PreferencesExpires = false;
            conD.PreferencesLinkLatestVersion = true;
            conD.RelatedRecordId = ConV2Rel.get(conv).LinkedEntityId;
            conD.PreferencesAllowOriginalDownload = true;
            conD.PreferencesAllowPDFDownload = true;
            conD.PreferencesAllowViewInBrowser = true;
            conD.ContentVersionId = conv;
            conD.PreferencesNotifyOnVisit = false;
            conDis.put(conv,conD);

	        if( existing.containsKey(conv) ){
	        	conDis.put(conv, existing.get( conv) );
	        }else{

	        }
        }
        upsert conDis.values();
        system.debug('^^^conDis.values() '+conDis.values());
    }
}