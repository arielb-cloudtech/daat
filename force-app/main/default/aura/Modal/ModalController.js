({
	doInit: function (cmp, evt, helper) {

		var recordId = cmp.get("v.recordId");
		var action = cmp.get("c.initObject");
		if ( recordId ) {
			action.setParams({ 'recId': recordId });
			cmp.set("v.isEdit", true);
		}
		else {
			var recordTypeOptions = cmp.get("v.recordTypeOptions");
			recordTypeOptions = recordTypeOptions.concat(helper.getPages().map(path=>({"label":path.label, name:path.recoredTypeName })));
			cmp.set("v.recordTypeOptions", recordTypeOptions);
		}
		cmp.set("v.filesForEditMode", {});

		action.setCallback(this, function (response) {
			var status = response.getState();
			if (cmp.isValid() && status === "SUCCESS") {
				var objData = response.getReturnValue();
				var newLawItem = objData.itm;
				var lawLines = [];
				var childLawLines = [];

				cmp.set("v.recordTypeLst", objData.rtLst);
				cmp.set("v.newLawItem", newLawItem);

				if ( newLawItem.Related_to_Laws__r ) {
					for ( var key in newLawItem.Related_to_Laws__r ) {
						var ll = Object.values(JSON.parse(JSON.stringify(newLawItem.Related_to_Laws__r[key])));
						var lawLine = {Id: ll[3]};
						if (ll[3].startsWith("a00")) {
							lawLine.Name = ll[5].Name;
							lawLine.JuncId = ll[2];
						}
						if (ll[3].startsWith("a04")) {
							lawLine.Name = ll[5].Law_Item_Name__c;
							lawLine.JuncId = ll[2];
							lawLines.push(lawLine);
						}
						if (ll[4] == "Parent Law" || ll[4] == "Law") {
							lawLines.push(lawLine);
						} else if (ll[4] == "Secondary Law Mother" || ll[4] == "Secondary Law Daughter") {
							childLawLines.push(lawLine);
						}
					}

					cmp.set("v.lawLines", lawLines);
					cmp.set("v.childLawLines", childLawLines);
				}

				if ( objData.itemFiles.length > 0 ) {
					var files = new Object();
					var ExtraFiles = [];
					objData.itemFiles.forEach( function(itemFile) {
						var type = itemFile.FileType.replace(/ /g, '');
						if (type != "ExtraFile") {
							files[type] = itemFile;
						} else {
							ExtraFiles.push(itemFile);
						}
					});
					if (ExtraFiles != undefined && ExtraFiles.length != 0) {
						cmp.set("v.otherFilesForEdit", ExtraFiles);
					}
					cmp.set("v.filesForEditMode", files);
				}

				if ( newLawItem.RecordType ) {
					cmp.set( "v.currentRecordType", newLawItem.RecordType.Name );
					helper.gotoPage(cmp, 0);
				}
			}
		});
		$A.enqueueAction(action);
	},

	onSelectRecordType: function(cmp) {
        var opLst = cmp.get('v.recordTypeOptions');
        opLst = opLst.filter(op=>op.name!='');
        cmp.set("v.recordTypeOptions", opLst);
	},

	closeError: function (cmp, event) {
		cmp.set("v.showError", 'false');
	},

	scriptsLoaded: function () {
		accessibility_rtl = true;
		pixel_from_side = 20;
		pixel_from_start = 20;
	},

	close: function (cmp, event, helper) {
		cmp.set("v.displayExitModal", "true");
	},

	saveModal: function (cmp, event, helper) {
		var steps = cmp.get("v.pages");
		if ( !steps.find(step=>step.hasError) ) {
			var lawItem = cmp.get("v.newLawItem");
			var pubDate = new Date(lawItem.Publish_Date__c).getTime();
			var lastDate = new Date(lawItem.Last_Date_for_Comments__c).getTime();
			var today = new Date();
			var before = new Date();
			before.setDate(today.getDate());
			var now = before.getTime();
			if (lawItem.Status__c == 'Distributed' && now < lastDate) {
				cmp.set("v.displayNoteModal", "true");
			} else if (lawItem.Status__c != 'Distributed' && (lastDate - now > 0)) {
				cmp.set("v.displaySaveModal", "true");
			} else if (now > pubDate && now > lastDate) {
				cmp.set("v.errorMsg", "תאריך אחרון לתגובות עבר, לא ניתן לבצע שינויים");
				cmp.set("v.showError", "true");
				setTimeout(() => {
					cmp.set("v.showError", "false");
				}, 2000);
			}
		} else {
			cmp.set("v.errorMsg", 'לא ניתן להפיץ מפני שישנם שדות חובה שאינם מלאים');
			var msg = cmp.get("v.errorMsg");
			cmp.set("v.showError", 'true');
			setTimeout(function () {
				cmp.set("v.showError", 'false');
			}, 3000);
		}
	},

	continueSaveModal: function (cmp, event, helper) {
		var lawItem = cmp.get("v.newLawItem");
		if (lawItem.Latest_Changes_Description__c) {
			lawItem.Item_Updated__c = true;
			cmp.set("v.newLawItem", lawItem);
			cmp.set("v.displayNoteModal", "false");
			cmp.set("v.displaySaveModal", "true");
		} else {
			cmp.set("v.errorMsg", "חובה למלא את מהות השינוי");
			cmp.set("v.showError", "true");
			setTimeout(() => {
				cmp.set("v.showError", "false");
			}, 2000);
		}
	},

	closeNoteModal: function (cmp, event, helper) {
		cmp.set("v.displayNoteModal", "false");
	},
	closeSaveModal: function (cmp, event, helper) {
		cmp.set("v.displaySaveModal", "false");
	},
	closeFilesModal: function (cmp, event, helper) {
		cmp.set("v.displayFilesModal", "false");
	},
	closeExitModal: function (cmp, event, helper) {
		cmp.set("v.displayExitModal", "false");
	},

	noSaveCloseModal: function (cmp, event, helper) {
		var isEdit = cmp.get("v.isEdit");
		if (isEdit == false) {
			var action = cmp.get("c.deleteObject");
			action.setParams({ objId: cmp.get("v.newLawItem").Id });
			action.setCallback(this, function (response) {
				var status = response.getState();
				if (cmp.isValid() && status === "SUCCESS") {
					response.getReturnValue();
				}
				cmp.set("v.displayExitModal", "false");
				window.history.back();
				setTimeout(() => {
					window.location.reload();
				}, 100);
			});
			$A.enqueueAction(action);
		} else {
			cmp.set("v.displayExitModal", "false");
			window.history.back();
			setTimeout(() => {
				window.location.reload();
			}, 100);
		}
	},

	saveCloseModal: function (cmp, evt, helper) {
		var isEdit = cmp.get("v.isEdit");
		cmp.set("v.displayExitModal", false);
		var lawItem = cmp.get("v.newLawItem");
		helper.save(cmp, true);
		if ( isEdit == false ) {
			window.location.href = "/" + lawItem.Id;
		} else {
			window.history.back();
			setTimeout(() => {
				window.location.reload();
			}, 100);
		}
	},

	toggleLoader: function (cmp, event, helper) {

		cmp.set('v.Spinner', !cmp.get('v.Spinner'));
	},

	prev: function (cmp, event, helper) {
		helper.gotoPage(cmp, (cmp.get("v.pageIndex")-1));
	},

	next: function ( cmp, evt, helper) {
		helper.gotoPage(cmp, ( cmp.get("v.pageIndex") + 1 ));
	},

	getPageState: function (cmp) {

		var state = cmp.get("v.state");
		var pageNum = cmp.get("v.item");
		cmp.set("v.displayPage", state.pagesStates[pageNum]);
	},

	stepFocus: function (cmp, evt, helper) {
		var indx = evt.getParam('index');
		helper.gotoPage(cmp,indx);
	},

	saveDraft: function (cmp, evt, helper) {
		helper.save(cmp, true);
	},

	publish: function (cmp, evt, helper) {
		var hasError = cmp.get("v.pages").find(page=> page.hasError ) ? true : false ;

		if (!hasError) {
			helper.save(cmp, false);
		}
	}
})