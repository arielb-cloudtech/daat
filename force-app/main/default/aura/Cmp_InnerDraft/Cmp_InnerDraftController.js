({
	edit: function (cmp, event, helper) {
		var id = cmp.get("v.comment.mLawItemId");
		window.location.href = "/s/law-item/" + id + '?draftid=' +  cmp.get("v.comment.mId");
	}
})