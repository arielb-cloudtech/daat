({
	doInit : function(cmp, event, helper) {
		var action = cmp.get("c.getParentLaws");
		action.setParams({"searchVar" : ""});
		action.setCallback(this, function(response) {
			var result = JSON.parse(response.getReturnValue());
			cmp.set("v.parentLawsSearchRes", result);
		});
		$A.enqueueAction(action);

		var action2 = cmp.get("c.getMotherLaws");
		action2.setParams({"searchVar" : ""});
		action2.setCallback(this, function(response) {
			var result = JSON.parse(response.getReturnValue());
			cmp.set("v.childLawsSearchRes", result);
		});
		$A.enqueueAction(action2);
	},

	addLine : function(cmp, evt, helper) {
		var lines = cmp.get("v.lawLines");
		if(lines.length < 5){
			var newLine = {Name:'',Id:'',showResults: false, showError: false }
			lines.push(newLine);
			cmp.set("v.lawLines", lines);
		}
	},

	removeLine : function(cmp, evt, helper) {
		var lines = cmp.get("v.lawLines");
		var lineToRemove = evt.getSource().get("v.value");
		var removedLine = lines.splice(parseInt(lineToRemove), 1)[0];
		var removedLawLines = [];
		removedLawLines = cmp.get("v.removedLawLines");
		removedLawLines.push(removedLine);
		cmp.set("v.removedLawLines", removedLawLines);
		cmp.set("v.lawLines", lines);
	},

	lawChoiceAction : function(cmp, event) {
		var action = cmp.get("c.getChildLaws");
		action.setParams({ parent : cmp.get("v.lawChoice") });
		action.setCallback(this, function(response) {
            var status = response.getState();
            if(cmp.isValid() && status === "SUCCESS") {
				cmp.set("v.childLaws", JSON.parse(response.getReturnValue()));
				cmp.set("v.childLawsSearchRes", JSON.parse(response.getReturnValue()));
			}
		});
		$A.enqueueAction(action);
	},

	handleKeyUp: function (cmp, evt, helper) {
		var lineNum = evt.target.classList[0].substring(8);
		var isEnterKey = true;
		var lawlines = cmp.get("v.lawLines");
		var queryTerm = lawlines[lineNum].Name;
		if (isEnterKey && queryTerm != '') {
			setTimeout(function() {
				var action = cmp.get("c.getParentLaws");
				action.setParams({"searchVar" : queryTerm});
				action.setCallback(this, function(response) {
					var result = JSON.parse(response.getReturnValue());
					cmp.set("v.parentLawsSearchRes", result);
				});
					$A.enqueueAction(action);
			}, 10);
		}
	},

	handleClick: function(cmp, evt){
		var rowNum = evt.target.getAttribute("data-rowindex");
		var lines = cmp.get("v.lawLines");
		lines[parseInt(rowNum)].showResults = true;
		cmp.set("v.lawLines", lines);
	},

	clear : function (cmp, evt) {
		var rowNum = evt.target.getAttribute("data-rowindex");
		var lines = cmp.get("v.lawLines");
		lines[rowNum].showResults = false;
		cmp.set("v.lawLines", lines);
	},

	clickOption: function (cmp, evt, helper) {
		var rowNum = evt.target.classList[0].substring(8);
		var clickedName = evt.target.textContent;
		var clickedId = evt.target.nextSibling.textContent;
		var lawLines = cmp.get("v.lawLines");
		lawLines[rowNum] .Name = clickedName;
		lawLines[rowNum] .Id = clickedId;
		lawLines[rowNum] .showError = false;
		cmp.set("v.lawLines", lawLines);
	},

	addLineChild : function(cmp, evt, helper) {

		var lines = cmp.get("v.childLawLines");
		if(lines.length < 5){
			var newLine = {Name:'',Id:'',showResults: false, showError: false }
			lines.push(newLine);
			cmp.set("v.childLawLines", lines);
		}
	},

	removeLineChild : function(cmp, evt, helper) {
		var lines = cmp.get("v.childLawLines");
		var lineToRemove = evt.getSource().get("v.value");

		if(Number.isInteger(parseInt(lineToRemove))){
			var removedLine = lines.splice(parseInt(lineToRemove), 1)[0];
			var removedLawLines = [];
			removedLawLines = cmp.get("v.removedLawLines");
			removedLawLines.push(removedLine);
			cmp.set("v.removedLawLines", removedLawLines);;
			cmp.set("v.childLawLines", lines);
		}
	},
	handleKeyUpChild: function (cmp, evt, helper) {

		var lineNum = evt.target.classList[0].substring(9);
		var isEnterKey = true;
		var lawlines = cmp.get("v.childLawLines");
		var queryTerm = lawlines[lineNum].Name;
		if (isEnterKey && queryTerm != '') {
			var action = cmp.get("c.getMotherLaws");
			action.setParams({"searchVar" : queryTerm});
			action.setCallback(this, function(response) {
				var status = response.getState();
				var result = JSON.parse(response.getReturnValue());
				cmp.set("v.childLawsSearchRes", result);
			});
			$A.enqueueAction(action);
		}
	},

	handleClickChild: function(cmp, evt){
		var rowNum = evt.target.getAttribute("data-rowindex");
		var lines = cmp.get("v.childLawLines");
		lines[parseInt(rowNum)].showResults = true;
		cmp.set("v.childLawLines", lines);
	},

	clearChild: function (cmp, evt) {
		var rowNum = evt.target.getAttribute("data-rowindex");
		var lines = cmp.get("v.childLawLines");
		lines[rowNum].showResults = false;
		cmp.set("v.childLawLines", lines);
	},

	clickOptionChild: function (cmp, evt, helper) {
		var rowNum = evt.target.classList[0].substring(8);
		var clickedName = evt.target.textContent;
		var clickedId = evt.target.nextSibling.textContent;
		var lawLines = cmp.get("v.childLawLines");
		lawLines[rowNum].Name = clickedName;
		lawLines[rowNum].Id = clickedId;
		lawLines[rowNum].showError = false;
		cmp.set("v.childLawLines", lawLines);
	},

	doValidation : function(cmp, event, helper) {
		
		var lawLines = cmp.get("v.lawLines");
		var lawIds = [];
		var lawLinesNames = [];
		for(const key in lawLines){
			lawLinesNames.push(lawLines[key].Name);
			if(lawLines[key].Id.startsWith("a0A")){
				lawIds.push(lawLines[key].Id);
			}
		}

		var errorCounter = 0;
		var iterator_1;
		var iterator_2;
		for(iterator_1 = 0 ; iterator_1 < lawLines.length ; iterator_1++){
			for(iterator_2 = (iterator_1 + 1) ; iterator_2 < lawLines.length ; iterator_2++){
				if(lawLines[iterator_2].Id == lawLines[iterator_1].Id){
					lawLines[iterator_2].showError = true;
					errorCounter++;
				}
			}
		}
		cmp.set("v.lawLines", lawLines);
	
		var innerCounter2 = 0;
		var childLawLines = cmp.get("v.childLawLines");


		for(iterator_1 = 0 ; iterator_1 < childLawLines.length ; iterator_1++){
			for(iterator_2 = (iterator_1 + 1) ; iterator_2 < childLawLines.length ; iterator_2++){
				if(childLawLines[iterator_2].Id == childLawLines[iterator_1].Id){
					childLawLines[iterator_2].showError = true;
					errorCounter++;
				}
			}
		}
		cmp.set("v.childLawLines", childLawLines);

		if(innerCounter2 == 0){
			cmp.set("v.showChildLawError", "false");
		}
		
		if(errorCounter == 0){
			cmp.set("v.showLawError", "false");
			cmp.set("v.showChildLawError", "false");
			cmp.set("v.showChildLawReqError", "false");
		}

		var popP641 = $A.get("e.c:updateVarsFromP41");
		popP641.setParams({
			"authValues": lawLinesNames 
		});
		popP641.fire();

		return errorCounter == 0;
	}
})