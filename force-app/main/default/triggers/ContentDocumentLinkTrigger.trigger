trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, after insert) {
    if ( Trigger.isBefore ) {
        for(ContentDocumentLink link : Trigger.new)
        { 
            link.Visibility = 'AllUsers'; 
            link.ShareType = 'I';
        }
    }
    else if (Trigger.isAfter ) {

        ContentDocumentLinkHandler Handler = new ContentDocumentLinkHandler();
        Handler.CreateContentDistribution(Trigger.new);
        
    }
}