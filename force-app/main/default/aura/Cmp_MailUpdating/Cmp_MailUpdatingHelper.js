({
	getBaseFilterObject : function() {
		var filters = [
			{
				name : 'type',
				title : $A.get("$Label.c.Document_Type_Search_Page"),
				options: [
					{ label :$A.get("$Label.c.emailnotification_typeSubject_label"), value:'classifications' },
					{ label: $A.get("$Label.c.emailnotification_type_relatedLaws_label"), value:'relatedLaws' },
					{ label: $A.get("$Label.c.emailnotification_type_offices_label"), value:'offices' },
					{ label: $A.get("$Label.c.emailnotification_type_docType_label"), value:'documentType' },
				],
				currentValue: '',
			},
			{
				name : 'documentType',
				title : $A.get("$Label.c.emailnotification_type_docType_label"),
				options : [],
				currentValue: []
			},
			{
				name : 'classifications',
				title : $A.get("$Label.c.emailnotification_typeSubject_label"),
				itemsByFirstChar: [],
				options : [],
				currentValue: []
			}, 
			{
				name : 'offices',
				title : $A.get("$Label.c.emailnotification_type_offices_label"),
				itemsByFirstChar: [],
				options : [],
				currentValue: []
			}, 
			{
				name : 'relatedLaws',
				title :$A.get("$Label.c.emailnotification_type_relatedLaws_label"),
				searchString: '',
				lawTopic: '',
				lawTopics: [],
				options : [],
				currentValue: []
			}
		];

		return filters;
	},
	openWizard: function(cmp){
		var filterList = cmp.get('v.filtersList');
		filterList.forEach(function(filterSection) {
			if( !filterSection.itemsByFirstChar ) {
				filterSection.currentValue = [];
			}
			else {

			}
		});

		var ruleIndex = cmp.get("v.editRuleIndex");
		if( ruleIndex > -1 ){
			var rule = (cmp.get("v.Rules"))[ruleIndex];
			filterList.filter(f=>f.name=='type')[0].currentValue = rule.type;
			filterList.filter(f=>f.name=='documentType')[0].currentValue = rule.docTypes;
		}
		else {
			filterList.filter(f=>f.name=='documentType')[0].currentValue = filterList.filter(f=>f.name=='documentType')[0].options.map(v=>v.value).filter(v=>v!='RIA_only');
		}

		cmp.set('v.filtersList', filterList );
		cmp.set("v.wizardStep", "type");
	}, 
	generateRuleLabel: function(cmp, rule) {
		var re = new RegExp('({[^}.]*})');
		// get pattern of message
		var templateParts = $A.get("$Label.c.notification_labelPattern").split(re);
		var filterList = cmp.get('v.filtersList');

		rule.text = [];
		// replace each placeholder with appropriate string part
		templateParts.forEach(function(templatePart){
			switch( templatePart ) {
				case "{DocTypes}":
					if (rule.type == 'documentType') break;
					var options = filterList.filter(s=>s.name=='documentType')[0].options;
					var selected =  rule.docTypes.filter(s=> s != 'RIA_only');
					
					if( rule.onlyRIA ){
						rule.text.push({ text: $A.get("$Label.c.notification_FilterType_riaOnly"), class:'bold' });
					}
					if( selected.length == 0 ){
						rule.text.push({ text: $A.get("$Label.c.notification_FilterType_all"), class:'bold' });
					}
					else {
						selected.forEach(function( docType, docTypeIndex ){
							// Add document type
							rule.text.push({ text: options.filter(d=> d.value==docType)[0].label , class:'bold' });

							// Add seperator
							if( docTypeIndex < selected.length - 2  ) {
								rule.text.push({ text:$A.get("$Label.c.notification_DocTypes_spliter") });
							}
							else if ( docTypeIndex == selected.length - 2  ) {
								rule.text.push({ text: $A.get("$Label.c.notification_DocTypes_lastSpliter"), class: 'oneword' });
							}
						});
						rule.text.push({ text: $A.get("$Label.c.notification_DocTypes_prefix"), class:'oneword' });
					}
				break;
				case "{filterLabel}":
					switch( rule.type ) {
						case "relatedLaws":
							rule.text.push({ text :$A.get("$Label.c.notification_FilterType_relatedLaws" ) });
						break;
						case "classifications":
							rule.text.push({ text :$A.get("$Label.c.notification_FilterType_classifications" ) });
						break;
						case "documentType":
							rule.text.push({ text :$A.get("$Label.c.notification_FilterType_documentType" ) });
						break;
						case "offices":
							rule.text.push({ text :$A.get("$Label.c.notification_FilterType_offices" ) });
						break;
					}
				break;
				default:
					rule.text.push({ text:templatePart });
			}
		});
		rule.text = rule.text.reverse();
		rule.valueLabels = [];
		// Create a list of selected values labels
		rule.values.forEach( function(selctedValue) {
			rule.valueLabels.push( 
				filterList.filter( s=>s.name==rule.type)[0]
					.options
					.filter(v=>v.Id == selctedValue || v.value == selctedValue )
					.map(v=> v.Name ? v.Name : v.label)[0]
				);
		});
		
	},
	endWizard: function(cmp){
		var ruleIndex = cmp.get("v.editRuleIndex");
		var rules = cmp.get("v.Rules");
		var filterList = cmp.get('v.filtersList');
		var filterType = filterList[0].currentValue;
		var filter = filterList.filter(s=>s.name==filterType)[0];
		var selectedValues = [];
		
		if ( filterType == 'documentType' ) {
			selectedValues = filter.currentValue;
		}
		else {
			selectedValues = filter.options.filter(c=>c.checked).map(c=>c.Id);
		}
		
		var currentItem = {
			type: filterType,
			onlyRIA: filterList.filter(s=>s.name=='documentType')[0].currentValue.indexOf("RIA_only") > -1,
			docTypes: filterList.filter(s=>s.name=='documentType')[0].currentValue,
			values: selectedValues,
			isDeleted: false
		 };
		 
		this.generateRuleLabel(cmp, currentItem);

		if(ruleIndex > -1 && ruleIndex < rules.length ) {
			rules[ruleIndex] = currentItem;
		}
		else {
			rules.push(currentItem)
		}
		cmp.set("v.Rules" , rules);
		cmp.set("v.wizardStep", "");
	}, 
	waitingId: null,
	getRelatedLaws: function(cmp) 
	{
		var action = cmp.get("c.getRelatedLawsItems");
        var filters = cmp.get("v.filtersList");
		var relatedLawsFilter = filters.filter(s=>s.name=='relatedLaws')[0];
		var lawTopics = [];

		if ( relatedLawsFilter.lawTopic ) {
			lawTopics.push(relatedLawsFilter.lawTopic);
		}

		action.setParams({
            search: relatedLawsFilter.searchString,
			lawClassifaction: lawTopics,
			excludeIds: relatedLawsFilter.currentValue.map(i=>i.Id)
        });
		
		action.setCallback(this, function(response) {
			var STATE = response.getState();
			
			if(STATE === "SUCCESS") {
				var responseObj = response.getReturnValue();
				//refresh filter object
                filters = cmp.get("v.filtersList");
				relatedLawsFilter = filters.filter(s=>s.name=='relatedLaws')[0];
				relatedLawsFilter.options = relatedLawsFilter.currentValue.concat(responseObj.relatedLaws);
                cmp.set("v.filtersList", filters);
			}

		});
		var timeOutMilisecs = 1;
		if( this.waitingId ) {
			window.clearTimeout(this.waitingId);
			timeOutMilisecs = 1200;
		}

        this.waitingId = window.setTimeout($A.getCallback(function() {
            if (cmp.isValid()) {
                $A.enqueueAction(action);
            }
        }), timeOutMilisecs);
	},

})