({
	saveRuleHelper : function(cmp, evt, helper) 
	{
		try 
		{
			console.log("in save rule");
			var isEdit = cmp.get("v.isWizardEdit");
			var editIndx = cmp.get("v.editIndex");
			cmp.set("v.showWizard", "false");
			var process = cmp.get("v.chosenProcess");
			switch(process) 
			{
				case '1':
				var newRules = cmp.get("v.ruleLst");
				var chosenSubs = cmp.get("v.chosenSubs");
				var typeSelection = cmp.get("v.typeSelection");
				console.log("chosenSubs");
				console.log(chosenSubs);
				console.log("typeSelection");
				console.log(typeSelection);
				for(const key in chosenSubs)
				{
					if(typeSelection[5] > 0)
					{
						console.log("in type ffff");
						for(const key2 in typeSelection)
						{
							var newRule = new Object();
							newRule.Classification__c = chosenSubs[key].Id;
							newRule.ClassificationName = chosenSubs[key].Name;
							if(typeSelection[key2] == true && key2 != '5')
							{
								console.log("in inner if");
								console.log(typeSelection[key2]);
								console.log("key2");
								console.log(key2);
								switch(key2) 
								{
									case '0':
									newRule.Item_Type__c = $A.get("$Label.c.MemorandumOfLaw");
									break;
									case '1':
									newRule.Item_Type__c = $A.get("$Label.c.Secondary_legislation");
									break;
									case '2':
									newRule.Item_Type__c = $A.get("$Label.c.Draft_test_support");
									break;
									case '3':
									newRule.Item_Type__c = $A.get("$Label.c.Draft_procedure_or_guideline");
									break;
									case '4':
									newRule.Items_With_RIA__c = true;
									break;
								}
								if(isEdit == "true")
								{
									newRule.Id = newRules[editIndx].Id;
									newRules.splice(editIndx, 1, newRule);
								}else
								{
									newRules.push(newRule);
								}
								console.log("newRule1");
								console.log(newRule);
							}
						}
					}else if (typeSelection[5] == 0 || !typeSelection[5])
					{
						var newRule = new Object();
						newRule.Classification__c = chosenSubs[key].Id;
						newRule.ClassificationName = chosenSubs[key].Name;
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
						// newRules.push(newRule);
					}
					console.log("newRules");
					console.log(newRules);
					
				}
				break;
				case '2':
				var newRules = cmp.get("v.ruleLst");
				var chosenSubs = cmp.get("v.selectedLaws");
				var typeSelection = cmp.get("v.typeSelection");
				console.log("chosenSubs");
				console.log(chosenSubs);
				console.log("typeSelection");
				console.log(typeSelection);
				for(const key in chosenSubs)
				{
					if(typeSelection[5] > 0)
					{
						console.log("in type ffff");
						for(const key2 in typeSelection)
						{
							var newRule = new Object();
							newRule.Law__c= chosenSubs[key].Id;
							newRule.LawName = chosenSubs[key].Name;
							if(typeSelection[key2] == true && key2 != '5')
							{
								console.log("in inner if");
								console.log(typeSelection[key2]);
								console.log("key2");
								console.log(key2);
								switch(key2) 
								{
									case '0':
									newRule.Item_Type__c = $A.get("$Label.c.MemorandumOfLaw");
									break;
									case '1':
									newRule.Item_Type__c = $A.get("$Label.c.Secondary_legislation");
									break;
									case '2':
									newRule.Item_Type__c = $A.get("$Label.c.Draft_test_support");
									break;
									case '3':
									newRule.Item_Type__c = $A.get("$Label.c.Draft_procedure_or_guideline");
									break;
									case '4':
									newRule.Items_With_RIA__c = true;
									break;
								}
								console.log("newRule1");
								console.log(newRule);
								if(isEdit == "true")
								{
									newRule.Id = newRules[editIndx].Id;
									newRules.splice(editIndx, 1, newRule);
								}else
								{
									newRules.push(newRule);
								}
								// newRules.push(newRule);
							}
						}
					}else if (typeSelection[5] == 0 || !typeSelection[5])
					{
						var newRule = new Object();
						newRule.Law__c = chosenSubs[key].Id;
						newRule.LawName = chosenSubs[key].Name;
						console.log("newRule2");
						console.log(newRule);
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
						// newRules.push(newRule);
					}
					
				}
				break;
				case '3':
				var newRules = cmp.get("v.ruleLst");
				var chosenSubs = cmp.get("v.chosenOffices");
				var typeSelection = cmp.get("v.typeSelection");
				console.log("chosenSubs");
				console.log(chosenSubs);
				console.log("typeSelection");
				console.log(typeSelection);
				for(const key in chosenSubs)
				{
					if(typeSelection[5] > 0)
					{
						console.log("in type ffff");
						for(const key2 in typeSelection)
						{
							var newRule = new Object();
							newRule.Ministry__c= chosenSubs[key].Id;
							newRule.MinistryName = chosenSubs[key].Name;
							if(typeSelection[key2] == true && key2 != '5')
							{
								console.log("in inner if");
								console.log(typeSelection[key2]);
								console.log("key2");
								console.log(key2);
								switch(key2) 
								{
									case '0':
									newRule.Item_Type__c = $A.get("$Label.c.MemorandumOfLaw");
									break;
									case '1':
									newRule.Item_Type__c = $A.get("$Label.c.Secondary_legislation");
									break;
									case '2':
									newRule.Item_Type__c = $A.get("$Label.c.Draft_test_support");
									break;
									case '3':
									newRule.Item_Type__c = $A.get("$Label.c.Draft_procedure_or_guideline");
									break;
									case '4':
									newRule.Items_With_RIA__c = true;
									break;
								}
								console.log("newRule1");
								console.log(newRule);
								if(isEdit == "true")
								{
									newRule.Id = newRules[editIndx].Id;
									newRules.splice(editIndx, 1, newRule);
								}else
								{
									newRules.push(newRule);
								}
								// newRules.push(newRule);
							}
						}
					}else if (typeSelection[5] == 0 || !typeSelection[5])
					{
						var newRule = new Object();
						newRule.Ministry__c = chosenSubs[key].Id;
						newRule.MinistryName = chosenSubs[key].Name;
						console.log("newRule2");
						console.log(newRule);
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
						// newRules.push(newRule);
					}
					
				}
				break;
				case '5':
				var newRules = cmp.get("v.ruleLst");
				var typeSelection = cmp.get("v.typeSelection");
				var newRule = new Object();
				for(const key2 in typeSelection)
				{
					var newRule = new Object();
					if(typeSelection[key2] == true && key2 != '5')
					{
						console.log("in inner if");
						console.log(typeSelection[key2]);
						console.log("key2");
						console.log(key2);
						switch(key2) 
						{
							case '0':
							newRule.Item_Type__c = $A.get("$Label.c.MemorandumOfLaw");
							break;
							case '1':
							newRule.Item_Type__c = $A.get("$Label.c.Secondary_legislation");
							break;
							case '2':
							newRule.Item_Type__c = $A.get("$Label.c.Draft_test_support");
							break;
							case '3':
							newRule.Item_Type__c = $A.get("$Label.c.Draft_procedure_or_guideline");
							break;
							case '4':
							newRule.Items_With_RIA__c = true;
							break;
						}
						console.log("newRule1");
						console.log(newRule);
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
						// newRules.push(newRule);
					}
				}
				break;
			}
			console.log("newRules");
			console.log(JSON.stringify(newRules));
			cmp.set("v.ruleLst", newRules);

		} catch (err) 
		{
			console.log(err.message);
		}
	}
})