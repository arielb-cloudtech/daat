public without sharing class Utils {
    public static final String BASE_URL = Pardot_Integeration__c.getAll().values()[0].BASE_URL__c;
    public static String APIUserKey = Pardot_Integeration__c.getAll().values()[0].API_Key__c;
    public static String APIKey = '';
    public static String UserEmail = Pardot_Integeration__c.getAll().values()[0].UserName__c;
    public static String UserPassword = Pardot_Integeration__c.getAll().values()[0].Password__c;

    public static String CalloutBuilder(String Operation){
        HttpRequest req = new HttpRequest(); 
        Map<String,String> resultMap = new Map<String,String>();

        Http http = new Http();
        req = authenticationRequest(req);
        System.debug('req:'+req);
        System.debug('req body:'+req.getBody());
        HTTPResponse res = http.send(req);
        system.debug(res.getHeader('Location'));
        if(res.getStatusCode() == 302){
            req.setEndpoint(res.getHeader('Location'));
            res = new Http().send(req);
        }
        system.debug(res);
        system.debug(res.getbody());
        String loginKey;
        if(res.getStatusCode() == 200){
            String StringBody = String.valueOf(res.getbody());

            if(StringBody.containsIgnoreCase('api_key')){
                loginKey = StringBody.substringBetween('<api_key>', '</api_key>');
            }
            APIKey = loginKey;
            system.debug('^^^ loginKey '+ loginKey);
        }

        return loginKey;
    }

    public static HttpRequest authenticationRequest(HttpRequest req){
        Map<String,String> params = new Map<String,String>();
        params.put('email', UserEmail);
        params.put('password', UserPassword);
        params.put('user_key', APIUserKey);

        String RequestBody = '';
        for(String s: params.keySet()){
            RequestBody += s + '=' +EncodingUtil.urlEncode(params.get(s),'UTF-8')+'&';
        }
        RequestBody =RequestBody.removeEnd('&');

        system.debug(RequestBody);
        String endPoint = BASE_URL+'api/login/version/4';/*/?'+ RequestBody+'/'*/
        //String RequestBody = 'email='+ EncodingUtil.urlEncode(UserEmail, 'UTF-8') + '&password='+ EncodingUtil.urlEncode(UserPassword, 'UTF-8')+  '&user_key='+ EncodingUtil.urlEncode(APIUserKey, 'UTF-8') ;
        //[ BLOCK TO BUILD URL]
        req.setMethod('POST');
        req.setEndpoint(endPoint);
        req.setBody(RequestBody);
        /* BLOCK TO SET HEADERS
        req.setHeader('SOAPAction','https://www.simplesms.co.il/webservice/SendMultiSms');*/

        req.setTimeout(120000); /// MAX time Out
        //req.setBody(xmlBuilder(Sender,CliTextMap));
        return req;
    }

    public static String createList(String thisAPIKey){
        
        Map<String,String> BodyParams = new Map<String,String>();
        Map<String,String> HeaderParams = new Map<String,String>();
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        String endPoint = BASE_URL+'api/list/version/4/do/create';/*/?'+ RequestBody+'/'*/
        req.setMethod('GET');
        req.setEndpoint(endPoint);
        
        HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(thisAPIKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
        
        BodyParams.put('name', 'SalesForce List '+String.valueOf(system.now() ));
        String RequestBody = '';
       
        for(String s: BodyParams.keySet()){
            RequestBody += s + '=' +EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8')+'&';
        }
        RequestBody = RequestBody.removeEnd('&');
        req.setBody(RequestBody);

        for(String s: HeaderParams.keySet()){
            req.setHeader(s,HeaderParams.get(s));
        }

        req.setTimeout(120000); /// MAX time Out
        HTTPResponse res = new HTTPResponse();
        res = http.send(req);
        //system.debug(res.getHeader('Location'));
        if(res.getStatusCode() == 302){
            req.setEndpoint(res.getHeader('Location'));
            res = new Http().send(req);
        }
        system.debug(res);
        system.debug(res.getbody());
        String responseBody = String.valueOf(res.getbody());
        if(responseBody.containsIgnoreCase('<errcode="1">')){
            ApiKey = CalloutBuilder('authentication');
            HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(ApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
            req.setHeader('Authorization',HeaderParams.get('Authorization'));
            res = new Http().send(req);
            responseBody = String.valueOf(res.getbody());
        }

        if(responseBody.containsIgnoreCase('<list>') && responseBody.containsIgnoreCase('<id>')){
            return (responseBody.substringBetween('<list>','</list>')).substringBetween('<id>','</id>');
        }

        return null;
    }

    public static String assignMembersToList(String apiKey, String ListId, List<String> prospects, Boolean isNew){
        
        if(prospects!=null && !prospects.isEmpty()){
            system.debug('^^^ (assignMembersToList) apiKey '+ apiKey);
            system.debug('^^^ (assignMembersToList) prospects '+ prospects);
            system.debug('^^^ (assignMembersToList) isNew '+ isNew);

            Map<String,String> BodyParams = new Map<String,String>();
            Map<String,String> HeaderParams = new Map<String,String>();
            HttpRequest req = new HttpRequest();
            Http http = new Http();

            String endPoint = BASE_URL+'api/listMembership/version/4/do/create';/*/?'+ RequestBody+'/'*/
            req.setMethod('POST');
            req.setEndpoint(endPoint);
            
            HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(apiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
            
            BodyParams.put('name', 'SalesForce List '+String.valueOf(system.now() ));
            BodyParams.put('list_id', ListId);
            BodyParams.put('prospect_id', prospects[0]);
            String RequestBody = '';
           
            for(String s: BodyParams.keySet()){
                RequestBody += s + '=' +EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8')+'&';
            }
            RequestBody = RequestBody.removeEnd('&');
            req.setBody(RequestBody);

            for(String s: HeaderParams.keySet()){
                req.setHeader(s,HeaderParams.get(s));
            }

            req.setTimeout(120000); /// MAX time Out
            HTTPResponse res = new HTTPResponse();
            res = http.send(req);
            //system.debug(res.getHeader('Location'));
            if(res.getStatusCode() == 302){
                req.setEndpoint(res.getHeader('Location'));
                res = new Http().send(req);
            }
            system.debug(res);
            system.debug(res.getbody());
            String responseBody = String.valueOf(res.getbody());
            if(responseBody.containsIgnoreCase('<errcode="1">')){
                String newApiKey = CalloutBuilder('authentication');
                HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(newApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
                req.setHeader('Authorization',HeaderParams.get('Authorization'));
                res = new Http().send(req);
                responseBody = String.valueOf(res.getbody());
            }

            if(responseBody.containsIgnoreCase('<list>') && responseBody.containsIgnoreCase('<id>')){
                return (responseBody.substringBetween('<list>','</list>')).substringBetween('<id>','</id>');
            }
        }

        return null;
    }

    public static void sendEmailsToLists(String thisAPIKey, String thisList, Map<String,object> BodyParams ){/*, Set<String> links*/
        system.debug('sendEmailsToLists');

        APIKey = thisAPIKey;
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        system.debug('thisList '+thisList);
        system.debug('ApiKey ' + ApiKey);
        if(APIKey == null || String.isBlank(APIKey)){
            ApiKey = CalloutBuilder('authentication');
        }

        Map<String,String> HeaderParams = new Map<String,String>();
        //BodyParams.put('subject', subject);
        //BodyParams.put('email_template_id', 3872);
        //BodyParams.put('text_content', text_content);
        
        BodyParams.put('campaign_id', Integer.ValueOf(Pardot_Campaign__c.getAll().get('Email Distribution').PardotId__c));
        //BodyParams.put('format', 'json');
        if(thisList!=null){
            BodyParams.put('list_ids[]', Integer.ValueOf(thisList));
        }
        
        Integer otherList;
        if(BodyParams.containsKey('list_id[]CS')){
            otherList = integer.ValueOf(BodyParams.get('list_id[]CS'));
            BodyParams.remove('list_id[]CS');
        }
            
        //HeaderParams.put('list_ids[]', thisList);
        HeaderParams.put('Authorization','Pardot '+'api_key='+EncodingUtil.urlEncode(ApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
        system.debug('Pardot '+'api_key='+EncodingUtil.urlEncode(ApiKey,'UTF-8')+', user_key='+EncodingUtil.urlEncode(APIUserKey,'UTF-8'));
         HeaderParams.put('Accept','*/*');
       
        HeaderParams.put('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Authorization',HeaderParams.get('Authorization'));
        //HeaderParams.put('Content-Type','application/json');
        //req.setHeader('list_ids[]','2518');
        String HtmlBody='';
        String RequestBody = '';
        Map<String, Object> content = new Map<String, Object>();
        for(String s: BodyParams.keySet()){
            if(!s.equalsIgnoreCase('html_content')/* && !s.equalsIgnoreCase('text_content')*/){
                RequestBody += s + '=' +EncodingUtil.urlEncode(String.valueOf(BodyParams.get(s)),'UTF-8')+'&';
            }else{
                //if(s.equalsIgnoreCase('html_content')){
                    //content.put('html_content', EncodingUtil.urlEncode(String.valueOf(BodyParams.get(s)),'UTF-8'));
                    content.put('html_content', String.valueOf(BodyParams.get(s).toString()));
                //}else{
                //    content.put('text_content',String.valueOf(BodyParams.get(s)));
                //}
            }
            //HeaderParams.put(s,EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8'));
            //req.setHeader(s,EncodingUtil.urlEncode(BodyParams.get(s),'UTF-8'));
        }

        HtmlBody = 'html_content='+ EncodingUtil.urlEncode(String.valueOf(content.get('html_content')),'UTF-8');
        //content.put('html_content','Hello World %%unsubscribe%%');
        //HtmlBody = JSON.serialize(content);
        //system.debug(req.getHeader());
        //RequestBody =RequestBody.removeEnd('&');
        if(otherList!=null){
            RequestBody += 'list_ids[]'+'=' +EncodingUtil.urlEncode(String.valueOf(otherList),'UTF-8');
        }else{
            RequestBody =RequestBody.removeEnd('&');
        }
        system.debug('RequestBody '+ RequestBody);
        system.debug('HtmlBody '+ HtmlBody);
        //RequestBody = Json.serialize(BodyParams);

        String endPoint = BASE_URL+'api/email/version/4/do/send/?'+ RequestBody+'/';
        system.debug('endPoint.length '+ endPoint.length());

        //String RequestBody = 'email='+ EncodingUtil.urlEncode(UserEmail, 'UTF-8') + '&password='+ EncodingUtil.urlEncode(UserPassword, 'UTF-8')+  '&user_key='+ EncodingUtil.urlEncode(APIUserKey, 'UTF-8') ;
        //[ BLOCK TO BUILD URL]
        req.setMethod('POST');
        req.setEndpoint(endPoint);
        req.setBody(HtmlBody);
        system.debug('find me '+req.getBody());
        /* BLOCK TO SET HEADERS
        req.setHeader('SOAPAction','https://www.simplesms.co.il/webservice/SendMultiSms');*/

        //system.debug(req.getBody());

        req.setTimeout(120000); /// MAX time Out
        //req.setBody(xmlBuilder(Sender,CliTextMap));
        //return req;
        HTTPResponse res = http.send(req);
        system.debug(res.getHeader('Location'));
        if(res.getStatusCode() == 302){
            req.setEndpoint(res.getHeader('Location'));
            res = new Http().send(req);
        }
        system.debug(res);
        system.debug(res.getbody());
        String loginKey;
        if(res.getStatusCode() == 200){
            /// i dono inform on success?
        }else{
            //// inform user on error
        }
    }

    public static Map<String,Set<String>> ReturnProspectsByLawItems(Set<Id> lawItemIds,String language){
        system.debug('>>>>>lawItemIds: ' + lawItemIds);
        system.debug('>>>>>language: ' + language);
        return collectingProspects(lawItemIds, Language);
    }

    /*
        Returning Map<LawItems -> Prospects>
    */
    private static Map<String,Set<String>> collectingProspects(Set<Id> lawItemIds, String Language){
        System.debug('>>>>>collectingProspects');

        Set<Law_Item__c> DistributedLawItems = new Set<Law_Item__c>();

        Map<String,String> lawItemIdToRecordTypeId = new Map<String,String>();
        Map<String,String> lawItemIdToRecordTypeName = new Map<String,String>();
	    Map<String,String> lawItemToAccount = new Map<String,String>();
        Map<String,Set<String>> lawItemToClassificationsIds = new Map<String, Set<String>>();
        Map<String,String> lawItemToLaw = new Map<String,String>();
        
        Map<String,Map<String,List<Mailing_Subscription__c>>> lawItemTo_ProspectToMailingSubscriptions = new Map<String,Map<String,List<Mailing_Subscription__c>>>();   

        Map<String,Map<String,List<Mailing_Subscription__c>>> lawItemTo_ProspectToMailingSubscriptions_Ministry = new Map<String,Map<String,List<Mailing_Subscription__c>>>();   
        Map<String,Map<String,List<Mailing_Subscription__c>>> lawItemTo_ProspectToMailingSubscriptions_Classifications = new Map<String,Map<String,List<Mailing_Subscription__c>>>();   
        Map<String,Map<String,List<Mailing_Subscription__c>>> lawItemTo_ProspectToMailingSubscriptions_Law = new Map<String,Map<String,List<Mailing_Subscription__c>>>();   
        Map<String,Map<String,List<Mailing_Subscription__c>>> lawItemTo_ProspectToMailingSubscriptions_ItemType = new Map<String,Map<String,List<Mailing_Subscription__c>>>();   

        Map<String,Set<String>> lawItemsToProspectsToReturn = new Map<String,Set<String>>(); 

        Map<String, Set<String>> lawItemToProspects = new Map<String, Set<String>>(); //object to be returned

        Boolean isWithRia = false;
        Boolean shouldBeAdded;
        String pardotProspectId;
        String currentLawItemRecordTypeName;
        String law;

        for(Law_Item__c li : [SELECT Id, Status__c, Law__c, Type__c, Account_Responsibility__c, RIA_Attachment__c, Law_Item__c,(SELECT Name, Id, Law__c, Authorization_Item__c FROM Related_to_Laws__r) RecordTypeId, RecordType.Name, RecordType.DeveloperName, (SELECT Id, Classification__c, Law_Item__c, Classification__r.ClassificiationID__c, Classification__r.Name FROM Law_Item_Classifications__r)  FROM Law_Item__c where Status__c != null AND Status__c ='Distributed' AND RecordTypeId != null AND Id In: lawItemIds]){
            DistributedLawItems.add(li);
            
            Id lawItemId = li.Id;
            
            //initializin Object that is supposed to contain Mailing data
            if(!lawItemTo_ProspectToMailingSubscriptions.containsKey(lawItemId)){
                lawItemTo_ProspectToMailingSubscriptions.put(lawItemId, new Map<String,List<Mailing_Subscription__c>>());
            } else  {
                System.debug('>>>>>Duplicated Law Item !!!');
            }

            //initializin Object TO BE RETURNED
            if(!lawItemsToProspectsToReturn.containsKey(lawItemId)){
                lawItemsToProspectsToReturn.put(lawItemId, new Set<String>());
            } else  {
                System.debug('>>>>>Duplicated Law Item !!!');
            }

            //Initializing maps for DEBUGGING PER CATEGORY START
            
            if(!lawItemTo_ProspectToMailingSubscriptions_Ministry.containsKey(lawItemId)){
                lawItemTo_ProspectToMailingSubscriptions_Ministry.put(lawItemId, new Map<String,List<Mailing_Subscription__c>>());
            } else  {
                System.debug('>>>>>Duplicated Law Item !!!');
            }

            if(!lawItemTo_ProspectToMailingSubscriptions_Classifications.containsKey(lawItemId)){
                lawItemTo_ProspectToMailingSubscriptions_Classifications.put(lawItemId, new Map<String,List<Mailing_Subscription__c>>());
            } else  {
                System.debug('>>>>>Duplicated Law Item !!!');
            }

            if(!lawItemTo_ProspectToMailingSubscriptions_Law.containsKey(lawItemId)){
                lawItemTo_ProspectToMailingSubscriptions_Law.put(lawItemId, new Map<String,List<Mailing_Subscription__c>>());
            } else  {
                System.debug('>>>>>Duplicated Law Item !!!');
            }

            if(!lawItemTo_ProspectToMailingSubscriptions_ItemType.containsKey(lawItemId)){
                lawItemTo_ProspectToMailingSubscriptions_ItemType.put(lawItemId, new Map<String,List<Mailing_Subscription__c>>());
            } else  {
                System.debug('>>>>>Duplicated Law Item !!!');
            }

            //Initializing maps for DEBUGGING PER CATEGORY END
            
            isWithRia = false;
            isWithRia = li.RIA_Attachment__c.equalsIgnoreCase('Attach RIA Report') ? true : false;

            ////// Collecting Mailing Subscription categories STRAT

            //Record type ID
            if(!lawItemIdToRecordTypeId.containsKey(lawItemId)){
                lawItemIdToRecordTypeId.put(lawItemId, li.RecordTypeId);
            }
            System.debug('>>>>>Tomers new lawItemIdToRecordTypeId: ' + lawItemIdToRecordTypeId);

            //Record type Name
            if(!lawItemIdToRecordTypeName.containsKey(lawItemId)){
                lawItemIdToRecordTypeName.put(lawItemId, li.RecordType.DeveloperName);
            }
            System.debug('>>>>>Tomers new lawItemIdToRecordTypeName: ' + lawItemIdToRecordTypeName);

            //Ministry/Account
            if(!lawItemToAccount.containsKey(lawItemId)){
                lawItemToAccount.put(lawItemId, li.Account_Responsibility__c);
            }
            System.debug('>>>>>Tomers new lawItemToAccount: ' + lawItemToAccount);

            //Classifications
            for(Law_Item_Classification__c lic : li.Law_Item_Classifications__r){
                System.debug('>>>>>lic.Classification__c: ' + lic.Classification__c);
                if(lic.Classification__c != null){
                    if(!lawItemToClassificationsIds.containsKey(lawItemId)){
                        lawItemToClassificationsIds.put(lawItemId, new Set<String>());
                    }
                    lawItemToClassificationsIds.get(lawItemId).add(lic.Classification__c );
                }
            }
            System.debug('>>>>>Tomers new lawItemToClassificationsIds: ' + lawItemToClassificationsIds);

            //Law__c
            if(li.Law__c != null){
                if(!lawItemToLaw.containsKey(lawItemId)){
                    lawItemToLaw.put(lawItemId, li.Law__c);
                }
            }
            System.debug('>>>>>Tomers new: lawItemToLaw: ' + lawItemToLaw);

            ////// Collecting Mailing Subscription categories END
            
            currentLawItemRecordTypeName = parseRecordTypeDeveloperName(lawItemIdToRecordTypeName.get(lawItemId));
            System.debug('>>>>>Parser record type developer name: ' + currentLawItemRecordTypeName);

            ////// Collecting Prospects From Mailing Subscriptions STRAT
            
            /*
                Collecting Mailing subscription By Ministry/account (And By Item Type And RIA)
            */
            for(Mailing_Subscription__c ms : [SELECT Id, Pardot_Prospect_Id__c, Ministry__c, Registration_Type__c, Item_Type__c, Items_With_RIA__c FROM Mailing_Subscription__c WHERE Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Registration_Type__c = 'Personal Preferences' AND Ministry__c = :lawItemToAccount.get(lawItemId)  AND Item_Type__c = :currentLawItemRecordTypeName]){
                shouldBeAdded = true;
                pardotProspectId = ms.Pardot_Prospect_Id__c;
                if(ms.Items_With_RIA__c){
                    if(!isWithRia){ //else shouldnt be added
                        shouldBeAdded = false;
                    }
                }

                if(shouldBeAdded){
                    lawItemTo_ProspectToMailingSubscriptions = addTolawItemTo_ProspectToMailingSubscriptions(lawItemTo_ProspectToMailingSubscriptions, lawItemId, pardotProspectId, ms);
                    lawItemTo_ProspectToMailingSubscriptions_Ministry = addTolawItemTo_ProspectToMailingSubscriptions_Category(lawItemTo_ProspectToMailingSubscriptions_Ministry, lawItemId, pardotProspectId, ms);
                    lawItemsToProspectsToReturn = addTolawItemsToProspectsToReturn( lawItemsToProspectsToReturn, lawItemId, pardotProspectId);
                }
            }
            System.debug('>>>>>Tomers new: Collecting Mailing subscription By Ministry/account (And By Item Type And RIA): ' + lawItemTo_ProspectToMailingSubscriptions_Ministry);

            /*
                Collecting Mailing subscription By Law__c (And By Item Type And RIA)
            */
            law = '';
            if(lawItemToLaw != null && !lawItemToLaw.isEmpty()){
                law = lawItemToLaw.get(lawItemId);  
                if(law != null && law != ''){
                    for(Mailing_Subscription__c ms : [SELECT Id, Pardot_Prospect_Id__c, Law__c, Registration_Type__c, Item_Type__c, Items_With_RIA__c FROM Mailing_Subscription__c WHERE Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Registration_Type__c = 'Personal Preferences' AND Law__c = :lawItemToLaw.get(lawItemId) AND Item_Type__c = :currentLawItemRecordTypeName]){
                        shouldBeAdded = true;
                        pardotProspectId = ms.Pardot_Prospect_Id__c;
                        if(ms.Items_With_RIA__c){
                            if(!isWithRia){ //else shouldnt be added
                                shouldBeAdded = false;
                            }
                        }

                        if(shouldBeAdded){
                            lawItemTo_ProspectToMailingSubscriptions = addTolawItemTo_ProspectToMailingSubscriptions(lawItemTo_ProspectToMailingSubscriptions, lawItemId, pardotProspectId, ms);
                            lawItemTo_ProspectToMailingSubscriptions_Law = addTolawItemTo_ProspectToMailingSubscriptions_Category(lawItemTo_ProspectToMailingSubscriptions_Law, lawItemId, pardotProspectId, ms);
                            lawItemsToProspectsToReturn = addTolawItemsToProspectsToReturn( lawItemsToProspectsToReturn, lawItemId, pardotProspectId);
                        }
                    }
                }
            }
            System.debug('>>>>>Tomers new: Collecting Mailing subscription By Law__c (And By Item Type And RIA): ' + lawItemTo_ProspectToMailingSubscriptions_Law);

            /*
                Collecting Mailing subscription By CLASSIFICATION (And By Item Type And RIA)
            */
            for(Mailing_Subscription__c ms : [SELECT Id, Pardot_Prospect_Id__c, Classification__c, Registration_Type__c, Item_Type__c, Items_With_RIA__c FROM Mailing_Subscription__c WHERE Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Registration_Type__c = 'Personal Preferences' AND Classification__c IN :lawItemToClassificationsIds.get(lawItemId) AND Item_Type__c = :currentLawItemRecordTypeName]){
                shouldBeAdded = true;
                pardotProspectId = ms.Pardot_Prospect_Id__c;
                if(ms.Items_With_RIA__c){
                    if(!isWithRia){ //else shouldnt be added
                        shouldBeAdded = false;
                    }
                }

                if(shouldBeAdded){
                    lawItemTo_ProspectToMailingSubscriptions = addTolawItemTo_ProspectToMailingSubscriptions(lawItemTo_ProspectToMailingSubscriptions, lawItemId, pardotProspectId, ms);
                    lawItemTo_ProspectToMailingSubscriptions_Classifications = addTolawItemTo_ProspectToMailingSubscriptions_Category(lawItemTo_ProspectToMailingSubscriptions_Classifications, lawItemId, pardotProspectId, ms);
                    lawItemsToProspectsToReturn = addTolawItemsToProspectsToReturn( lawItemsToProspectsToReturn, lawItemId, pardotProspectId);
                }
            }
            System.debug('>>>>>Tomers new: Collecting Mailing subscription By CLASSIFICATION (And By Item Type And RIA): ' + lawItemTo_ProspectToMailingSubscriptions_Classifications);

            /*
                Collecting Mailing subscription By ITEM TYPE ONLY (And RIA)
            */
            for(Mailing_Subscription__c ms : [SELECT Id, Pardot_Prospect_Id__c, Classification__c, Ministry__c, Law__c, Registration_Type__c, Item_Type__c, Items_With_RIA__c 
                                                FROM Mailing_Subscription__c 
                                                WHERE Pardot_Prospect_Id__c!=null AND Mailing_Language__c =: Language AND Registration_Type__c = 'Personal Preferences' AND 
                                                    (Classification__c = null OR Classification__c = '') AND
                                                    (Ministry__c = null OR Ministry__c = '') AND
                                                    (Law__c = null OR Law__c = '') AND
                                                    Item_Type__c = :currentLawItemRecordTypeName]){
                shouldBeAdded = true;
                pardotProspectId = ms.Pardot_Prospect_Id__c;
                if(ms.Items_With_RIA__c){
                    if(!isWithRia){ //else shouldnt be added
                        shouldBeAdded = false;
                    }
                }

                if(shouldBeAdded){
                    lawItemTo_ProspectToMailingSubscriptions = addTolawItemTo_ProspectToMailingSubscriptions(lawItemTo_ProspectToMailingSubscriptions, lawItemId, pardotProspectId, ms);
                    lawItemTo_ProspectToMailingSubscriptions_ItemType = addTolawItemTo_ProspectToMailingSubscriptions_Category(lawItemTo_ProspectToMailingSubscriptions_ItemType, lawItemId, pardotProspectId, ms);
                    lawItemsToProspectsToReturn = addTolawItemsToProspectsToReturn( lawItemsToProspectsToReturn, lawItemId, pardotProspectId);
                }
            }
            System.debug('>>>>>Tomers new: Collecting Mailing subscription By ITEM TYPE (And RIA): ' + lawItemTo_ProspectToMailingSubscriptions_ItemType);

            ////// Collecting Prospects From Mailing Subscriptions END
        }

        System.debug('>>>>>All collected prospects: ' + lawItemTo_ProspectToMailingSubscriptions);
        System.debug('>>>>>lawItemsToProspectsToReturn: ' + lawItemsToProspectsToReturn);
        System.debug('>>>>>DistributedLawItems: ' + DistributedLawItems);

        return lawItemsToProspectsToReturn;
    }

    public static Map<String,Map<String,List<Mailing_Subscription__c>>> addTolawItemTo_ProspectToMailingSubscriptions(Map<String,Map<String,List<Mailing_Subscription__c>>> lawItemTo_ProspectToMailingSubscriptions, Id lawItemId, String pardotProspectId, Mailing_Subscription__c ms){
        if(!lawItemTo_ProspectToMailingSubscriptions.get(lawItemId).containsKey(pardotProspectId)){
            lawItemTo_ProspectToMailingSubscriptions.get(lawItemId).put(pardotProspectId, new List<Mailing_Subscription__c>());
        }
        lawItemTo_ProspectToMailingSubscriptions.get(lawItemId).get(pardotProspectId).add(ms);
        
        return lawItemTo_ProspectToMailingSubscriptions;
    }

    public static Map<String,Map<String,List<Mailing_Subscription__c>>> addTolawItemTo_ProspectToMailingSubscriptions_Category(Map<String,Map<String,List<Mailing_Subscription__c>>> lawItemTo_ProspectToMailingSubscriptions_Category, Id lawItemId, String pardotProspectId, Mailing_Subscription__c ms){
        if(!lawItemTo_ProspectToMailingSubscriptions_Category.get(lawItemId).containsKey(pardotProspectId)){
            lawItemTo_ProspectToMailingSubscriptions_Category.get(lawItemId).put(pardotProspectId, new List<Mailing_Subscription__c>());
        }
        lawItemTo_ProspectToMailingSubscriptions_Category.get(lawItemId).get(pardotProspectId).add(ms);

        return lawItemTo_ProspectToMailingSubscriptions_Category;
    }

    public static Map<String,Set<String>> addTolawItemsToProspectsToReturn( Map<String,Set<String>> lawItemsToProspectsToReturn, Id lawItemId, String pardotProspectId){
        if(!lawItemsToProspectsToReturn.get(lawItemId).contains(pardotProspectId)){
            lawItemsToProspectsToReturn.get(lawItemId).add(pardotProspectId);
        }

        return lawItemsToProspectsToReturn;
    }

    public static String parseRecordTypeDeveloperName(String name){
        return name.replaceAll('_', ' ');
    }

    public static Date convertStringToDate(String dString){
        try {
            List<String> splitted = dString.split('-');
            Integer year = Integer.valueOf(splitted[0]);
            Integer month = Integer.valueOf(splitted[1]);
            Integer day = Integer.valueOf(splitted[2].split('T')[0]);
            Date d = Date.newInstance(year, month, day);
            return d;
        } catch (Exception ex){
            return null;
        }
    }
}