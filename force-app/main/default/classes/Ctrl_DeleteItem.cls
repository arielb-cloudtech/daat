public class Ctrl_DeleteItem{

	private final Law_Item__c li;
	public Boolean hasError {get; set;}

	public Ctrl_DeleteItem(ApexPages.StandardController controller){
		this.li = (Law_Item__c)controller.getRecord();
		hasError = false;
	}

	public PageReference deleteItem(){
		try {
				li.Status__c = 'Deleted';
				update li;
				PageReference p = new PageReference('/' + li.Id);
				p.setRedirect(true);
				return p;
		} catch (Exception ex){
			hasError = true;
			return null;
		}
	}
}