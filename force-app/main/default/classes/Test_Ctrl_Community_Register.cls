@isTest
public with sharing class Test_Ctrl_Community_Register extends accountMockUp  {
    

    private static List<User> users = [select Id, Name, accountId, profileId from User Where id= :UserInfo.getUserId() LIMIT 1];
    
    @isTest
    public static void testPortalUserRegistration(){
        Test.startTest();
        
        Ctrl_Community_Register.registrationResponse newUserResponse1 = Ctrl_Community_Register.AddPortalUser('0507321212', 'valid', 'validy', 'valid@mail.com', '1Az23!@#@1','', '' );
        Ctrl_Community_Register.registrationResponse newUserResponse2 = Ctrl_Community_Register.AddPortalUser('0507121212', 'invalid', 'duplicateMail', 'valid@mail.com',  '1Az23!@#@1','', '' );
        
       // System.assertEquals( 'success', newUserResponse1.status, newUserResponse1 );
       // System.assertEquals( 'error', newUserResponse2.status, newUserResponse2 );
        
        List<User> portalUsers = [SELECT  Username, MobilePhone, Email, FirstName, LastName, Company__c, Role__c  FROM User WHERE FirstName = 'valid'];
       // System.assertEquals( 1, portalUsers.size() );
        
        //ReturnObjects.AjaxMessage  msg = Ctrl_Community_Register.sendForgottenPassword(portalUsers[0].Name);
        //System.assertEquals('success', msg.status, msg.message);

        Test.stopTest();
    }


    @isTest
    public static void testGetUser(){
        ReturnObjects.MojUser user = Ctrl_Community_Register.getCurrentUser();
         System.assertNotEquals(null, user);
    }

    @isTest
    public static void testForgotMyPassword(){
        String res = Ctrl_Community_Register.forgotMyPassword(UserInfo.getUserId());
    }

    @isTest
    public static void testGetUserData(){ //correct userId
        Ctrl_Community_Register.getCurrentUser();
    }

    @isTest
    public static void testChangePassword(){
        String res = Ctrl_Community_Register.changePassword('kiki', 'kiki', 'teagonet01');
    }

    @isTest
    public static void testRegisterUser(){
         registerMockUp();
        Test.StartTest();
        Account account = new Account(Name='Community Account');
        insert account;
        ReturnObjects.AjaxMessage res = Ctrl_Community_Register.AddPortalUser('0503333333', 'shaul', 'eizenberg', 't@gmail.com', 'rrr', 'gigli', 'manager');
        Test.stopTest();
        // System.assertEquals(res.status, 'success');
    }

    @isTest
    public static void testGetMailingRules(){
        Ctrl_Community_Register.getMailingRules();
    }

    @TestSetup
    static void makeData(){
        registerMockUp();
        //cls obj creator instance to create/ret records
        ClsObjectCreator cls = new ClsObjectCreator();
        //accounts
        List<Account> accs = new List<Account>();
        accs.add(cls.returnAccount('Community Account'));
        insert accs;

        List<Contact> contacts = new List<Contact>();
        contacts.add(cls.returnContact('mylname', UserInfo.getUserId()));
        insert contacts;
        //mailing subscription
        List<Mailing_Subscription__c> msList = new List<Mailing_Subscription__c>();
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Memorandum of Law'));
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Secondary Legislation Draft'));
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Support Test Draft'));
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Ministry Directive or Procedure Draft'));
        insert msList;
    }
}