public with sharing class Batch_SendEmailToWizardList implements Database.Batchable<String>, Database.AllowsCallouts{
    public List<Law_Item__c> lawItemsFromTrigger; 
    public String Language; 
    
    public Batch_SendEmailToWizardList(List<Law_Item__c> lawItemsList, String Lang){
        this.lawItemsFromTrigger = lawItemsList;
        this.Language = Lang;
    }

    public Iterable<String> start(Database.BatchableContext info){
        List<String> scope =  Utils_EmailDistributionListViaWizard.createListOfLawItemIdAndEmailKeys(this.lawItemsFromTrigger, this.Language);
        return scope;
    }

    /* 
        If scope is null or Empty - execute method wont be executed
        Scope is List of keys: LawItemId_Email.
        It is done to return a single scope to be iterated, and avoid chaining jobs that could exceed SF limits.
    */
    public void execute(Database.BatchableContext info, List<String> scope){
        Map<String,List<String>> emailsMap = Utils_EmailDistributionListViaWizard.convertKeysListToEmailsList(scope);
        Map<String,String> pardotHtmlTemplateMap;
        String template;
        String subject;

        Messaging.SingleEmailMessage[] emailMsgs = new Messaging.SingleEmailMessage[]{}; 
        Messaging.SingleEmailMessage emailMsg;

        // OrgWideEmailAddress owea = [select Id from OrgWideEmailAddress where Address = 'support@gov.il' LIMIT 1];

        /*
            sendEmail per Law Item.
            The template is built according to the current law item.
        */
        for(String liId : emailsMap.keySet() ){
            emailMsg = new Messaging.SingleEmailMessage();
      		emailMsg.setToAddresses(emailsMap.get(liId));
            pardotHtmlTemplateMap = Util_Email_Templates.SendEmail(liId, this.Language, null, null, null, null, null, null, 'WizardDistributionList');
            template = pardotHtmlTemplateMap.get('html_content');
            subject = pardotHtmlTemplateMap.get('subject');

            emailMsg.setSubject(subject);
            emailMsg.setHtmlBody(template);

            // if(owea != null){
            //     emailMsg.setOrgWideEmailAddressId(owea.Id);
            // } else{
            //     System.debug('>>>>>Problem! org wide address hasnt been defined properly!');
            // }
            
            emailMsgs.add(emailMsg);
        }
        
		Messaging.SendEmailResult[] results = Messaging.sendEmail(emailMsgs, true);
    }

    public void finish(Database.BatchableContext info){
    }
}
