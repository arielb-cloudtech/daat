@isTest
public with sharing class Test_Ctrl_Community_MyAccount extends accountMockUp  {
    
    @isTest
    public static void testGetUser(){
        registerMockUp();
       
		Test.startTest();
        // create Account
        Account account = new Account(Name = 'The ministry of something');
        insert account;

        // Create Users
        Profile profile = [SELECT Id from Profile where Name='Customer Community MOJ']; 

        

        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;

        List<Contact> contacts = new List<Contact> {
            new Contact(FirstName='tester1', Email=uniqueName+'1@mojmail.test', LastName ='tester1', AccountId = account.Id),
            new Contact(FirstName='tester2', Email=uniqueName+'2@mojmail.test', LastName ='tester2', AccountId = account.Id)
        };
        insert contacts;  

        List<User> users = new List<User> {
            new User(LastName='one', UserName= uniqueName+'1@mojmail.test', ContactId=contacts[0].Id, LocaleSidKey='iw_IL', Alias = 'standt',  ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', LanguageLocaleKey='en_US',  EmailEncodingKey='UTF-8', Email='testuser1@moj.mail' , FirstName = 'user'),
            new User(LastName='two', UserName= uniqueName+'2@mojmail.test', ContactId=contacts[1].Id, LocaleSidKey='iw_IL', Alias = 'standt',  ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', LanguageLocaleKey='en_US',  EmailEncodingKey='UTF-8', Email='testuser2@moj.mail' , FirstName = 'user')
        };
        insert users;
        

        List<ContentVersion> cvs = new List<ContentVersion>();
        cvs.add(new ContentVersion(Title = 'myCv', PathOnClient = 'myCv.txt', versionData = Blob.valueOf('kiki')));
        insert cvs;

        List<Law_Item__c> liList = new List<Law_Item__c>();
        liList.add(new Law_Item__c(Law_Item_Name__c = 'pro', Status__c='Distributed', Account_Responsibility__c = account.Id));
        insert liList;

        //for testUpdtCmt & getmailingdata
        List<Comment__c> comments = new List<Comment__c> {
            new Comment__c(OwnerId = users[0].Id, Comment_By__c = users[0].contactId, Comment_Text__c = 'user1 comment 1', Comment_Status__c = 'Published', Related_to_Law_Item__c = liList[0].Id),
            new Comment__c(OwnerId = users[1].Id, Comment_By__c = users[1].contactId, Comment_Text__c = 'user2 text 1', Comment_Status__c = 'Published', Related_to_Law_Item__c = liList[0].Id),
            new Comment__c(OwnerId = users[0].Id, Comment_By__c = users[0].contactId, Comment_Text__c = 'user1 comment 2', Comment_Status__c = 'Published', Related_to_Law_Item__c = liList[0].Id),
            new Comment__c(OwnerId = users[0].Id, Comment_By__c = users[0].contactId, Comment_Text__c = 'user1 comment 3', Comment_Status__c = 'Published', Related_to_Law_Item__c = liList[0].Id),
            new Comment__c(OwnerId = users[1].Id, Comment_By__c = users[1].contactId, Comment_Text__c = 'user2 text 2', Comment_Status__c = 'Published', Related_to_Law_Item__c = liList[0].Id)
        };

        insert comments;
        
        List<Comment_Reply__c> commentsReplys = new List<Comment_Reply__c>{
            new Comment_Reply__c(Reply_to_Comment__c = comments[0].Id, Read__c = true),
            new Comment_Reply__c(Reply_to_Comment__c = comments[3].Id, Read__c = false),
            new Comment_Reply__c(Reply_to_Comment__c = comments[1].Id, Read__c = true)
        };

        insert commentsReplys;

        //something overrides comment status and assigns published
        //plus there are field that are not displayed, maybe because of contact id for community users and user lookup from contact to internal
    
        //continue - for get data - files 
        
        String cdId = [select ContentDocumentId from ContentVersion LIMIT 1].ContentDocumentId;
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        cdlList.add(new ContentDocumentLink(ContentDocumentId = cdId, LinkedEntityId = comments[1].Id, Visibility = 'ALLUsers', ShareType = 'I'));
        insert cdlList;
        Ctrl_Community_MyAccount.MyAccountCountersResponse response;
        
        List<String> excludeIds = new List<String>();
        String filter = 'total';

        System.runAs( users[0]) {
            response = Ctrl_Community_MyAccount.getUserComments(filter, excludeIds);
            system.assertEquals( 3, response.Comments.size(), response );
            system.assertEquals( 3, response.Counters.get('total') );
            system.assertEquals( 1, response.Counters.get('unread') );
            
            filter = 'unread';
            response = Ctrl_Community_MyAccount.getUserComments(filter, excludeIds);
            system.assertEquals( 1, response.Comments.size() );
            system.assertEquals( 3, response.Counters.get('total') );
            system.assertEquals( 1, response.Counters.get('unread') );
        }

        System.runAs(users[1]) {
            filter = 'total';
            response = Ctrl_Community_MyAccount.getUserComments(filter, excludeIds);
            system.assertEquals( 2, response.Comments.size() );
        }
        Test.stopTest();
    }
}