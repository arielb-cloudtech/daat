({
	radioChangeAction : function(cmp, event, helper) {
		if (cmp.get("v.radioValue") == "exists") {
			var lawItem = cmp.get("v.newLawItem");
			lawItem.Law__r = { Name:"", Id: null };
			cmp.set("v.newLawItem" , lawItem );
		}
		else {
			cmp.set("v.searchVar", "");
			helper.clearSelectedLaw(cmp);
		}
	},

	doInit : function(cmp, event, helper) {

		var lawItem = cmp.get("v.newLawItem");

		if( lawItem.Law__r ) {
			cmp.set("v.radioValue", "exists");
			if ( lawItem.Law__c ) {
				cmp.set("v.searchVar", lawItem.Law__r.Law_Name__c);
				cmp.set("v.parentLawsSearchRes", [{ Name:lawItem.Law__r.Law_Name__c, Id: lawItem.Law__c }] );
			}
		}
	},

	onKeyUp: function(cmp, evt , helper ) {
		var currentItemIndex = cmp.get("v.selectedItemIndex");
		var results = cmp.get("v.parentLawsSearchRes");

		if( evt.keyCode == 40 || evt.keyCode == 38 ){ // UP / DOWN
			currentItemIndex += (evt.keyCode == 40? 1: -1);
			cmp.set("v.selectedItemIndex" , currentItemIndex );
			cmp.set("v.searchVar", results[currentItemIndex].Name );
			return;
		}
		else if ( evt.keyCode == 13) { // Enter
			helper.selectLaw(cmp, results[currentItemIndex].Id, results[currentItemIndex].Name);
			return;
		}
	},

	onLawChange: function(cmp, evt , helper ) {
		var newLawItem = cmp.get("v.newLawItem");
		var currentText = cmp.get("v.searchVar");
		var queryTerm = cmp.find('enter-search1').get('v.value');
		
		var results = cmp.get("v.parentLawsSearchRes");
	
			// clear selected 
		if ( newLawItem.Law__c ) {
			if ( results.find(e=>e.Id==newLawItem.Law__c).Name !=currentText) {
				helper.clearSelectedLaw(cmp);
			}
		}

		// fetch results
		var action = cmp.get("c.getParentLaws");
		action.setParams({"searchVar" : queryTerm});
		action.setCallback(this, function(response) {
			var result = JSON.parse(response.getReturnValue());
			cmp.set("v.parentLawsSearchRes", result);
			cmp.set("v.showResults", true);
			cmp.set("v.selectedItemIndex" , -1 );
		});
		$A.enqueueAction(action);
	},

	clickOption: function (cmp, evt, helper) {
		var clickedId = evt.target.nextSibling.textContent;
		var clickedText = evt.target.textContent;
		helper.selectLaw(cmp, clickedId, clickedText);
	},

	doValidation : function(cmp, event, helper) {
		var lawItem = cmp.get("v.newLawItem");

		if ( cmp.get("v.radioValue") == "exists" && ( lawItem.Law__c == null || lawItem.Law__r.Id == null ) ) {
			lawItem.Name = "";
			cmp.set("v.newLawItem", lawItem);
			cmp.set("v.showLawError", true);
			return false;
		} 

		cmp.set("v.showLawError", false);
		return true;
	},
})