({
	initOpts : function(cmp, evt, helper) 
	{
		try 
		{
			var options = [];
			var opt1 = new Object();
			opt1.label = $A.get("{!$Label.c.Open_For_Public_Comments_Checkbox_Search_Page}");
			opt1.value = "פתוח";
			opt1.dispVal = "פתוח";
			options.push(opt1);
			var opt2 = new Object();
			opt2.label = $A.get("{!$Label.c.Closed_for_Public_Comments_Checkbox_Search_Page}");
			opt2.value = "סגור";
			opt1.dispVal = "סגור";
			options.push(opt2);
			var opt3 = new Object();
			opt3.label = $A.get("{!$Label.c.All_Search_Page}");
			opt3.value = "הכל";
			opt3.dispVal = "הכל";
			options.push(opt3);
			cmp.set("v.options", options);
			cmp.set("v.value", 'הכל');
		} catch (err) 
		{
			console.log("err:");	
			console.log(err.message);	
		}
		
	},
	clearClassChoice : function(cmp, evt, helper) 
	{
		var clear = $A.get("e.c:ClearClassChoice");	
		console.log(clear);
		clear.fire();
		helper.getResults(cmp, evt, helper);
	},
	clearOfficeChoice : function(cmp, evt, helper) 
	{
		var clear = $A.get("e.c:ClearOfficeChoice");
		console.log(clear);
		clear.fire();
		helper.getResults(cmp, evt, helper);
	},
	clearSpecificClassChoice : function(cmp, evt, helper) 
	{
		var clear = $A.get("e.c:ClearSpecificClassChoice");	
		console.log(clear);
		clear.setParams({cls : evt.getParam("item").Name});
		clear.fire();
		helper.getResults(cmp, evt, helper);
	},
	clearSpecificOfficeChoice : function(cmp, evt, helper) 
	{
		var clear = $A.get("e.c:ClearSpecificOfficeChoice");
		console.log(clear);
		clear.setParams({off : evt.getParam("item").Name});
		clear.fire();
		helper.getResults(cmp, evt, helper);
	},
	clearLawChoice : function(cmp, evt, helper) 
	{
		var clear = $A.get("e.c:ClearLawChoice");
		console.log(clear);
		clear.fire();
		helper.getResults(cmp, evt, helper);
	},
	getResults : function(cmp, evt, helper) 
    {
        try
		{
			cmp.set("v.showSpinner", "true");
			console.log("im here");
			var indxRecType = window.location.search.indexOf("recType");
			var recType = decodeURIComponent(window.location.search.substring(indxRecType).substring(8,9));
			//var recType= '1';
			// evt.stopPropagation();
			// var LawItems = cmp.get("v.LawItems");
			// var liIdsLst = [];
			// for(const key in LawItems)
			// {
			// 	liIdsLst.push(LawItems[key].Itemcard.Id);
			// }
			var itmTypeChoices = cmp.get("v.itmTypeChoicesObj");
			if(evt.getParam("itmTypeChoices"))
			{
				itmTypeChoices = evt.getParam("itmTypeChoices");
			}
			var rtCounter = 0;
			for(const key in itmTypeChoices)
			{
				if(itmTypeChoices[key].isSelected == true)
				{
					rtCounter++;
				}
			}
			itmTypeChoices.numOfChoices = rtCounter;
			var startDate = cmp.get("v.startDate");
			var endDate = cmp.get("v.endDate");
			var chosenSubs = cmp.get("v.chosenSubs");
			var selectedLaws = cmp.get("v.selectedLaws");
			var selectedSecLaws = cmp.get("v.selectedSecLaws");
			var selectedOffices = cmp.get("v.selectedOffices");
			var openForComments = cmp.get("v.value");
			var ria = cmp.get("v.RIAFilter");
			var ordb = cmp.get("v.orderBy");
			if(ordb)
			{
				ordb = "Desc";
			}
			if(chosenSubs.length == 0)
			{
				chosenSubs = evt.getParam("classes");
			}
			console.log("selectedOffices22: ");
			console.log(selectedOffices);
	
			var filterObj = new Object();
			filterObj.rtChoices = itmTypeChoices
			filterObj.distDateStart = startDate;
			filterObj.distDateEnd = endDate;
			filterObj.chosenClassifications = chosenSubs;
			filterObj.relatedLaws = selectedLaws;
			filterObj.relatedSecLaws = selectedSecLaws;
			filterObj.selectedOffices = selectedOffices;
			filterObj.openForComments = openForComments;
			filterObj.RIAFilter = ria;
			filterObj.orderBy = ordb;
			console.log(JSON.stringify(filterObj));
			
			console.log("searchval: ");
			// var searchVal = cmp.get("v.searchVal");
			var indx = window.location.search.indexOf("search4");
			var searchVal = "%" + decodeURIComponent(window.location.search.substring(indx).substring(8)).replace(/\+/g, " ") + "%";
			var indxType = window.location.search.indexOf("type");
			var type = decodeURIComponent(window.location.search.substring(indxType).substring(5,6));
			filterObj.searchVar = "%%";
			// if(!recType)
			// {
			// 	filterObj.searchVar = searchVal;
			// }else
			// {
			// 	filterObj.searchVar = "%%";
			// }
			if(recType != '1' && recType != '2' && recType != '3' && recType != '4')
			{
				filterObj.searchVar = searchVal;
			}
			console.log(searchVal);
			var action = cmp.get("c.getLawItems");
			action.setParams({ filters : JSON.stringify(filterObj),
								searchType : type});
			action.setCallback(this, function(response) {
			var STATE = response.getState();
			
			if(STATE === "SUCCESS") {
			
				console.log("i have returned");
				console.log(response.getReturnValue());
				if(response.getReturnValue() != "empty")
				{
					var returnValue = JSON.parse(response.getReturnValue()); 
					// cmp.set("v.LawItems", returnValue);
					console.log(returnValue.length);
					var processRes = $A.get("e.c:ProcessRes");
					processRes.setParams({lawItems : returnValue});
					console.log(processRes);
					processRes.fire();
					// cmp.set("v.showSpinner", "false");							
				}else
				{
					var lis = [];
					cmp.set("v.lawItems", lis);
					var processRes = $A.get("e.c:ProcessRes");
					processRes.setParams({lawItems : lis});
					console.log(processRes);
					processRes.fire();
					// cmp.set("v.showSpinner", "false");							

				}

			}
			
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + 
									errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
			});
			cmp.set("v.isFiltersOpen", "false");
			$A.enqueueAction(action);
			// evt.pause();
		}catch(err)
		{
			console.log("error: ");
			console.log(err.message);
		}
			
	}
})