@isTest
public with sharing class Test_Ctrl_DeleteItem {


    @TestSetup
    static void makeData(){

        Id rtId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId();
        List<Law_Item__c> liList = new List<Law_Item__c>();
        liList.add(new Law_Item__c(Document_Brief__c = 'asdf',
									Last_Date_for_Comments__c = System.now().addDays(35),
									Law_Item_Name__c = 'asdfsd',
									// Legal_Counsel_Name__c = contacts[0].Id,
									Main_File_Name__c = 'MainFile',
									// Main_File_URL__c = '/' + docId,
									New_or_Exist_Item__c = false,
									No_RIA_Reason__c = 'Unregulated',
									Notify_Users__c = false,
									Number_of_Days_to_Respond__c = '35',
									// Publisher_Account__c = acc.Id,
									Ready_for_Disribution__c = false,
									RecordTypeId = rtId,
									RIA_Attachment__c = 'RIA Report Not Required',
									Status__c = 'Distributed',
									Updates_Recipients__c = 'pikov94264@imail5.net',
									X2118_Regulations__c = false));
        insert liList;
    }

    @isTest
    static void testing(){
        Law_Item__c li = [SELECT Id FROM Law_Item__c LIMIT 1];
        PageReference pref = Page.VF_deleteItem;
		// pref.getParameters().put('id',liId);
		Test.setCurrentPage(pref);
		Test.startTest();
        ApexPages.StandardController con = new ApexPages.StandardController(li);
        Ctrl_DeleteItem myCtrl = new Ctrl_DeleteItem(con);
        myCtrl.deleteItem();
        Test.stopTest();

    }
    @isTest
    static void testing1(){
        Id rtId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId();
       Law_Item__c li = new Law_Item__c(Document_Brief__c = 'asdf',
									Last_Date_for_Comments__c = System.now().addDays(35),
									Law_Item_Name__c = 'asdfsd',
									// Legal_Counsel_Name__c = contacts[0].Id,
									Main_File_Name__c = 'MainFile',
									// Main_File_URL__c = '/' + docId,
									New_or_Exist_Item__c = false,
									No_RIA_Reason__c = 'Unregulated',
									Notify_Users__c = false,
									Number_of_Days_to_Respond__c = '35',
									// Publisher_Account__c = acc.Id,
									Ready_for_Disribution__c = false,
									RecordTypeId = rtId,
									RIA_Attachment__c = 'RIA Report Not Required',
									Status__c = 'Distributed',
									Updates_Recipients__c = 'pikov94264@imail5.net',
									X2118_Regulations__c = false);

        PageReference pref = Page.VF_deleteItem;
		// pref.getParameters().put('id',liId);
		Test.setCurrentPage(pref);
		Test.startTest();
        ApexPages.StandardController con = new ApexPages.StandardController(li);
        Ctrl_DeleteItem myCtrl = new Ctrl_DeleteItem(con);
        myCtrl.deleteItem();
        Test.stopTest();

    }


}
