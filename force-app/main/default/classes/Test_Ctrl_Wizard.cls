@isTest
public with sharing class Test_Ctrl_Wizard {
	public class MockGenerator implements HttpCalloutMock{

        public HttpResponse respond(HttpRequest req){
            HttpResponse res = new HttpResponse();
			if(req.getEndpoint().contains('hebcal')){
                res.setBody('{"{"gy":2019,"gm":3,"gd":11,"hy":5779,"hm":"Adar II","hd":4,"hebrew":"ד׳ בַּאֲדָר ב׳ תשע״ט","events":["Parashat Vayikra"]}');
                res.setStatusCode(200);
			}else if(req.getMethod() == 'GET'){
                res.setBody('{"size":1,"totalSize":1,"done":true,"queryLocator":null,"entityTypeName":"GlobalValueSet","records":[{"attributes":{"type":"GlobalValueSet","url":"/services/data/v42.0/tooling/sobjects/GlobalValueSet/0Nt1t000000TtQtCAK"},"Id":"0Nt1t000000TtQtCAK"}]}');
                res.setStatusCode(200);
            }else if(req.getMethod() == 'POST'){
                res.setBody('');
                res.setStatusCode(204);
            }
            return res;
        }
    }

	@TestSetup
	static void makeData(){
		Test.setMock(HttpCalloutMock.class, new MockGenerator());
        Test.startTest();
		//data creator obj
		ClsObjectCreator cls = new ClsObjectCreator();
		//accounts
		List<Account> accList = new List<Account>();
		accList.add(cls.returnAccount('name'));
		accList[0].IsUnit__c = true;
		//accList[0].IsActive__c = true;
		accList[0].Sort_Name__c = 'sortName';
		insert accList[0];
		//Contact
		List<Contact> contacts = new List<Contact>();
		contacts.add(cls.returnContact('Jamchi', UserInfo.getUserId()));
		contacts[0].AccountId = accList[0].Id;
		contacts.add(cls.returnContact('Jamchi22222222', UserInfo.getUserId()));
		contacts[1].AccountId = accList[0].Id;
		insert contacts;
		Test.stopTest();
		//laws
		RecordType parentLawRt = [Select Id From RecordType Where DeveloperName = 'Parent_Law' Limit 1];
		RecordType parentLawRt2 = [Select Id From RecordType Where DeveloperName = 'Secondary_Law_Mother' Limit 1];
		RecordType childLawRt = [Select Id From RecordType Where DeveloperName = 'Law' Limit 1];
		List<Law__c> laws = new List<Law__c>();
		laws.add(cls.returnLaw());
		laws[0].Law_Name__c = 'p';
		laws[0].LawValidityDesc__c = 'תקף';
		laws[0].RecordTypeId = parentLawRt.Id;
		laws.add(cls.returnLaw());
		laws[1].Law_Name__c = 'p';
		laws[1].RecordTypeId = parentLawRt2.Id;
		laws.add(cls.returnLaw());
		laws[2].RecordTypeId = childLawRt.Id;
		laws.add(cls.returnLaw());
		insert laws;
		//law binding
		List<LawBinding__c> lbList = new List<LawBinding__c>();
		lbList.add(cls.returnLawBinding(laws[2].Id, laws[1].Id));
		lbList.add(new LawBinding__c(LawID__c = laws[1].Id, ParentLawID__c = laws[0].Id));

		insert lbList;
		//Classifications
		List<Classification__c> classifications = new List<Classification__c>();
		classifications.add(cls.returnClassification());
		insert classifications;
		//law classification
		List<LawClassification__c> lcList = new List<LawClassification__c>();
		lcList.add(cls.returnLC(classifications[0].Id, laws[0].Id));
		insert lcList;
		//law items
		//String rtId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByName().get('Memorandum of Law').getRecordTypeId();
		List<Law_Item__c> liList = new List<Law_Item__c>();
		liList.add(cls.returnLawItem('Draft', 'wow1p', accList[0].Id));
		liList[0].Law__c = laws[0].Id;
		liList[0].Publish_Date__c = Datetime.newInstance(2011, 11, 11);
		liList[0].Last_Date_for_Comments__c = Datetime.newInstance(2012, 11, 11);
		liList[0].Last_Date_for_Comments__c = null;
		liList.add(cls.returnLawItem('Draft', 'wow2p', accList[0].Id));
		liList.add(cls.returnLawItem('Draft', 'wow3p', accList[0].Id));
		liList.add(cls.returnLawItem('Draft', 'wow4p', accList[0].Id));
		//liList[3].RecordTypeId = rtId;
		insert liList;
		//law item classification
		List<Law_Item_Classification__c> licList = new List<Law_Item_Classification__c>();
		licList.add(cls.returnLIClassification(classifications[0].Id, liList[3].Id));
		insert licList;
		//cv cd cdl
		List<ContentVersion> cvList = new List<ContentVersion>();
		cvList.add(cls.returnContentVersion('name', 'name.txt', 'vd'));
		cvList.add(cls.returnContentVersion('name2', 'name2.txt', 'vd'));
		cvList.add(cls.returnContentVersion('name3', 'name3.txt', 'vd'));
		cvList.add(cls.returnContentVersion('name4', 'name4.txt', 'vd'));
		cvList.add(cls.returnContentVersion('name5', 'name5.txt', 'vd'));
		cvList[4].File_Type__c = 'Main File';
		insert cvList;
		List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
		cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[0].Id Limit 1].ContentDocumentId, laws[0].Id));
		cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[1].Id Limit 1].ContentDocumentId, laws[0].Id));
		cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[2].Id Limit 1].ContentDocumentId, laws[0].Id));
		cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[3].Id Limit 1].ContentDocumentId, laws[0].Id));
		cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[4].Id Limit 1].ContentDocumentId, liList[3].Id));
		insert cdlList;
		//related to law
		List<Related_to_Law__c> rtlList = new List<Related_to_Law__c>();
		rtlList.add(cls.returnRelatedToLaw(liList[3].Id, laws[3].Id));
		insert rtlList;
	}

	@isTest
	public static void test(){
		Id liId1 = [Select Id From Law_Item__c Where Law_Item_Name__c = 'wow1p' Limit 1].Id;
		Id liId2 = [Select Id From Law_Item__c Where Law_Item_Name__c = 'wow2p' Limit 1].Id;
		Id accId1 = [Select Id From Account Where Name='name'].Id;
		Id liId3 = [Select Id From Law_Item__c Where Law_Item_Name__c = 'wow3p' Limit 1].Id;
		Id lawId = [Select Id From Law__c Limit 1].Id;
		Id cdId = [Select ContentDocumentId From ContentDocumentLink Where LinkedEntityId = :lawId Limit 1].ContentDocumentId;
		Id parentLawId = [Select Id From Law__c where RecordType.DeveloperName = 'Secondary_Law_Mother' Limit 1].Id; //there is only one currently w/ this RT
		Law_Item__c lawItem = [Select Id From Law_Item__c Where Law_Item_Name__c = 'wow4p' Limit 1];
		Ctrl_Wizard cw = new Ctrl_Wizard();

		Ctrl_Wizard.initObject(null);
		Ctrl_Wizard.initObject(liId1);

		//catch exception - doesnt work
		Ctrl_Wizard.getAdvisor(null);
		//correct account id
		Ctrl_Wizard.getAdvisor(accId1);

		Ctrl_Wizard.deleteObject(liId3);

		Ctrl_Wizard.deleteFileServer(cdId);

		Ctrl_Wizard.populateClasses(liId1, new List<Id> { lawId });

		//inc is true
		Ctrl_Wizard.getParentLaws('p', 'true');
		//inc is false
		Ctrl_Wizard.getParentLaws('p', 'false');

		Ctrl_Wizard.getSpecLaw(lawId);

		Ctrl_Wizard.getMotherLaws('p');

		Ctrl_Wizard.getChildLaws(parentLawId);

		Ctrl_Wizard.getLawItems();

		Ctrl_Wizard.getObjects();

		//exception
		Ctrl_Wizard.updateFileDescs('catchException');
		//correct values
		List<Ctrl_Wizard.fileLine> flines = new List<Ctrl_Wizard.fileLine>();
		List<ContentDocument> conDocs= [Select Id, Description From ContentDocument Limit 1];
		for(Integer i = 0; i< conDocs.size();i++){
			flines.add(new Ctrl_Wizard.fileLine());
			flines[i].Id = conDocs[i].Id;
			flines[i].Description = conDocs[i].Description;
		}
		Ctrl_Wizard.updateFileDescs(JSON.serialize(flines));

		List<Ctrl_Wizard.classification> classLst = new List<Ctrl_Wizard.classification>();
		List<Law_Item_Classification__c> lawIC = [Select Classification__c From Law_Item_Classification__c Limit 1];
		for(Integer i=0; i<lawIC.size();i++){
			classLst.add(new Ctrl_Wizard.classification());
			classLst[i].sfId = lawIC[i].Classification__c;
		}
		Ctrl_Wizard.LawLineContainer lw = new Ctrl_Wizard.LawLineContainer();
		lw.lawLines = new List<Ctrl_Wizard.lawLine>();
		lw.lawLines.add(new Ctrl_Wizard.lawLine());
		lw.lawLines[0].Id = lawId;
		lw.lawLines[0].IsLi = false;
		lw.lawLines.add(new Ctrl_Wizard.lawLine());
		lw.lawLines[1].Id = lawId;
		lw.lawLines[1].IsLi = false;
		List<String> removedClsLst = new List<String>();
		for(Law_Item_Classification__c lic : [Select Classification__c From Law_Item_Classification__c Where Law_Item__c =: lawItem.Id Limit 1]){
			removedClsLst.add(lic.Classification__c);
		}
		Ctrl_Wizard.LawLineContainer childLawLines = new Ctrl_Wizard.LawLineContainer();
		childLawLines.lawLines = lw.lawLines;
		Ctrl_Wizard.LawLineContainer removedLawLines = new Ctrl_Wizard.LawLineContainer();
		removedLawLines.lawLines = lw.lawLines;

		//correct
		Ctrl_Wizard.dateTimeParser('2017-07-21 00:00:00');
		//null
		Ctrl_Wizard.dateTimeParser(null);

		Ctrl_Wizard.getUsers();

		Ctrl_Wizard.getLawsP41();

		Law__c motherLaw = [SELECT Id FROM Law__c WHERE RecordType.DeveloperName = 'Secondary_Law_Mother' LIMIT 1];

		Ctrl_Wizard.getParentLaws('P', 'true', motherLaw.Id);

		Law_Item__c lawItemForFile = [SELECT Id FROM Law_Item__c WHERE Law_Item_Name__c = 'wow4p' LIMIT 1];
		ContentDocument doc = [SELECT Id FROM ContentDocument LIMIT 1];
		Ctrl_Wizard.updateFileTypes(lawItemForFile.Id, new List<String>{doc.Id}, 'OtherFile', null);
		Ctrl_Wizard.updateFileTypes(lawItemForFile.Id, new List<String>{doc.Id}, 'NegligibleEffectFile', null);
		Ctrl_Wizard.updateFileTypes(lawItemForFile.Id, new List<String>{doc.Id}, 'Combined Version File', null);
		Ctrl_Wizard.updateFileTypes(lawItemForFile.Id, new List<String>{doc.Id}, 'HomeshFile', null);
		Ctrl_Wizard.updateFileTypes(lawItemForFile.Id, new List<String>{doc.Id}, 'RIAFile', null);
		Ctrl_Wizard.updateFileTypes(lawItemForFile.Id, new List<String>{doc.Id}, 'Main File', null);

		// Law_Item__c liToSave = new Law_Item__c();
		// String lawItem, String dates, String lawLines, String childLawLines, String removedLawLines, String classLst, String removedClsLst, String mainFileId
		Id MemorandumofLawRtId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId();
		Id conID = [SELECT Id FROM Contact WHERE AccountId = :accId1 LIMIT 1].Id;
		Law_Item__c newLawItem = new Law_Item__c();
		insert newLawItem;

		Id lawLineId = [SELECT Id FROM Law_Item__c WHERE Id != :newLawItem.Id LIMIT 1].Id;
		Id  clsificId = [SELECT Id FROM Classification__c LIMIT 1].Id;

		String newLawItemStr = '{"Name":"placeholder","Id":"' + newLawItem.Id + '","Law__c":null,"Publisher_Account__c":"' + accId1 + '","Legal_Counsel_Name__c":"' + conID + '","Law_Item_Name__c":"דכגע","Document_Brief__c":"דגכע","Number_of_Days_to_Respond__c":21,"Updates_Recipients__c":"sfg@sdfg.dsfg","Publish_Date__c":"2019-12-26T08:00:00.000Z","Last_Date_for_Comments__c":"2020-01-16T23:59:00.000Z","RIA_File_URL__c":null,"No_RIA_Reason__c":"Unregulated","Five_Year_Plan_URL__c":null,"Reason_for_Gov_Decision__c":null,"No_RIA_Other_Reason__c":null,"RIA_File_Name__c":null,"Five_Year_Plan_File_Name__c":null,"Negligible_Impact_Name__c":null,"No_RIA_Other_Name__c":null,"RIA_Attachment__c":"RIA Report Not Required","X2118_Regulations__c":false,"Status__c":"Distributed","RecordTypeId":"' + MemorandumofLawRtId + '"}';
		String dates = '{"pubDate":"2019-12-26T08:00:00.000Z","lastDate":"2020-01-16T23:59:00.000Z"}';
		String lawLines = '[{"Name":"321","Id":"' + lawLineId + '","showResults":false,"showError":"false"}]';
		String childLawLiness = '[{"Name":"","Id":""}]';
		String removedLawLiness = '[{"Name":"גדכעדגכע","Id":"","showResults":false,"showError":"false"},{"Name":"דכגעדגכע","Id":"","showResults":false,"showError":"false"}]';
		String classLstt = '[{"type":"basic","label":"1","Name":"1","sfId":"' + clsificId + '"}]';
		String removedClsLstt = '[]';
		String mainFileId = doc.Id;
		Ctrl_Wizard.saveLawItem(newLawItemStr, dates, lawLines, childLawLiness, removedLawLiness, classLstt, removedClsLstt, mainFileId);

		Ctrl_Wizard.updateFileTypes(new List<String>{doc.Id}, 'OtherFile');

		Ctrl_Wizard.deleteOldFile(doc.Id);
	}
}