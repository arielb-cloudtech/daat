global class Sch_EmailUpdateDaily implements Schedulable {
	global void execute(SchedulableContext sc) {
		Batch_SendPeriodicalyUpdateEmails b = new Batch_SendPeriodicalyUpdateEmails('1','Hebrew');
		database.executeBatch(b);
		// Batch_SendPeriodicalyUpdateEmails c = new Batch_SendPeriodicalyUpdateEmails('1','Arabic');
		// database.executeBatch(c);
	}
}