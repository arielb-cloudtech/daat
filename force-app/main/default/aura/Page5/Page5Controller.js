({
	
	doInit : function(cmp, event, helper) {
	},

	handleUploadFinishedSingle: function (cmp, event) {
        var uploadedFiles = event.getParam("files");
		var lawItem = cmp.get("v.newLawItem");
		var filesForEditMode = cmp.get("v.filesForEditMode");
		var oldFileId = filesForEditMode.CombinedVersionFile ? filesForEditMode.CombinedVersionFile.Id : null;

		var action = cmp.get("c.updateFileTypes");

		action.setParams({ 
			"lawItemId": lawItem.Id ,
			"filesId": [ uploadedFiles[0].documentId ],
			"fileType": "CombinedVersionFile",
			"oldFileId": oldFileId
		});
		action.setCallback(this, function (response) {
			var status = response.getState();
			if (cmp.isValid() && status === "SUCCESS") {
				filesForEditMode.CombinedVersionFile = {Name: uploadedFiles[0].name, Id: uploadedFiles[0].documentId};
				cmp.set("v.filesForEditMode", filesForEditMode);
				lawItem.Combined_File_URL__c = "/" + uploadedFiles[0].documentId;
				cmp.set("v.newLawItem", lawItem);
			}
		});
		$A.enqueueAction(action);
	},

	closeFilesModal : function(cmp, event, helper) {
		cmp.set("v.displayFilesModal", "false");
	},

	handleUploadFinishedMulti: function (cmp, event) {
		cmp.set("v.displayFilesModal", "true");
		cmp.set("v.updatePressed", "false");
		var uploadedFiles = event.getParam("files");
		var otherFilesForEdit = cmp.get("v.otherFilesForEdit");
		
		for (const key in uploadedFiles){
			var newObj = new Object();
			newObj.Name = uploadedFiles[key].name;
			newObj.Id = uploadedFiles[key].documentId;
			otherFilesForEdit.push(newObj);
		}
		cmp.set("v.otherFilesForEdit", otherFilesForEdit);
		var action = cmp.get("c.updateFileTypes");
		var cvs = [];
		for(const key in uploadedFiles){
			cvs.push(uploadedFiles[key].documentId);
		}
		action.setParams({ cvId : cvs,
						   fType : "Extra File" });
		action.setCallback(this, function(response) {
			var status = response.getState();
			var result = response.getReturnValue();
		});
		$A.enqueueAction(action);
	},

	deleteFileSingle : function(cmp, event, helper) {
		var filesForEditMode = cmp.get("v.filesForEditMode");
		var action = cmp.get("c.deleteFileServer");
		action.setParams({ objId : filesForEditMode.CombinedVersionFile.Id });
		action.setCallback(this, function(response) {
			filesForEditMode.CombinedVersionFile = null;
			cmp.set("v.filesForEditMode", filesForEditMode);
		});
		$A.enqueueAction(action); 	
	},

	deleteFileMulti : function(cmp, evt, helper) {
		var rowNum = evt.target.classList[0];
		var otherFilesForEdit = cmp.get("v.otherFilesForEdit");
		var singleFileId = otherFilesForEdit[rowNum].Id;
		if(singleFileId){
			var action = cmp.get("c.deleteFileServer");
			action.setParams({ objId : singleFileId });
			action.setCallback(this, function(response) {
				var status = response.getState();
				if(cmp.isValid() && status === "SUCCESS") {
					otherFilesForEdit.splice(rowNum, 1);
					cmp.set("v.otherFilesForEdit", otherFilesForEdit);
				}
			});
			$A.enqueueAction(action); 	
		}
	},

	updateFilesDesc : function(cmp, event, helper) {
		var otherFilesForEdit = cmp.get("v.otherFilesForEdit");
		var isError = false;
		for(const key in otherFilesForEdit){
			if(!otherFilesForEdit[key].Description){
				isError = true;
			}else{
				otherFilesForEdit[key].isDisabled = "true";
			}
		}
		if(!isError){
			cmp.set("v.updatePressed", "true");
			cmp.set("v.updatePressedError", "false");
			var action = cmp.get("c.updateFileDescs");
			action.setParams({ files : JSON.stringify(otherFilesForEdit) });	
			action.setCallback(this, function(response) {
				var status = response.getState();
				if(cmp.isValid() && status === "SUCCESS") {
					cmp.set("v.updatePressedSuccess", "true");
					setTimeout(() => {
						cmp.set("v.updatePressedSuccess", "false");
					}, 2000);
					cmp.set("v.showSpinner", "false");
					cmp.set("v.displayFilesModal", "false");
				}
			});
			$A.enqueueAction(action);
		}else{
			cmp.set("v.updatePressedError", "true");
			cmp.set("v.updatePressedSuccess", "false");
			cmp.set("v.showSpinner", "false");
		}
		cmp.set("v.otherFilesForEdit", otherFilesForEdit);
	},

	doValidation : function(cmp, event, helper) {
		var otherFilesForEdit = cmp.get("v.otherFilesForEdit");
		var errorCounter = 0;

		for ( const key in otherFilesForEdit){
			if(!otherFilesForEdit[key].Description){
				errorCounter++;
			}
		}
		if ( errorCounter== 0){
			cmp.set("v.updatePressedError", "false");
		} else {
			cmp.set("v.updatePressedError", "true");
		}
		return errorCounter == 0;
	}
})