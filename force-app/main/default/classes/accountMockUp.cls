public virtual class accountMockUp {

    public class MockGenerator implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req){
            HttpResponse res = new HttpResponse();
            if(req.getMethod() == 'GET'){
                res.setBody('{"size":1,"totalSize":1,"done":true,"queryLocator":null,"entityTypeName":"GlobalValueSet","records":[{"attributes":{"type":"GlobalValueSet","url":"/services/data/v42.0/tooling/sobjects/GlobalValueSet/0Nt1t000000TtQtCAK"},"Id":"0Nt1t000000TtQtCAK"}]}');
                res.setStatusCode(200);
            }else if(req.getMethod() == 'POST'){
                res.setBody('');
                res.setStatusCode(204);
            }
            return res;
        }
    }
    
    private static Boolean mockupRegistered = false;

    public static void registerMockUp(){
        if( !mockupRegistered ) {
            Test.setMock(HttpCalloutMock.class, new MockGenerator());
        }
        mockupRegistered = true;
    }
}
