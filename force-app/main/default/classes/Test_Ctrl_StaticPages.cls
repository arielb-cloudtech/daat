@isTest
public with sharing class Test_Ctrl_StaticPages {
    public Test_Ctrl_StaticPages() {

    }

    @TestSetup
    static void makeData(){
        //data creation obj
        ClsObjectCreator cls = new ClsObjectCreator();
        //page parent - creating 5 pp for 5 methods to check, one w/ needed name attr for each func
        List<Page_Parent__c> ppList = new List<Page_Parent__c>();
        ppList.add(cls.returnPageParent('Questions & Answers'));
        ppList.add(cls.returnPageParent('Main Page'));
        ppList.add(cls.returnPageParent('Legal Terms'));
        ppList.add(cls.returnPageParent('Terms Of Use'));
        ppList.add(cls.returnPageParent('Registration Terms Of Use'));
        insert ppList;
        //page child - 5 - one for each page parent
        List<Page_Child__c> pcList = new List<Page_Child__c>();
        pcList.add(cls.returnPageChild(ppList[0].Id));
        pcList.add(cls.returnPageChild(ppList[1].Id));
        pcList.add(cls.returnPageChild(ppList[2].Id));
        pcList.add(cls.returnPageChild(ppList[3].Id));
        pcList.add(cls.returnPageChild(ppList[4].Id));
        insert pcList;
    }

    @isTest
    public static void testGetFAQData(){
        Ctrl_StaticPages.getFAQData();
    }

    @isTest
    public static void testGetaboutData(){
        Ctrl_StaticPages.getAboutData(); 
    }

    @isTest
    public static void testGetLegalData(){
        Ctrl_StaticPages.getLegalData();
    }

    @isTest
    public static void testGetTermsData(){
        Ctrl_StaticPages.getTermsData();
    }

    @isTest
    public static void testGetRegTermsData(){
        Ctrl_StaticPages.getRegTermsData();
    }

    @isTest
    public static void testConstructor(){
        Ctrl_StaticPages obj = new Ctrl_StaticPages();
    }
}