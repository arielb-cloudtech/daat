({
	showText : function(component, event, helper) { 

		console.log('showText: ...');
		var reply = JSON.parse(JSON.stringify(component.get("v.reply")));
		//var element = document.getElementById(reply.mId);
		var element = document.querySelector('#' + reply.mId);
		if (element)
		{
			if (element.classList.contains('slds-truncate'))
				element.classList.remove('slds-truncate');
			else
				element.classList.add('slds-truncate');
		}

	}
})