public with sharing class LawItemTriggerHandler {
    public LawItemTriggerHandler(){}

    public void splitDocBriefInsert(List<Law_Item__c> newList) {
        Map<Id, Law_Item__c> rels = new Map<Id, Law_Item__c>();
        for(Law_Item__c l : newList){
            if(l.Notify_Users__c){
                rels.put(l.Id, l);
            }
        }

        if(!rels.isEmpty()){
            for(Law_Item__c li : newList)
            {
                if((li.Document_Brief__c != null && !String.isBlank(li.Document_Brief__c)) && li.Document_Brief__c.length() > 512)
                {
                    li.Document_Brief_Short_1__c = li.Document_Brief__c.subString(0,254);
                    li.Document_Brief_Short_1__c.subString(0,li.Document_Brief_Short_1__c.lastIndexOf(' '));
                    li.Document_Brief_Short_2__c = li.Document_Brief__c.subString(254,509);
                    li.Document_Brief_Short_2__c.subString(0,li.Document_Brief_Short_2__c.lastIndexOf(' '));
                }
            }
        }
    }

    public void splitDocBriefUpdate(Map<Id, Law_Item__c> newMap, Map<Id, Law_Item__c> oldMap, List<Law_Item__c> newList)
    {
        Map<Id, Law_Item__c> rels = new Map<Id, Law_Item__c>();
        for(Law_Item__c l : newList){
            if(l.Notify_Users__c){
                rels.put(l.Id, l);
            }
        }

        if(!rels.isEmpty()){
            for(Law_Item__c li : newMap.values())
            {
                if((li.Document_Brief__c != null && !String.isBlank(li.Document_Brief__c)) &&li.Document_Brief__c.length() > 512)
                {
                    li.Document_Brief_Short_1__c = li.Document_Brief__c.subString(0,254);
                    li.Document_Brief_Short_1__c.subString(0,li.Document_Brief_Short_1__c.lastIndexOf(' '));
                    li.Document_Brief_Short_2__c = li.Document_Brief__c.subString(254,509);
                    li.Document_Brief_Short_2__c.subString(0,li.Document_Brief_Short_2__c.lastIndexOf(' '));
                }
            }
        }
    }

    public void ImmediateMailing(List<Law_Item__c> newList, Map<Id, Law_Item__c> newMap, String Language){
        Map<Id, Law_Item__c> rels = new Map<Id, Law_Item__c>();
        for(Law_Item__c l : newList){
            if(l.Notify_Users__c){
                rels.put(l.Id, l);
            }
        }

        if(!rels.isEmpty()){
            Map<String,Set<String>> ProspectsByLawItems = Utils.ReturnProspectsByLawItems(rels.KeySet(),Language );

            for(String LawItem : ProspectsByLawItems.KeySet()){
                Batch_SendEmailsViaPardot bsevp = new Batch_SendEmailsViaPardot('Immediate', Language, ProspectsByLawItems.get(LawItem), new Set<Id>{LawItem});
                // Batch size is 8 to avoid hitting the limit of number of callouts per execute/transaction (1 to gain new API KEY + 1 to resend current request + 8)
                Database.executeBatch(bsevp, 8);
            }
        }
    }

    public void UnCheckNotify(List<Law_Item__c> newList){
        List<Law_Item__c> rels = new List<Law_Item__c>();
        for(Law_Item__c l : newList){
            if(l.Notify_Users__c){
                rels.add(new Law_Item__c(Id=l.Id, Notify_Users__c = false));
            }
        }

        if(!rels.isEmpty()){
           update rels;
        }
    }

    /*
        This method is initialized from the trigger to execute the proccess of sending emails to the wizard's emails list.
        The proccess is done by a Batch to avoid hitting governor limits.
    */
    public void sendEmailToWizardList(List<Law_Item__c> lawItemsFromTrigger, String Language){
        List<Law_Item__c> fromWizard = new List<Law_Item__c>();
        for(Law_Item__c li : lawItemsFromTrigger){
            if(li.Notify_Users__c){
                fromWizard.add(li);
            }
        }

        if( fromWizard != null && !fromWizard.isEmpty() ){
            Batch_SendEmailToWizardList batch = new Batch_SendEmailToWizardList(fromWizard, Language);
            /*
                100 toAdresses are enabled for single email msg
                10 sendEmail invocation are allowed within a single transaction,
                batch size -> 10 - to avoid hitting limit invocation if there are 10 la witems from the trigger - sendEmail is per LawItem. 
                Currently only 1 Law item will be recieved from the trigger.
            */
            Database.executeBatch(batch, 10);
        }
    }
}