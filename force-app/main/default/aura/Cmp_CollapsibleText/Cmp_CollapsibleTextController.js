({
    doInit : function(component, event, helper) {
        var fullText = component.get("v.fullText");
        if (!fullText ) { fullText = ''; }


        var collapseMinTextLength = component.get("v.collapseMinTextLength");
        var shortTextLength = component.get("v.shortTextLength");
        
        component.set("v.Paragraphs", fullText.split("\n"));

        if ( fullText.length > collapseMinTextLength ) {
            component.set("v.isCollapsible",  true );
            component.set("v.isExpended",  false );
            component.set("v.shortText",  fullText.substr(0,shortTextLength)+ '...');
        }
        else {
            if ( component.get("v.onViewFullText") ) {
                $A.enqueueAction(component.get("v.onViewFullText"));
                component.set("v.onViewFullTextHasBeenCalled", true);
            }
        }
    }, 
    toggleFullText : function(component, event, helper) {
        var isExpended = !component.get("v.isExpended");
        component.set( "v.isExpended",   isExpended );
        var itemStateId  = component.get("v.StateId");
        if ( itemStateId ) {
            if( isExpended ) {
                helper.openItems.push( itemStateId );
            }
            else {
                helper.openItems.splice( helper.openItems.indexOf(itemStateId), 1);
            }
        }
        if ( component.get("v.onViewFullText") && ! component.get("v.onViewFullTextHasBeenCalled") ) {
            $A.enqueueAction(component.get("v.onViewFullText"));
            component.set("v.onViewFullTextHasBeenCalled", true);
        }
    }
})