public with sharing class Ctrl_Header {

	public class currentUserResponse extends ReturnObjects.AjaxMessage {
		@AuraEnabled public ReturnObjects.Author user;
		@AuraEnabled public Integer numberOfUnreadReplies;
	} 

	/**
		Get data of current user.
		Called from header and then envoke event
	*/
    @AuraEnabled   
	public static currentUserResponse getSession(){

		currentUserResponse response = new currentUserResponse();

		if ( UserInfo.getSessionId() != null ) {
			List<User> myUser = [
				SELECT Id, Name, FirstName, LastName, LanguageLocaleKey 
				FROM User
                WHERE Id = :System.UserInfo.getUserId() 
				LIMIT 1];
			response.user = new ReturnObjects.Author( myUser[0] );

			Id contactId = Ctrl_Community_MyAccount.getCurrentContactId();
			response.numberOfUnreadReplies = database.countQuery('SELECT count() FROM  Comment_Reply__c WHERE Reply_to_Comment__r.Comment_By__c = :contactId AND Read__c != TRUE');
	
			response.setSuccessStatus();
			
		}
		
		return response;  
	}

	@AuraEnabled   
	public static String changeLang(String lang)
	{
		try
		{
			User us = new User(Id = UserInfo.getUserId(), LanguageLocaleKey = lang);
			update us;
			return 'success';
		}catch(Exception ex)
		{
			System.debug('!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber());
			return '!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber();
		}
	}

	@AuraEnabled
	public static String getData(String userId)
	{
		Map<Id, Comment__c> lawItemCommentsMap = new Map<Id, Comment__c>([SELECT Id,
																		(SELECT Id,Read__c FROM Ministry_Responses__r )																		
																		FROM Comment__c WHERE OwnerId = :userId AND Comment_Status__c != 'Draft'  ]);
		
		Integer notread = 0;
	 	Map<Id, Comment_Reply__c> commentReplyMap = new Map<Id, Comment_Reply__c>();
		 for(Id commentId : lawItemCommentsMap.keySet()){
			for(Comment_Reply__c reply : lawItemCommentsMap.get(commentId).Ministry_Responses__r){
				if(reply.Read__c != true){
				notread++;
				}
			}					
		 }																	
		return String.valueOf(notread);
	}	
	
}