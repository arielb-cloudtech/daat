({ 
	createReply : function(component, event, helper) {
		console.log('replyId before = ' + component.get("v.replyId"));				
		if (component.get("v.replyId").length > 0)
			return;
		
		var action = component.get("c.CreateComment");
		 console.log('change = ' + component.get("v.replyId"));
		var recordId = component.get("v.recordId");
		console.log('change = ' + component.get("v.replyId"));
		console.log('recordId = ' + recordId);
		action.setParams({ "action": "reply", "lawItemId": recordId, "commentId" : component.get("v.comment.mId") });
		action.setCallback(this, function (response) {						
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {
				var returnValue = response.getReturnValue();								
				component.set("v.replyId", returnValue);
				console.log('replyId = ' + component.get("v.replyId"));				
			}
			else {				
				alert("Custom ERROR!!!");
			}			
		});
		$A.enqueueAction(action);
	}
})