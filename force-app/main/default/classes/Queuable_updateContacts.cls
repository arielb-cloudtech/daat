public class Queuable_updateContacts implements Queueable{

    Map<Id, Contact>  communityContacts = new Map<Id, Contact>();
    Map<Id, Id> userToContact = new Map<Id, Id>();
    Profile communityProfile;
    public Queuable_updateContacts(Map<Id, Contact> contact, Map<Id, Id> user, Profile profile){
        communityContacts = contact;
        userToContact = user;
        communityProfile = profile;
    }
    public void execute(QueueableContext context){
        System.debug('communityContacts: ' + communityContacts);
        System.debug('communityContacts: ' + userToContact);
       List<Id> contactsToUpdate = new List<Id>();
		for(Id userId : userToContact.keySet()){
			if(userId != null && userToContact.get(userId) != null){
				Id contactId = userToContact.get(userId);
				if(communityContacts.containsKey(contactId) && communityContacts.get(contactId) != null){
					System.debug('~~~ If: ');
					communityContacts.get(contactId).User__c = userId;
					communityContacts.get(contactId).Active_User__c = true;
					communityContacts.get(contactId).Role__c = communityProfile.Name;

					contactsToUpdate.add(contactId);
				}
			}
		}
    
    update communityContacts.values();
    }

}
